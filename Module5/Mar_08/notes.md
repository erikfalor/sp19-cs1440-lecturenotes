# CS1440 - Fri Mar 08 - Module 5

# Announcements

## Exam #1 the week after Spring Break

Available in the Testing Center from Tue 3/19 - Thu 3/21

We will hold a review on Monday 3/18



## DC435 Capture The Flag system

Zodiak (Matt Lorimer) of USU IT presented his CTF system last night at the
DC435 meetup.  You're invited to get an account and search for flags in this
excellent simulation.

1. Visit http://bit.ly/dc435 to get onto the DC435 Slack

2. Join the #ctf channel and introduce yourself to @Zodiak to get your own
   account on the CTF system

3. ???

4. Profit!




# Topics:
* Assignment #4 stand up meeting
* Refactoring
* Readability makes a difference
* Code Smells




----------------------------------------------------------------------------
# Assignment #4 stand up meeting

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?


*   What are some things that are working well for you?

*   What are some impediments that you're encountering?
    - Impending spring break
    - Functions mentioned in the starter code, but not actually implemented

    -   Getting the unit tests to pass
        -   Mismatch between the tests as originially written, and changes
            you've made to the starter code
    -   Too many newlines in the output

*   What is your plan to overcome your barriers?



----------------------------------------------------------------------------
# Refactoring

## Definition:
> The process of restructuring existing computer code without changing its
> external behavior.

[Code Refactoring](https://en.wikipedia.org/wiki/Code_refactoring)


## Refactoring improves non-functional attributes of the software.

*Q0.* What's the point of making big changes to code *without* changing its
externally-observable behavior?

## Refactoring improves non-functional attributes of the software.


*Q1.* What tools and techniques can we leverage to make broad, sweeping changes
to a code base *without* changing its behavior?

-   git - to bravely make changes without consequence
-   rubber ducky - to ID your unknown-unknowns
-   tutor lab - bounce ideas off other people's heads
-   Experimentation with the REPL

*Q2.* How can we apply our handy-dandy GPSTs to the task of refactoring?

-   Don't get frustrated - if I'm in a calm and relaxing environment, I can
    refactor betterer
-   Reduce the problem - down to the level a muggle can understand
-   write down all of the ideas/problems/moments of stupor you encounter as you
    read the requirements
-   Divide the problem: refactor one function/class/file at a time
-   Look for Analogies: collapse duplicate or analogous functionality
-   Always Have a Plan: schedule time to take a break from the code and go do
    something else and let your subconscious mind go to work on the problem



General Problem-Solving Techniques
==================================
* Always Have a Plan
* Divide the Problem - split a long function into smaller functions
* Start with What You Know - refactor the parts which you do understand
* Restate the Problem
* Reduce the Problem
* Look for Analogies
* Experiment - test to make sure we didn't change it
* Don't get Frustrated



----------------------------------------------------------------------------
# Readability makes a difference

![One does not simply...](one_does_not_simply.jpg)

If you insist on using global variables, the least you can do is give them good
names.

<Demo: readability/>


![I, too, like to live dangerously](live_dangerously.png)





----------------------------------------------------------------------------
# Code Smells

> In computer programming, a code smell is any characteristic in the source
> code of a program that possibly indicates a deeper problem.  Determining what
> is and is not a code smell is subjective, and varies by language, developer,
> and development methodology.
>
> -- [Wikipedia](https://en.wikipedia.org/wiki/Code_smell)



### A catalogue of Code Smells

[Code Smells on SourceMaking](https://sourcemaking.com/refactoring/smells)



### Somebody collected an entire repository of smelly Python code

[Python Code Disasters](https://github.com/sobolevn/python-code-disasters.git)

    git clone https://github.com/sobolevn/python-code-disasters.git
    Cloning into 'python-code-disasters'...

Let's look at a few examples.  With your neighbors list on your *mud cards*
what you find to be problematic with these examples and why:

* python-code-disasters/python/my_first_calculator.py
* python-code-disasters/python/send_email.py
