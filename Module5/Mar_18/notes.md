# CS1440 - Mon Mar 18 - Module 5

# Announcements

## Exam #1 in Testing Center this week

The exam is available from Tues 3/19 through Thur 3/21


## Assignment 4 Effort Survey

* [Section 001](https://usu.instructure.com/courses/531135/quizzes/715546)
* [Section 002](https://usu.instructure.com/courses/527950/quizzes/715548)

I appreciate any feedback you can offer that helps create make better assignments.





----------------------------------------------------------------------------
# Exam 1 Review

* 40 questions in total - 100 points
* 10 review questions from the previous exam
* 30 questions covering material spanning modules 3-5


## Review question topics
* Git basics
* Essential vs. Accidental 
* Identify General Problem-Solving Techniques
* Python string methods



## Passing the Word

* The primary work-product of the chief architect is the system manual. Brooks states that the manual must describe
    -   Everything the user sees (e.g. user interfaces), and refrain from describing *how* the system works


* Existing computer systems were used as formal definitions in the early days of computing
    -   True


* What are advantages of using an existing implementation as a formal definition?
    - Questions are settled unambiguously by experiment; debate is unnecessary
    - Answers are always correct, by definition
    - Answers are as precise as desired


* What are disadvantages of using an existing implementation as a formal definition?
    - Invalid syntax always provides at least some result
    - All kinds of side-effects can appear which come to be relied upon by programmers
    - The existing system describes not only *what* should happen, but a great deal about *how* it should be done


* English, or any other human language for that matter, is precise enough to formally define a software system
    -   False



## Python's built-in data structures

* What's the difference between a list and a tuple in Python?
    -   Tuples are immutable sequences, whereas lists are sequences that can be changed


* How do I ask whether the key "Skeleton" is in the set `Alohomora`?
    -   "Skeleton" in Alohomora


* It is correct to regard the contents sets and dictionaries as being stored in order
    -   False


* A dictionary is a mapping between ______ and ______
    - Keys and values


* You can "invert" a dictionary's key/value pairs at the risk of
    -   Losing data when a duplicate value becomes a key


* A key in a dictionary may only appear once; adding a new value to an existing key in a dictionary results in the existing value being overwritten
    -   True


* A set is like a dictionary without ______; it is simply a collection of keys.
    - Values


* Sets are useful for answering a question such as *blank*
    - "Have I seen this thing before?"


* You have a set and a list each with 50k items; finding an arbitrary item in
  the set will be slower than finding an arbitrary item in the list
    -   False


* I should use an ordered container (list or tuple) when the relative position of my data elements is important to me
    -   True



## Intermediate Git

* Git tags must be manually pushed to a remote repository
    -   True


* In git, what is the working tree?
    -   The files tracked by the git repository which are presently checked out, along with any new or changed files


* Which command displays the uncommitted changes you have made in your files since your last commit?
    - git diff


* Which command displays a manual page defining bits of git jargon?
    -   git help glossary


* What is the significance of the SHA-1 Object Name associated with a git commit?
    -   It is a cryptographically-strong digital fingerprint of the commit prevents tampering with the history of the repository


* What does `HEAD` mean to git?
    -   It refers to the currently checked-out commit


* Which command do you use to remove the tag `Assn3` from the remote repository named `origin`?
    -   git push --delete origin Assn3


* You may abbreviate 'a8600bda633faa069c0b4ad606f8ef1f340deaa4' to 'a8600bd'
    -   True


* Which command is used to "go back in time" to the commit tagged Assn2?
    -   git checkout Assn2



## Software Testing

* _Validation_ is the process that aims to answer the question "is my program doing the right thing?"
    -   True


* _Verification_ is the process that aims to answer the question "is my program doing the thing right?"
    -   True


* A sufficiently thorough testing method can uncover all possible errors in all programs
    -   False


* A software failure is
    - Runtime behavior that is not expected


* A software error is
    - A problem in the code, the design, or requirements (i.e. a bug)



## Unified Modeling Language (UML)

* A dependency in UML is represented as a ______ line
    -   Dashed


* A dependency provides insight into the ______ definition of a class
    -   Static - the fact that one class's definition depends upon another class


* An association in UML is represented as a ______ line
    - Solid


* An association in UML represents
    -   A link between objects which exist while the program is running
    -   Runtime structure between objects


* What does an arrowhead on an association mean?
    -   Navigability; one object contains a reference to another object and
        can "reach" it as the program is running


* What is a multiplicity constraint?
    -   Indicates the number of objects participating in a relationship

* What is a role?
    -   Annotation on an association which provides an alternative name for a
        set of objects from the perspective of the connected objects
