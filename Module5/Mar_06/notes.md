# CS1440 - Wed Mar 06 - Module 5

# Announcements

## FSLC - Scheme: the reason your programming language has any redeeming qualities at all

Bring a laptop with an SSH client (OpenSSH or PuTTY) so you can play along.

Wednesday, March 6th @ 7pm
ESLC 053




## DC435 Capture The Flag

Make sure you bring a laptop with RDP (Remote Desktop Protocol) and/or SSH
clients (e.g. PuTTY) so you can play!

You'll be logging into a Kali Linux box from which to do your hacking.

Thursday, March 7th @ 7pm
Bridgerland Main Campus - 1301 North 600 West - Room 840



## Exam #1 the week after Spring Break

Available in the Testing Center from Tue 3/19 - Thu 3/21

We will hold a review on Monday 3/18


# Topics:
* Assigned Reading Discussion
* Introduction to Assignment 5


----------------------------------------------------------------------------
# Assigned Reading Discussion


## The Mythical Man-Month

* Do teams spend enough time testing their code?

* Do more programmers make a project come together faster?


Developers are irresponsibly optimistic when it comes to estimating project
schedules.  On the other hand, some developers give up on estimations entirely
and just throw out huge numbers without paying much thought to the matter.

With experience you can become a good estimator.  This is helped by gaining
knowledge both in the problem domain as well as with the tools/technologies you
will be using.

One reason large projects exceed their schedule is communication. As more
people become involved, more coordination is needed.  This increases time spent
in large meetings.  Meeting time is very expensive (programmers cost a lot per
hour), and this is time that is not spent on creating code or running tests.

System testing is often delayed until all production is completed.  If the
production schedule has slipped, testing time is foreshortened.  This is a grave
mistake, but one that is, sadly, all too common.

Brooks recommends spending up to 50% of a project's time in testing. Most
projects don't budget for this, and go past the due date by a significant
amount anyway.



## *The Pragmatic Programmer*, Chapter 6: "While you are coding"

* As programs evolve, must we reconsider our earlier design decisions?
    - Of course!

* Why must we reconsider our earlier design decisions?
    - Because with the benefit of hindsight, we will find better ways of doing
      the same thing

    - Until you have a system in front of you, you might not actually know what
      it is you want to make

* What is refactoring at its heart?
    - Improving the non-functional aspects of the code base

* Should your refactor *and* add functionality at the same time
  (e.g. within the same git commit?)

    - No!  You'll increase the likelihood of introducing new bugs

    - Math analogy: You can only solve for one variable at a time.  Doing two
      things at once increases the likelihood that something will go wrong, and
      the increased complexity makes it harder to undo that.


----------------------------------------------------------------------------
# Introduction to Assignment 5

* [Section 001](https://usu.instructure.com/courses/531135/assignments/2606371)
* [Section 002](https://usu.instructure.com/courses/527950/assignments/2606382)


## Your tasks:
* Study the Starter Code
* Improve the Non-Functional Aspects of this Code Base


## What is this program about?

    git clone https://bitbucket.org/erikfalor/cs1440-falor-erik-assn5
