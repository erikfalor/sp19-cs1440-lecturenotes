# CS1440 - Fri Mar 22 - Module 5

# Announcements

## Assigned Reading for next Wednesday

Wednesday Mar 27: Chapter 5 of *The Pragmatic Programmer* - "Bend, or Break"


# Topics:
* Assignment #5 stand up meeting
* Exam #1 Recap
* Exercise: Refactoring a Graphing Calculator



----------------------------------------------------------------------------
# Assignment #5 stand up meeting
        _
    .__(.)<  - Scrum!
     \___)   

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?


----------------------------------------------------------------------------
# Exam #1 Recap

## Failures & Errors
__Question 31__ (84% answered correctly)
* Q:    A software failure is
* A:    Runtime behavior that is not expected


__Question 32__ (64% answered correctly)
* Q:    A software error is
* A:    A problem in the code, the design, or requirements (i.e. a bug)


## Unified Modeling Language
__Question 34__ (87% answered correctly)
* Q:    An association tells us something about the structure of the ______ data
* A:    Runtime


__Question 35__ (67% answered correctly)
* Q:    An association in UML is represented as a solid line and indicates
* A:    A link between objects which exist while the program is running


![UML Diagram](uml_diagram.png)

__Question 36__ (69% answered correctly)
* Q:    The `C` class must be defined in order to define the `B` class
* A:    True


----------------------------------------------------------------------------
# Exercise: Refactoring a Graphing Calculator

I have a sample of [ugly Python code](graphing_calculator/ugly.py).
Let's refactor it together.

<Demo: what does it do?>

`refactor`

Take a few minutes with your study buddies to read through the code and write
notes about what you notice.

1. What would you change to improve the readability of this code?
2. Are there variables or statements which are unused?
3. Are there passages of code which are redundant?
4. Capture the essence of this program in two or three short statements;
   decide how we could split this program into three modules.


## Before and after refactoring

The original program `graphing_calculator/ugly.py` was 138 lines long,
including comments and whitespace.

I decided to break this program into three pieces:

1. A driver program `graphing_calculator/main.py` (35 lines)
2. A class to gather together the function to plot, its name and color
   `graphing_calculator/Plotter.py` (9 lines)
3. A class which is wholly responsible for the GUI.  It handles everything from
   setting up the drawing surface to putting the pixels on it (40 lines)

In total this came out to 84 lines of code, comments and whitespace.  I don't
mention the line count to give the impression that this is a good metric to
judge the quality of a piece of software; I am illustrating that around 40% of
this program turned out to be redundant or useless.

Additionally, we identified a few potential errors in the program as we went
over it with a fine-toothed comb, improved its readability with better names,
and made it much easier to extend and modify later on down the road.
