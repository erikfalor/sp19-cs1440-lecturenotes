#!/usr/bin/python3

from math import sin, cos
from Graph import Graph
from Plotter import Plotter


def square(x):
    return x * x


def cube(x):
    return x * x * x


def cosXsquared(x):
    return cos(x*x)


graph = Graph(x_axis=(-5.0, 5.0), y_axis=(-5.0, 5.0), pixels=1024)

# Plotter's default initializer plots the identity function with a white line
p = Plotter()
graph.draw_plot(p)

# Erik's style preference: I can further shorten my program by realizing that
# the variable `p` is never used after its passed to `graph.draw_plot()`
graph.draw_plot(Plotter(name="sine", color='#ff0000', fn=sin))
graph.draw_plot(Plotter(name="cosine", color='#00ff00', fn=cos))
graph.draw_plot(Plotter(name="abs", color='#0000ff', fn=abs))
graph.draw_plot(Plotter(name="square", color='#ffff00', fn=square))
graph.draw_plot(Plotter(name="cube", color='#ff00ff', fn=cube))
graph.draw_plot(Plotter(name="cos(x^2)", color='#00ffff', fn=cosXsquared))

graph.mainloop()
