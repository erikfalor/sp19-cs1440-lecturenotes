from tkinter import Tk, Canvas, PhotoImage, mainloop


class Graph:
    def __init__(self, x_axis=(-5.0, 5.0), y_axis=(-5.0, 5.0), pixels=1024):
        self.x_axis = x_axis
        self.y_axis = y_axis
        self.pixels = pixels
        self.window = Tk()

        # TODO: Why hardcode the color of the background?  It could be a
        # parameter of the initializer with a default value of black
        self.canvas = Canvas(self.window, width=pixels, height=pixels, bg="#000000")
        self.half_pixels = self.pixels / 2
        self.img = PhotoImage(width=pixels, height=pixels)
        self.canvas.create_image((self.half_pixels, self.half_pixels),
                                 image=self.img, state="normal")
        self.canvas.pack()

    def draw_plot(self, plotter):
        print(f"Plotting {plotter.name}...")

        for x_px in range(self.pixels + 1):
            x_value = (self.x_axis[0]
                       + (x_px * ((self.x_axis[1] - self.x_axis[0]) / float(self.pixels))))

            y_value = plotter.fn(x_value)

            # TODO: shouldn't this conversion be done in terms of
            # `self.y_axis` instead of `self.x_axis`?
            y_px = int((self.half_pixels
                        - (y_value / ((self.x_axis[1] - self.x_axis[0]) / float(self.pixels)))))

            # Avoid a crash when plotting Y pixels which are not on the PhotoImage
            if 0 <= y_px <= self.pixels:
                self.img.put(plotter.color, (x_px, y_px))
                self.window.update()

    def mainloop(self):
        mainloop()
