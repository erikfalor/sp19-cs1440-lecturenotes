# CS1440 - Mon Mar 04 - Module 5

# Announcements


## HackUSU March Madness Mini-Hackathon Tonight!!!

The March Madness Hackathon kicks off tonight.

Bring your team (or find one there), make your plans, and prepare to take over
the world!!!!

The rules of the hackathon and Pizza will be served.

Old Main room 406
6pm - 7pm




## Assigned Reading for Wednesday March 6th

* Chapter 6 of *The Pragmatic Programmer* - "While you are coding"
* Chapter 2 of *The Mythical Man-Month* - "The Mythical Man-Month"





## SDL/USU Technical Lecture - The Integrated Engineer

Preparing Yourself to Successfully Integrate into a Multidisciplinary Team
presented by Ashley Willardson, Software Tester for the Space Dynamics Laboratory

Free pizza after lecture. 

Tuesday, March 4th @ 4:30pm
ENGR 201. 



## FSLC - Scheme: the reason your programming language has any redeeming qualities at all

> A language that doesn't affect the way you think about programming,
> is not worth knowing.
>
> -- Alan Perlis

Take a step towards enlightenment by exploring Scheme, a practical and simple
dialect of LISP.  This talk will demystify those concepts functional
programming novices find the most confusing, giving you a whole new way of
thinking about programming.

Bring a laptop with an SSH client (OpenSSH or PuTTY) so you can play along.

Wednesday, March 6th @ 7pm
ESLC 053




## DC435 Capture The Flag

Make sure you bring a laptop with RDP (Remote Desktop Protocol) and/or SSH
clients (e.g. PuTTY) so you can play!

You'll be logging into a Kali Linux box from which to do your hacking.

Thursday, March 7th @ 7pm
Bridgerland Main Campus - 1301 North 600 West - Room 840



# Topics:

* Paxton: PyCharm UnitTest howto
* Types of software tests
* Non-functional requirements
* Refactoring


----------------------------------------------------------------------------
# Paxton: PyCharm Unit Test How-To

* In the file explorer, right click the 'Testing' folder -> Run Unittests in 'Testing'

* Edit configurations -> '+' -> Python Tests -> unittests
    -   Specify the 'Testing' folder as the 'Script Path'
    -   Set working directory to the 'Testing' folder
    -   Set interpreter to your Anaconda Python 3.7 interpreter


----------------------------------------------------------------------------
# Types of software tests

* [Software testing and jargon](../Readings_and_Resources.md#markdown-header-software-testing)

* [Important Software Testing Jargon](../Readings_and_Resources.md#markdown-header-important-software-testing-jargon)

