# CS1440 - Wed Mar 20 - Module 5

# Announcements


## Assigned Reading for next Wednesday

Wednesday Mar 27: Chapter 5 of *The Pragmatic Programmer* - "Bend, or Break"


## FSLC - Belated Pi Day celebration

We wanted to share with you some cool projects to do on a Raspberry Pi on Pi
day, but it landed on Spring Break this year.  Better late than never!

Wednesday, March 20th
7pm - ESLC Room 053


--------------------------------------------------------------------------------
# Code Reading Activity

[CODE_READ_II - Functions (Student) - Py3.pdf](CODE_READ_II - Functions (Student) - Py3.pdf)
