# Table of Contents

* Assigned Reading
* Non-functional requirements
* Refactoring


-------------------------------------------------------------------------------
# Assigned Reading

* Wednesday Mar 27: Chapter 5 of *The Pragmatic Programmer* - "Bend, or Break"
* Wednesday Apr 3: Chapter 11 of *The Mythical Man Month* - "Plan to throw one away"




----------------------------------------------------------------------------
# Non-functional attributes

Broadly speaking, *non-functional attributes* are aspects of a program which
are difficult to measure, or which aren't spelled out in the specification.
Often people think of these things as being non-functional attributes:

* Speed, memory usage, runtime performance in general
* Number of lines of code, functions, classes, etc.
* Number of parameters to functions, number of methods or data members in a class
* How easy it is to extend the code
* How readable the code is
* How easy it is to locate and fix bugs in the code

These are all important things that may relate to the performance and operation
of a system, but they often take a backseat to the set of capabilities that the
customer expects.  Often these non-functional attributes are considered as
*quality requirements* of the system.


- [Wikipedia: Non-functional requirement](https://en.wikipedia.org/wiki/Non-functional_requirement)




--------------------------------------------------------------------------------
# Refactoring

> The process of restructuring existing computer code without changing its
> external behavior.

-   [Sourcemaking.com: Code smells](https://sourcemaking.com/refactoring/smells)
-   [Sourcemaking.com: Refactoring](https://sourcemaking.com/refactoring)
-   [Wikipedia: Code Refactoring](https://en.wikipedia.org/wiki/Code_refactoring)

As an employee of DuckieCorp you have been tasked with taking a client's
project to the next level. To get there means adding new features and possibly
fixing some bugs. Instead of starting from scratch and building the program
"the right way" you are instead beginning from a working prototype.

In this case it may be desirable to do so because the program is doing
computations which you don't fully understand the workings of - it seems easier
to change this already program instead of trying to make one of your own.

In other cases you will join a project that has been in progress for many years
(or decades), and which is the combined effort of many developers. A project of
this scope may have cost hundreds of man-years of combined effort and is "too
big to fail"; it would be far too expensive to scrap it and start over the
"right way".

In either case, the deficiencies in the code bases are obvious and the desire
exists to make them go away (especially among the engineers). The project
managers who decide how to allocate scarce development resources will view the
sad state of the source code as a case of "if it ain't broke, don't fix it".

From their perspective the risks of tackling such a large task outweigh the
comparatively small benefits. In the worst case, your efforts will fail and
waste weeks or months of time. In the best case your efforts will produce
neither new features nor performance enhancements. If you make the improvements
correctly the end-users will receive an update with no perceptible
enhancements.

Explaining that these changes will lead to future productivity enhancements or
a reduction of bugs in subsequent updates is a tough sell when considered
against the obvious and immediate risks. Before embarking upon this course of
action it is imperative to provide assurances that your efforts cannot result
in expensive and time-consuming setbacks.
