# CS1440 - Fri Jan 18 - Module 1

# Announcements


## SWE Engineering Student Panel January 22nd

"What to expect when expecting an Engineering degree"

4:30pm - 5:15pm Tuesday, Jan 22
ENGR 101

Q&A with upperclassmen in each major within the CoE

*   What might your future carreer be like
*   What classes should I take?
*   Where can I find more information about the direction I should go
*   FREE PIZZA!
*   Men and women welcome (even tho it's a SWE event)




## Cyber Security Club

The opening meeting will occur next Wednesday, January 23rd @ 6pm - ESLC 053



## FSLC InstallFest

Wednesday, January 23rd @ 7pm - ESLC 053

Do you want to rock Linux on your own computer, but don't know how to start?
Get experienced help installing it!

* Try a new distribution
* Reinstall Linux
* Dual-, Triple-, or Quadruple-boot
* Repair a broken installation

Bring a laptop and a flash drive, or install on one of our machines





# Topics:
* Assignment #0 Retrospective
* Reading files in Python
    * The Read, Eval, Print, Loop (REPL)



----------------------------------------------------------------------------
# Assignment #0 Retrospective

With the submission of Assignment #0 you have just completed a big step on your
road to understanding git and becoming a valuable part of the DuckieCorp team.


    db   d8b   db  .d88b.   .d88b.  d888888b db db db
    88   I8I   88 .8P  88. .8P  88. `~~88~~' 88 88 88
    88   I8I   88 88  d'88 88  d'88    88    YP YP YP
    Y8   I8I   88 88 d' 88 88 d' 88    88
    `8b d8'8b d8' `88  d8' `88  d8'    88    db db db
     `8b8' `8d8'   `Y88P'   `Y88P'     YP    YP YP YP


Reaching an important milestone is a good time for a software team to reflect
on what went well (things to continue to do) and what didn't (things to avoid
for next time).

This activity will enable us to identify the most common positive events and
disasters that befell the class.  Our goal is to keep what is effective and
discard things that hold us back.


### Aha! and Oh No! Moments

Come get a mud card.

On each note write down your A#

Draw a timeline on your mud card.


                    

### Questions for you to consider:

* What was the single biggest problem for the class?

    *   Unsure where the files were being stored, under c:\Users\<username>
        instead of the Desktop

    *   Navigating the CMD prompt is new, syntax and spelling can be tricky

    *   Lost pa55w0rd!


* What was the most beneficial thing that happened to the class?
    *   Tutor lab helped with using git

    *   Lecture notes were helpful

    *   Read the instructions carefully, put them into your own words

    *   Write a skeleton program in comments, then translate the comments into
        working code (Always have a plan, divide the problem, start with what
        you know, etc.)



What can you take away from this timeline?

* What is one thing that you will *begin doing* for Assignment 1?
    *   Start early, give yourself some time for the creative process to happen

* What is one thing that you will *stop doing* for Assignment 1?
    *   Stop procrastinating on getting a start



## Mud Card Impressions

After going over your mud cards I have noticed the following sentiments:

1.  Many of you got stuck because of an incomplete understanding of what the
    assignment is about.  You will address this by more carefully reading and
    understanding the assignment description before working toward an unclear
    goal.
2.  Many of you discovered that the answers to your questions were in the
    lecture notes all along.
3.  The tutors in the tutor lab have become quite good at helping you with git
    problems.  They will be a good resource for future assignments.
4.  I have some very talented doodlers in my class.  I respect this.
5.  Talking through the problem yields insights quickly.
6.  A few of you declined to sign your papers, not even with an A#.  You
    realize that these things are worth points, right?


----------------------------------------------------------------------------
# Reading files in Python

[Python tutorial section 7.2](https://docs.python.org/3.7/tutorial/inputoutput.html#reading-and-writing-files)

Many of the programs we'll be writing this semester will take as command-line
arguments names of files that your program must process.

These are a few basic operations that all programing languages allow you to
perform on files:

* Open
* Read
* Write (we'll do this later)
* Close



## The Read, Eval, Print, Loop (REPL)
We will explore and experiment with file operations in the REPL

[The Read, Eval, Print, Loop (REPL)](Readings_and_resources.md#markdown-header-the-read-eval-print-loop-repl)

When in doubt, remember these two important functions in the REPL

*   `help()`
*   `dir()`

Use these functions whenever you need a hint.


## `f = open()`

This function takes the name of a file as an argument and gives us back a file
object.

If there is any problem accessing or opening the file this function will raise
an error which will terminate your program.



## `f.read()`

You can read any quantity of data from a file.  To read a particular number of
bytes use the `.read()` method, passing that number as the argument:

    tenBytes = f.read(10)
    twentyBytes = f.read(20)

Python keeps a cursor within the file to remember where you were at the last
time you read from the file.  Each time you call `.read()` the cursor is
advanced automatically.

`.read()` returns the chunk of data as a String.  It is not an error to try to
read beyond the end of the file.  For example, if you call `f.read(100)` and
there are only 30 bytes available, you get those remaining 30 bytes.

Subsequent reads simply return an empty string, which is one way you can detect
the end of the file.

If you want to *slurp* the entire file in one go, pass a negative number to
`.read()`.

*   Using the REPL, can you find a method that allows you to rewind the file to
    the beginning so as to re-read it?



## `f.readline()``

When we know that our file contains lines of text it is more convenient to read
it one line at a time.  `.readline()` will read the file until it reaches an
end-of-line (EOL) character "\n" or the end-of-file (EOF), returning a string
which includes the EOL.

*   How will your program know when it has reached the EOF?  Try this out in the REPL.
*   What happens when you use `.readline()` on a non-text file?  There is a
    .jpg file in this lecture notes directory.  What happens when you read it?
*   Because the string resulting from `.readline()` contains the EOL "\n"
    character, printing it with the `print()` function causes an extra blank
    line to appear.  How can you prevent this from happening?


## `f.close()`
`
Your program is limited in the number of files it can hold open at a time.
This limit varies by system, and once reached, subsequent calls to `open()`
will fail until the number of open files is reduced.

After you are finished using a file it is good programming hygeine to close the
file using its `.close()` method.  This is something that I and the graders
will be on the lookout for.


*   What is the consequence of not closing files that you are finished with?
*   Is it okay to leave a file open if you are still using it?



## Reading and printing a text file line-by-line

With these concepts we can create a *copy* program which copies a text from
from the disk, line-by-line, to the screen.

    f = open("notes.md")
    for line in f:
        print(line, end='')

When you use a file object in a for loop like this, Python calls the
`.readline()` method for you and quits when the empty string is returned, which
indicates that you have reached the end of the file.  This lets you process a
**HUGE** file without needing massive amounts of RAM.
