# CS1440 - Mon Jan 14 - Module 1

# Announcements

## CyberSecurity talk by Rob Clyde

Tuesday Jan 15 6pm Huntsman Hall room 326



## Free Software and Linux Club Spring Opening Social


Come hang out with people who share an interest in Linux

There will be pizza!

Wednesday Jan 16, 7pm @ ESLC 053




# Topics:

* What is an Integrated Development Environment?


----------------------------------------------------------------------------
# What is an Integrated Development Environment?

[What is an IDE?](https://bitbucket.org/erikfalor/sp19-cs1440-lecturenotes/src/master/Readings_and_resources.md#markdown-header-what-is-an-ide)

## Configuring PyCharm to use Anaconda Python


## Pycharm + Anaconda

Paxton Alexander will demonstrate how to configure PyCharm to use Anaconda's
version of Python.
