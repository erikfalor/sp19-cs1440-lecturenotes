# Module 1 Readings & Resources

Read "The Tar Pit", Chapter 1 of "The Mythical Man-Month" in preparation for a
discussion this Wednesday (1/16)




--------------------------------------------------------------------------------
## What is an IDE?

**Important**!  Discussion of this topic must *not* be construed as a rule or
requirement for this class.  I am not particularly concerned with how you choose to
write your code, provided that it

1.  runs on Anaconda Python version 3.7
2.  is written honestly 

It is a goal of this class to teach you how to use real-world tools and an IDE
is *the* primary tool for most professionals.  My demonstration of PyCharm is
not an endorsement for its use, nor are you required to use PyCharm or any IDE.

#### Integrated Development Environment (IDE)

A definition from [Wikipedia](https://en.wikipedia.org/wiki/Integrated_development_environment)

> An integrated development environment (IDE) is a software application that
> provides comprehensive facilities to computer programmers for software
> development. An IDE normally consists of a source code editor, build
> automation tools, and a debugger.
> 
> [...]
> 
> The boundary between an integrated development environment and other parts of
> the broader software development environment is not well-defined. 

There are two broad (and fuzzy) categories into which the program you write
your code may fall:

1.  Text editor
2.  Integrated Development Environment


This article gives a good overview 
* [Migrating from Text Editors](https://www.jetbrains.com/help/pycharm/migrating-from-text-editors.html)


### Advantages of IDEs:

*   Language-specific tools: more helpful and insightful than tools which
    perform string-based operations.  So much so that the culture of some
    programming languages is such that the programming language is virtually
    inseperable from its IDE
*   One-program-for-everything: you need only learn one interface, one set of
    shortcut keys, one menu layout, etc. 
*   Promotes laziness: let the specialized tool free you from repetitive,
    tedious work
*   Everything just works: you have no need to turn to several disjoint
    programs with different interfaces which were not necessarily designed to
    work together.  Tools within an IDE are meant to fit together.


### Advantages of Text Editors:

*   Language-agnostic: work equally between different programming languages,
    especially new and less-popular languages which do not have a dedicated IDE
*   Flexibility: gives the user control/responsibility over configuration
    choices
*   IDEs promote the bad kind of laziness: developers become too reliant on the
    IDE and cannot function without it; an editor doesn't rot your mind
*   Simplicity: both conceptually simple and technically simple; doesn't take
    2GB of RAM and two minutes to start up in the morning



Programming tools represent a very personal choice, and is the prime example of
a [Religious Issue](http://www.catb.org/jargon/html/R/religious-issues.html).

In fact, hackers' preference among [text editors](http://wiki.c2.com/?EmacsVsVi)
remains one of the longest running
[Holy Wars](http://www.catb.org/jargon/html/H/holy-wars.html) in computing.



Most of you will enjoy PyCharm and some of you will hate it; that's okay.
Nobody (including me) is judging you for how you choose to write code.  I
encourage you to try several tools for an extended period of time to discover
what works best for you.

Even if you ultimately decide that IDEs are not your thing it is important that
you know how to use them because the company that you go to work for may not
has as liberal of a stance as DuckieCorp does.  You may be required use a
particular IDE for certain tasks.

In fact, a hybrid approach is quite common.  Personally, I keep an IDE around
for its excellent debugger even if its text editor is sub-par.  At all of the
shops I've worked at the only way to correctly build the entire program is to
use the IDE's `build` button.  So I write all of my code in my preferred text
editor, using the IDE only to build and test the program.




### Further reading

More information that will give background to your understanding of this common tool-of-the-trade

*   [PyCharm IDE Essentials](https://www.jetbrains.com/help/pycharm/general-guidelines.html)
*   [Mastering PyCharm keyboard shortcuts](https://www.jetbrains.com/help/pycharm/mastering-keyboard-shortcuts.html)
*   [Does Visual Studio Rot the Mind?](http://www.charlespetzold.com/etc/doesvisualstudiorotthemind.html)



--------------------------------------------------------------------------------
## Python programming resources

I have invited you to a Canvas course called [Python
Intro](https://usu.instructure.com/courses/474722).  Please use the lessons in
there to supplement your studies as you brush up on Python basics.  Consider it
a free textbook to accompany this course.  I'll refer to it throughout this semester.

Additional resources that I recommend:

-   The official [Python3 Documentation](https://docs.python.org/3/)
-   The Python3 [Programming FAQ](https://docs.python.org/3.7/faq/programming.html)
-   Python Course.eu [Python3 tutorial](https://www.python-course.eu/python3_course.php)


----------------------------------------------------------------------------
# The Read, Eval, Print, Loop (REPL)

Perhaps Python's most important feature for the beginning programmer is the REPL.

#### REPL: Read Eval Print Loop

The REPL is an interactive environment where you can play with the language and
see for yourself how stuff works.  It is a great way to experiment with the
language and to try out new ideas.

The idea of the REPL was born in the LISP language (as so many good ideas
were), and all self-respecting "modern" languages feature this nowadays.


You enter the REPL simply by running the `python` executable with no arguments.

You are in the REPL when you see the >>> prompt.

You may also launch the REPL after your script has run by running

    $ python -i scriptname.py


Once you're in the REPL you can just try things out.

    $ python
    Python 3.7.1 (default, Dec 14 2018, 19:28:38) 
    [GCC 7.3.0] :: Anaconda, Inc. on linux
    Type "help", "copyright", "credits" or "license" for more information.

    >>> 1 + 1
    2

    >>> "yabba dabba" + " doo"
    'yabba dabba doo'

    >>> 2 * 7
    14

    >>> 2 ** 7
    128


### Two important functions: `help()` and `dir()`

Besides being a great base for experimentation, the REPL gives you access to
Python's built-in documentation through the `help()` function.  Pass to this
function a value or variable to read its documentation.  Remember `help()` any
time you have a question about how to use some aspect of the language.

    $ python
    Python 3.7.1 (default, Dec 14 2018, 19:28:38) 
    [GCC 7.3.0] :: Anaconda, Inc. on linux
    Type "help", "copyright", "credits" or "license" for more information.

    >>> help(int)
    # Shows the help for the 'int' class

    >>> help(3 + 7)
    # Idem.

    >>> help(list)
    # Shows the help for 'list' objects, including valid methods that may be
    used on lists.

    >>> help([])
    # Idem.


You can see a more compact list of possibilities with the `dir()` function.
This function gives a directory listing of members and methods on an object in
Python.  This listing is given as a list of strings, so you can use the
ordinary Python list and string operations on it:


    >>> dir(list)
    ['__add__', '__class__', '__contains__', '__delattr__', '__delitem__',
    '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__',
    '__getitem__', '__gt__', '__hash__', '__iadd__', '__imul__', '__init__',
    '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mul__',
    '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__',
    '__reversed__', '__rmul__', '__setattr__', '__setitem__', '__sizeof__',
    '__str__', '__subclasshook__', 'append', 'clear', 'copy', 'count',
    'extend', 'index', 'insert', 'pop', 'remove', 'reverse', 'sort']


    # Cut the list of methods down to the public ones
	>>> for s in dir(list):
	...     if not s.startswith('__'): print(s)
	... 
	append
	clear
	copy
	count
	extend
	index
	insert
	pop
	remove
	reverse
	sort


*   [Using the Python Interpreter](https://docs.python.org/3/tutorial/interpreter.html)
