# CS1440 - Wed Jan 16 - Module 1

# Announcements


## Free Software and Linux Club Spring Opening Social Tonight

Come hang out with people who share an interest in Linux

There will be pizza!

Tonight, 7pm @ ESLC 053




# Topics:

* Assignment 1: Caesar Cipher
* Command-line arguments
* Strings and loops




----------------------------------------------------------------------------
# Assignment 1: Caesar Cipher

*   [Section 001](https://usu.instructure.com/courses/531135/assignments/2606365)
*   [Section 002](https://usu.instructure.com/courses/527950/assignments/2606376)


----------------------------------------------------------------------------
# Command-line arguments

```
# make the list variable "sys.argv" available to your code
import sys

# print all arguments given to your program
for i in sys.argv:
    print(sys.argv[i])
```



----------------------------------------------------------------------------
# Strings and loops

The data that you read from a file is _usually_ presented as strings.

In Python, strings are a type of sequence, just like lists, ranges and tuples.
This means that the same operations which work on a list will work on a string.

Let's use the REPL to try out some of the operations given in the Python Intro:

[Python Intro: Tuples and Ranges](https://usu.instructure.com/courses/474722/pages/tuples-and-ranges)


Because strings are treated as sequences, we can use a for loop on a string.

for x in the_string:
    ...

* If I loop over `the_string`, what do you think the value of `x` will be at
  each iteration?




