import sys
import time

print("You are running python version " + sys.version)
print("hello world")
print("hello from PyCharm!!!! ;)")

print("my args are in an array that looks like this:")
print(sys.argv)

i = 0
for arg in sys.argv:
    print(str(i) + ": " + arg)
    i += 1


# Let's see what 'a' + 'b' is
c = ord('a') + ord('b')
print(chr(c))


# Print the ASCII code of each letter in the string, one by one
for l in "hello world!":
    print(ord(l))
