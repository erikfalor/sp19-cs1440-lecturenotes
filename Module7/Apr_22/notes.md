# CS1440 - Mon Apr 22 - Module 7

# Announcements


## Engineering State Assistants Needed

### If you're interested, send me an email

* erik.falor@usu.edu

Engineering State is a fun-filled 4-day summer camp for students entering their
senior year of high school. Participants explore how engineering has changed
our world and learn what earning a degree in engineering is all about. Many of
our graduates have majored in engineering and now have successful, fulfilling
careers.

    https://engineering.usu.edu/events/e-state/


I am running two Challenge Sessions at Engineering State this summer: one is an
intro to Python and programming, and the other is a Virtual Reality experience.
I need two assistants who will be available to be on campus in the daytime
from June 3rd through the 5th (Monday - Wednesday), and who meet these
requirements:

* Know at least a little bit of Python
* Good communicators with upbeat, positive personalities
* Excited to work with teenagers
* Know how to party like a multi-tape Turing Machine!


## Reminder: Please take the IDEA survey

Big thanks to those who have already left their feedback!





# Topics:
* Assignment 7 standup meeting
* Final Exam Review


----------------------------------------------------------------------------
# Assignment 7 standup meeting
        _
    .__(.)<  - Scrum!
     \___)

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?



----------------------------------------------------------------------------
# Final Exam Review

* 50 questions in total - 130 points
* Cumulative across the entire semester
* Available in the Testing Center over the entirety of Finals Week



# Problem Solving/Strategy

* Most "real world" programming is supplementing or modifying an existing code
  base, rather than writing a new solution completely from scratch

* Knowing when to use a technique (or when not to use it) is as important as
  knowing how to use it

* The definition of "Exploratory Learning" is *explicitly alloting time for
  learning new components as a general task*

* The definition of "As-needed Learning" is *searching for a component to solve
  a specific problem*

* Global variables are discouraged because it's not clear when and why their
  values change, making code difficult to understand

* Essential difficulties are inherent in the nature of the system

* Accidental difficulties are those which may be overcome with better tools



# Git

* My advice is to make frequent, small and focused commits to your repository.

* My advice is to store your different projects in separate git repositories

* Git branches let you freely and safely experiment and try out new
  possibilities in code without jeopardizing already working code

* `git blame` helps you quickly identify which developer is responsible for a
  questionable line of code

* `git bisect` embodies the strategy of "Wolf-Fence Debugging" by zeroing in on
  a commit which introduced a bug

* When you don't remember which git command to use, remember  `git help`



# Software Testing

* Software testing is the process which aims to validate and verify a software system

* A software failure is runtime behavior that is not expected

* A unit test is a fine-grained test cases designed to exercise the fundamental
  units of your program, such as functions or methods.
    
* A regression test ensures that a new software change hasn't created new
  problems or re-introduced old problems.

* Beta tests involving your user base overcomes the limitations of in-house testing

* **Verification** is the process that aims to answer the question "is my
  program doing the thing right?"

* **Validation** is the process that aims to answer the question "is my program
  doing the right thing?"

* No amount of testing will uncover all possible errors in all programs



# Refactoring 

* Refactoring is the process of restructuring existing computer code without
  changing its external behavior

* Git supports your refactoring efforts by allowing you to turn back the clock
  when you inadvertently change the program's behavior

* Unit tests support your refactoring efforts by proving to you that a new code
  change has not caused an unexpected change in the program's behavior

* A refactored program should exhibit the same outward behavior as the original
  version; put another way, after a program has been refactored, a user should
  not be able to tell a difference.
    - The point of refactoring is to improve the source code to facilitate
      future improvements 

* A "code smell" is Any characteristic in source code that indicates a deeper
  problem



# Design Patterns and Principles of Object-Oriented Design

* Abstraction means to strip away accidental details from a system, reducing it
  to the essential qualities that really matter

* Encapsulation means to hide complexity so other programmers need not be
  concerned about details irrelevant to their immediate problem

* Inheritance lets us avoid code duplication by reusing common behaviors and
  properties among related classes, and adding unique aspects to those classes
  which differ.

* Polymorphism means that two different classes have the same interface, and
  can be used interchangeably in the same situations

* A Design Pattern is A general, repeatable solution to a commonly occurring
  problem in software design

* An abstract class is a class that cannot be instantiated directly

* The Abstract Factory Pattern is useful because it collects into one place
  code related to creating related objects

* Overriding a method means that a sub-class defines a different version of an
  operation provided by its super-class



# Recursion

* A problem need not possess the essential quality of recursion for a recursive
  solution to be applied to it

* Recursion and iteration are equally expressive.
    - You may pick whichever approach provides the best balance of performance
      and elegance
    - Any recursive algorithm can be refactored into an iterative algorithm
      which uses an explicitly managed stack
    - Recursion is often applied to complex, dynamic data structures because
      such structures benefit the most from recursion

* Recursion best embodies the general problem-solving principle "Divide the
  problem"

* When designing a recursive solution you must
    1.  Identify the base case(s)
    2.  Discover operations which transform your current state into one that's
        one step closer to a base case

* *Head* recursion occurs when the recursive call comes before other processing
  in the function

* *Tail* recursion occurs when the recursive call comes after other processing
  in the function

    - "Tail Call Optimization" is when a compiler is able to transform a
      function call into a jump instrcution (just like it does in a loop)
      instead of making a new stack frame

    - Programs which take advantage of Tail Call Optimization will not exhaust
      the call stack, even in cases where the recursive calls have no end.

* V. Anton Spraul's "Big Recursive Idea" is that if you follow certain
  conventions in your coding, you can pretend that no recursion is taking place

* The two most common mistakes beginning programmers make with recursion are
  "too many parameters" and "global variables"

* When your recursive function never reaches its base case, the error is called
  Stack Overflow

* A wrapper function is a non-recursive function which makes the first call to
  the recursive function, often for convenience reasons
