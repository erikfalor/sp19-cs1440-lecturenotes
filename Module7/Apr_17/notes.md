# CS1440 - Wed Apr 17 - Module 7

# Announcements

## FSLC End of Year Social

Now with Pizza!!

ESLC 053 @ 7pm 



## Reminder: Please take the IDEA survey

Big thanks to those who have already left their feedback!


## Please leave your detailed feedback about Assignment 6 on the Canvas Survey:

* [Section 001 Assignment 6 Effort Survey](https://usu.instructure.com/courses/531135/quizzes/715551)
* [Section 002 Assignment 6 Effort Survey](https://usu.instructure.com/courses/527950/quizzes/715554)



# Topics:
* Assignment 7 standup meeting
* Questions about demo programs from 08_Mon
* Why am I making you use recursion in HW7?
* Practical considerations when using recursion


----------------------------------------------------------------------------
# Assignment 7 standup meeting
        _
    .__(.)<  - Scrum!
     \___)

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?


Make sure that your crawler program works when run from the command-line, e.g.:

    `python src/main.py https://cs.usu.edu`

This is how I and the graders will run your program.



----------------------------------------------------------------------------
# Questions about demo programs from 08_Mon

Are there any questions I can clear up about the use of these programs or how
they relate to your solution?



----------------------------------------------------------------------------
# Why am I making you use recursion in HW7?

What reasons can you come up with?

* Recursion is a powerful tool
* It can simplify your solution to a problem
* It makes repetitive problems very simple
* Recursion gives exposes you to better/alternative approaches

I want to teach you recursion to give you another problem-solving tool.  For
all of its mystery and reputation of complexity, recursion can lend itself to
very short and elegant solutions to problems which have one of these
properties:

* The data structure underlying the problem is itself recursive

* You can identify a trivial "base case" *and* an inductive step which changes
  a complex case into something that's closer to being the base case.


Problems with these properties include:

* Search algorithms (i.e. a chess or go engine)

* Parsing programming languages (your compiler or interpreter is recursive)

* Traversing a recursive data structure (like your filesystem or the internet)

However, recursion isn't just for problems that naturally have recursive
characteristics.  *Any* iterative algorithm can be converted into an equivalent
recursive algorithm, even if the above properties don't apply.



----------------------------------------------------------------------------
# Practical considerations when using recursion

See this module's [Readings and Resources](../Readings_and_resources.md)
