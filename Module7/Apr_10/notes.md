# CS1440 - Wed Apr 10 - Module 7

# Announcements

## FSLC Meeting Tonight

Tour of Open Source Software Licenses

7pm @ ESLC 053



## Computer Science Department Spring Social and Awards Dinner

Next Tuesday, 4/16 5–7 pm @ West Stadium Center


* Free Dinner - Taco Bar & Drinks from The Italian Place
* Prizes
* The RSVP list is full!  Contact cora.price@usu.edu to get on the wait list.



## FSLC Closing Social

* Come relax and de-stress before finals week sucks the fun out of life
* Pizza will be provided
* Next Wednesday, 4/17 7pm @ ESLC 053


## On-Campus Capture The Flag Event

Thursday, April 18th in HH 220. Please bring your laptop.



## Park City High School "Girls in Tech" Club Hackathon needs mentors

16 mentors are needed on April 27 (the Saturday in the middle of Finals Week)
to mentor a group as they complete a project for the competition. We have tons
of funding and will be giving away prizes to the winning groups at the end of
the event, including the mentor. We are looking for USU computer science
students with experience in App Inventor/Thunkable and/or Android Studio.

The event will be at Park City High School and we are having the mentors stick
with their teams all day and so the commitment would be around 11 hours as our
mentoring begins around 10:00.

[Sign up link](https://forms.gle/BFjx8VnfcqRMUAN78)



# Topics:

* Assignment #6 stand up meeting
* Reading discussion: Chapter 5 of *The Mythical Man Month* - "Second System Effect"
* Learn about this one weird recursion trick they don't want you to know
* Hands-on recursion



----------------------------------------------------------------------------
# Assignment #6 stand up meeting
        _
    .__(.)<  - Scrum!
     \___)

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?



----------------------------------------------------------------------------
# Reading discussion: Chapter 5 of *The Mythical Man Month* - "Second System Effect"

## Mud card questions

* Why is an architect's first work is apt to be spare and clean?
* Why is the second system the most dangerous system to build?
* How does one avoid the second-system effect?



----------------------------------------------------------------------------
# Learn about this one weird recursion trick they don't want you to know

Recursion is hard to wrap your head around at first.  Be assured that learning
recursion is like riding a bicycle.  After you do it once, you'll always be
able to do it again.  Another thing that is to your advantage is that there is
a simple trick that you can employ to turn any interative solution into a
recursive one.

In his book *Think Like a Programmer*, V. Anton Spraul introduces his Big
Recursive Idea (BRI).  The BRI is to pretend that no recursion is actually
taking place at all.  I'll illustrate by telling the story of two factorial
functions, the hard-working and industrious iterative factorial, and the lazy
recursive one.

We begin with the industrious iterative Factorial function and verify that it
works to our satisfaction.

    def factorial(n):
        print(f"At this call n = {n}")
        r = 1
        for i in range(1, n + 1):
            r *= i
        return r

    >>> factorial(0)
    At this call n = 0
    1

    >>> factorial(1)
    At this call n = 1
    1

    >>> factorial(2)
    At this call n = 2
    2

    >>> factorial(3)
    At this call n = 3
    6

    >>> factorial(4)
    At this call n = 4
    24

    >>> factorial(10)
    At this call n = 10
    3628800


Looks legit.  Next, we create a "middle-man" function which, true to form, is a
lazy bum who pawns the hard work off onto some sucker who will do it for less.

    def middleman0(n):
        print(f"I'm a lazy middle-man, and n = {n}")
        return factorial(n)


The iterative function becomes annoyed that this useless middle-man is taking
credit for his hard work and complains to their boss.  The boss tells the
middle-man that in order to be a team player he must provide *some* value to
the company.

The middle-man decides that hard work is hard, but computing trivial values of
the factorial function (for inputs 0 and 1) is easy enough to not be beneath
his dignity, though he still pawns the heavy lifting off to the iterative
function.

    def middleman1(n):
        print(f"I'm a lazy middle-man, and n = {n}")
        if n < 2:
            return 1
        else:
            return factorial(n)


The iterative function quickly realizes that the middle-man still does not add
any *real* value to the team (after all, it could already handle the trivial
base case by itself), and begins making impertinent remarks about the
middle-man function's provenance, work ethic, and worth to society at large.

Perhaps out of guilt, shame, or injured pride, our middle-man resolves to do at
least one *teensy* little bit of work by itself before foisting the rest of the
job on to the iterative function:

    def middleman2(n):
        print(f"I'm a lazy middle-man, and n = {n}")
        if n < 2:
            return 1
        else:
            return n * factorial(n-1)


"Surely this will get everyone off my back!" the self-satisfied middle-man
function exclaims to himself.  But the other function is not impressed.
However, once the boss has seen that the middle-man is capable of doing that
one little bit of work, it's easy to see that it is perfectly capable of doing
a bit more.  This time, instead of letting the middle-man shirk its duty at the
expense of its more industrious co-worker, the boss forces the middle-man to
pawn its work onto *itself*.

    def middleman3(n):
        print(f"I'm a lazy middle-man, and n = {n}")
        if n < 2:
            return 1
        else:
            return n * middleman3(n-1)


And you have a recursive solution, and everybody lived happily ever after.



----------------------------------------------------------------------------
# Hands-on recursion

## Merge Sort Algorithm

I need 15 volunteers to come fill the first 4 rows of the classroom like so:

    # # # # # # # #
     #. ,#   #. ,#
       #.     ,#
         ` # '
    
    <Demo: MergeSort.pseudo>


Tips for thinking about problems recursively:
---------------------------------------------
* Start with what you know - this means to identify the base case(s).
  The base case(s) are states of the problem for which the answer to the
  problem are self-evident.

* Divide the problem - this means to break the problem into smaller chunks
  and solve those first.  Discover operations which transform your current
  state into one that is one step closer to a base case.

* Reduce the problem - add constraints to the problem such that you end up
  excluding extraneous, unneccesary details which complicate a recursive
  solution. Problems which may be divided into pieces which do not need to
  rely on each other's state are good candidates for a recursive solution;
  such recursive solutions could be run in a distributed system taking
  advantage of parallel processing.




## Can you find recursive solutions to these problems?

Come pick out a game and a problem.  On the front of your mudcard describe an
iterative approach to solving the problem.  On the back of the mudcard describe
a recursive solution to the same problem.

* What are the pros and cons of each approach?
* Which is easier to explain to your studdy buddy (or rubber duckie?)
* Which way is easier to act out with physical pieces?


Scrabble tiles
--------------
* Detecting Palindromes ("level", "civic", "tacocat", "A man, a plan, a canal, Panama")
* Fisher-Yates Shuffle (an efficient way to mix up an ordered collection of items)
* Find all permutations of N tiles (order matters: {123, 132, 213, 231, 312, 321})
* Find all combination of N tiles  (order doesn't matter: {123} == {321})


Cards
-----
* Merge Sort (MergeSort.pseudo)
* Quick Sort
* Linear-Search
* Binary-Search


Coins
-----
* Make change for N cents using the fewest coins
* Find all ways to make change for a dollar


Towers of Hanoi
---------------
* The classical solution is recursive; can you find it?
  (Hint: begin with fewer disks)
* Can you devise an iterative solution to this classic puzzle?

