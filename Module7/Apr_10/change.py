USA_ideal = [50, 25, 10, 5, 1]
USA_realistic = [25, 10, 5, 1]
USA = USA_realistic
EU = [50, 20, 10, 5, 2, 1]


# Instead of popping coins from the list, we will maintain an index into the list
def iChangeNondestructive(amount, coins):
    """Keep track of the largest coin we can use from our stack"""
    top = 0
    change = []
    while amount > 0:
        if amount >= coins[top]:
            amount -= coins[top]
            change.append(coins[top])
        else:
            while amount < coins[top]:
                top += 1  # We have to manually keep track of which coins to use
    return change


# What does the recursive solution look like?  Use Anton's Big Recursive Idea
# to evolve it
def rChange(amount, coins):
    if amount > 0:
        if amount >= coins[0]:
            return [coins[0]] + rChange(amount - coins[0], coins)
        else:
            return rChange(amount, coins[1:])
    else:
        return []
