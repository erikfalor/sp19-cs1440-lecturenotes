# CS1440 - Mon Apr 08 - Module 7

# Topics:
* Assignment #6 pointers and help
* Assignment #7: Recursive Web Crawler
* demo_urlparse.py: Anatomy of a URL
* demo_requests.py: GETting data from the web
* demo_beautifulsoup.py: Finding order in chaos



--------------------------------------------------------------------------------
# Assignment #6 pointers and help


## Q: What difference does it make...

... to change the # of iterations in a fractal configuration file?

**A: All of the difference in the world!**  By varying the number of iterations
in the `data/spiral1.frac` configuration file we were able to discover a whole
new world of detail in an otherwise boring image.

See for yourself the differences between these files:

* [100 iterations](100spiral1.png)
* [256 iterations](256spiral1.png)
* [512 iterations](512spiral1.png)
* [1024 iterations](1024spiral1.png)

The other marvelous thing I want you to notice about this is that we were able
to travel farther into this fractal *without changing the source code*.
Imagine what you'd need to do to the starter code if you wanted to do this
*before* you refactored Assignment 5.



## What is the best way to get information into your Fractal's `count()` method?

The Julia fractal's `count()` method can use extra information that the
Mandelbrot fractal doesn't need.  I want to maintain the polymorphic qualities
of my Fractal sub-classes such that they are drop-in replacements for each other.

* Is it worth it to use an `if`/`elif`/`else` decision tree in the middle of my
  ImagePainter's `paint()` method?

If I change the signature of the `count()` method of each sub-class of
`Fractal`, my fractals are no longer drop-in replacements of each other.  The
complexity of the `if`/`elif`/`else` decision tree is the very thing I was
trying to avoid with Polymorphism in the first place.

* Is it worth it to use a global variable to avoid changing the signature of
  `count()`?
    + No, because globals are icky and error-prone.

* Is it worth it to use a class variable to avoid changing the method
  signature?
    + No, because when I want to use two instances of a Julia fractal at once,
      they would share this single value.  I would scarecely be better off than
      with a global variable.

*nb.* Examples of **class** variables are found in the Sequences examples shown
in the lecture Friday, April 5th.  The variables `phi`, `psi` and `two_sqrt2`
pertain to the class `Pell`, and are shared amongst all Pell objects in the
program.

    class Pell(Sequence):
        phi = (1.0 + sqrt(2.0))
        psi = (1.0 - sqrt(2.0))
        two_sqrt2 = 2.0 * sqrt(2.0)


The correct approach is to store special values needed by a particular
fractal's `count()` method inside each *instance* of that fractal.  Give the
value to the constructor of the fractal object and assign it onto the `self`
reference.  One solution proposed in class today is to store the Fractal
Configuration dictionary in each Fractal instance:

    class Julia(Fractal):
        def __init__(self, cfg):
            self.cfg = cfg
            ...


----------------------------------------------------------------------------
# Assignment #7: Recursive Web Crawler

* [Section 001](https://usu.instructure.com/courses/531135/assignments/2606368)
* [Section 002](https://usu.instructure.com/courses/527950/assignments/2606379)

This assignment will see you combining three 3rd party Python libraries into a
web-crawling bot.  I've written some demo programs to help you become familiar
with each library.  I expect you to hack on these programs to answer your own
questions about how to use these libraries in your own program.

* `demo_urlparse.py`: The anatomy of a URL
* `demo_requests.py`: GETting data from the web
* `demo_beautifulsoup.py`: Finding order in chaos
