#!/bin/env python3

# if this doesn't work because of a missing package, `conda install requests`
import requests

def demo(url):
    try:
        # TODO: what if an error happens?
        response = requests.get(url)
        print(response.text)

    except Exception as e:
        print(f"Failed to get {url} because {e}")

demo('http://example.com')

demo('https://cs.usu.edu/about/contact')
