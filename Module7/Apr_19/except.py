import time


print("[H[2JReloaded except.py...")

def doIt(string):
    while True:
        try:
            eval(string)  # This is risky business right here
            print("starting over...")
        except Exception as e:
            print(f"You got an error! {e}")

        time.sleep(1)
