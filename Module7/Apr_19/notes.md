# CS1440 - Fri Apr 19 - Module 7

# Announcements

## Tutor Lab End of Semester Schedule

The tutor lab will close for the semester at the end of the day on Tuesday the
23rd, which is the last day of classes.



# Topics:
* Thinking like a Programmer
* Wisdom vs. Knowledge
* Catalog your strengths and weaknesses
* Suggested activities to continue your growth as a programmer
* Things which have made me a better programmer



--------------------------------------------------------------------------------
# Thinking like a Programmer

This lecture is about applying our problem solving strategies to the problem of
becoming a better programmer.

As stated in the course syllabus, the purpose of this course has been to teach
you how to analyze, strategize, and form a problem-solving plan; manage
complexity; improve software quality; leverage existing solutions; and select
the best tool to solve the problem at hand.

As we wrap up this semester, let's take a moment to think back upon some of the
new programming skills you have learned about:


* Command-line arguments
* File I/O - how to read files to/from the disk
* Git!  Git all of the things!
* Break a bigger problem down into small tractable pieces
* Design before you code
    * Don't over-design your solution
* UML diagrams
* Refactoring
* Different programming paradigms exist (not everything looks like a Python solution)



--------------------------------------------------------------------------------
# Wisdom vs. Knowledge

Having learned these things has increased your *knowledge* about programming.
There are still many techniques that you'll need to learn about, but this is a
good first step.  Another important ingredient, *wisdom* comes only with
experience and time.


## The difference between wisdom and knowledge

Knowing *when* to use a technique is just as important as knowing *how* to do it

> Knowledge is knowing that a tomato is a fruit, but wisdom is knowing to not
> put a tomato into a fruit salad.
>
> -- Somebody wiser than I

* What is the cost of applying a technique inappropriately?
    + It could take longer - both from a computational perspective, and from a
      productivity perspecive
    + You can introduce bugs - subtle design errors instead of


* How will you know when you should do something in a particular way?
    + It's not going as easily as planned
    + Your problems are more "accidental" in nature than "essential"
    + You find yourself running into lots of "unintended consequences" (the bad
      kind of consequences)


We all understand how to increase our knowledge, that's as simple as opening a
book or finding a blog.  But how can you intentionally increase your wisdom?
One must have experiences to learn *when* and *why* to do things in a particular
way.  Here is one way you can find those experiences that will give you the best
return on your time investment:


#### Exploratory Learning
Explicitly allot time for learning new components as a general task.

#### As-needed Learning
Search for a component to solve a specific problem facing you now.


Much of the time you will spend engaged in learning as a professional programmer
will be in the "as-needed" category.  When your boss asks you to do something
that you've never done before, you now *need* to figure it out.

More rewarding is the learning you will do (usually on your own time) pursuing
subjects that you find most interesting.  One result of this is that you may
become *the expert* of that thing in the office, and more tasks that you are
interested in doing will begin to come your way.  Or, perhaps it won't work out
quite so well...

[The Expert](https://www.youtube.com/watch?v=BKorP55Aqvg)



--------------------------------------------------------------------------------
# Catalog your strengths and weaknesses

Another way to identify areas for you to focus on is to make a list of your
strengths and weaknesses, and devise activities to turn your weaknesses into
newfound strengths.



## What are your weaknesses?

* Convoluted designs
* Can't get started
* Don't finish anything
* Fail to test
* Overconfident
* Weak at X

Are there any others?


## What are your strengths?

* Eye for detail
* Fast learner
* Fast coder
* Never gives up
* Super problem-solver
* Tinkerer
* Mull it over
* Curious!!!
* Able to work on many pieces at once
* Good at doing up-front research
* Find all of the ways *not* to do it

What else are you great at?

Curiosity and an unstoppable drive to learn and improve are indispensable
attributes of the fledgling Computer Scientist, and their cultivation is the
focus of this course.

> Do only what only you can do
>
> -- Edsger W. Dijkstra



--------------------------------------------------------------------------------
# Suggested activities to continue your growth as a programmer

The good news is that you can minimize your weaknesses and increase your
strengths through study and practice.

If there were topics in this class or other classes which you don't feel that
you possess a mastery of, spend a little bit of time on them over the Summer so
that when you return you'll feel confident and deserving of your place in the
next challenges which will come your way, whether those are new classes or an
internship or a new career.



## Curate a portfolio of programming artifacts

Keep every bit of code you ever write.  This is easier than ever if you're
using git.

* It's nice to look back upon your old programs and to see that you've
  improved.

* Someday you'll face a problem and say to yourself "I think I've solved this
  once before".  If you are keeping track of your code you'll be able to reach
  into an ever-growing library of past solutions and find something that may be
  adapted to your current task.

One of my greatest regrets is that I have long since lost the code to the very
first programs I ever wrote in CS1 and CS2 (no git in those days).  I do have
code from CS3 and onward, and every now and then I'll look at that code and
laugh at it.



## Explore the world of Open Source software

Most "real world" programming is not writing a new solution completely from
scratch, but rather supplementing or modifying an existing code base.

Between GitLab, Bitbucket and GitHub it is easier than ever to get involved
with an Open Source software project.  Find a project that you're curious about,
clone the code, and dive in.  You will learn just as much from *reading* code as
you do from *writing* code.


## Learn a new programming language

[99 Bottles of Beer](http://www.99-bottles-of-beer.net/)

This will be beneficial to you for so many reasons:

1. When your only tool is a hammer, you'll have sore thumbs.  I first learned to
   think in C++.  It took me years to reverse the brain damage that caused.  But
   now I can think about problems in higher-level terms because I understand
   alternative ways to approach them.

2. You will gain context.  "Why does language X have this feature?"  You can
   reason about trade-offs in the design of languages because you will have more
   and varied points of comparison.  You have available more handholds by which
   you can understand new concepts.  Seeing new code (and the same code in
   different styles) is not as perplexing as it once was.

3. Each new language will teach you a bit more about logical thinking and
   computation.  The pace of learning accelerates with each new language you
   learn.

4. Like human languages, programming languages are embedded within their unique
   cultures.  My advice is to learn at least one programming language which is
   embedded in an "academic" culture (How can you tell if a language is
   academic?  Good signs are that its website doesn't look very good, and it was
   created as someone's dissertation).

I've personally learned very much from the Scheme hackers that I've hung out
with.  The level of discourse is so much higher there than in other places.
There's probably something to gain from every language culture, but make sure
that you are going to the Opera instead of a NASCAR event once in a while.

The point isn't to learn just those things that look neat on your résumé, but
to learn things that are neat to you.  Besides, the more languages you know,
the more of a language snob you get to become.

![The programmer hierarchy](hierarchy.jpg)

![The programmer hierarchy for LISP hackers](lisp_hierarchy.jpg)



## Take part in coding competitions/challenges

There are plenty of these out there; here are a few I've enjoyed:

* https://programmingpraxis.com/
* https://projecteuler.net/
* http://exercism.io/
* https://adventofcode.com/

Combine these challenges with a new language for double the benefit.  In fact,
when you pick a new language it's helpful to have some project in mind to focus
your effort on.  These coding challenges serve that need well.



## Connect with the broader community of programmers

By this I mean "connect with programmers IRL".  Get involved with other hackers
at meetings in meatspace.  There are plenty of clubs and events on campus
during the school year.

Networking is so crucial to your career, yet it is something which we invest
too little energy into.

### Utah Python User Group
Look for the Logan location about 2/3 of the way down:

[Utah Python User Group](http://utahpython.org/#/locations)


--------------------------------------------------------------------------------
# Things Which Have Made Me a Better Programmer

## Learning the History of the Field

Learning about the challenges of the past gives us the context to understand
the status quo, and informs our efforts to improve it. In general, things aren't
the way they are because the people who came before were idiots, and you
probably aren't the first person to have that cool idea.

Studying the earliest calculation devices from the days of Charles Babbage
through the Manchester "Baby" and the ENIAC not only gives one a better
appreciation for modern technology, but encourages us to better use it. We often
think that through the magic of Moore's law that we have moved past the concerns
and challenges of the first generation of computers. But we'd do well to
remember their struggles because:

1. Our predecessors had to do really clever things out of necessity. We don't
   work under the same privations as they did, and so we don't think very much
   or very hard about what we're doing. We justify this to ourselves because
   we're not working with crappy plugboards or punch cards.

   It is true that we shouldn't have to think very hard about clever
   algorithms. The reason is not because our hardware is better, but because
   those guys and gals were good enough to write papers and books about it.

   There is a point where over-thinking the situation becomes a bad thing.
   I argue that we are usually very far away from that place.

2. Moore's law is not a natural law that governs or guarantees future
   performance. It just an observation that happens to explain how things have
   worked so far. We've already had to employ cleverness to work around the
   limitations we've already run into. Relying on it to mitigate out problems
   doesn't work now, and becomes less viable as time goes on.

   Also, Gordon Moore himself notes that:

> Moore's law is a violation of Murphy's law.
> Everything gets better and better.
>
>                     -- "Moore's Law at 40 - Happy birthday"
>             The Economist 2005-03-23.  Retrieved 2006-06-24

Computer Science is a young field. There is no excuse for being ignorant of its
beginnings while there are still people living who were born before it began.


## Understand and Appreciate the Fundamentals of Computation

When I was younger, I thought that computers were magical things, and I didn't
want the magic to end by uncovering the wizard behind the curtain. Now I am
ashamed of myself that I ever felt that way toward gaining knowledge.  The more
I know about how these things work, my youthful awe is replaced by a sense of
disbelief that everything works as well as it does. Computers hold very nearly
the same sense of magical wonderment as before, it is just tempered by cynicism
now (or, that's just a result of being an old grump). I am still in awe, but I
can do something about it.

Writing a compiler made me a better user of compilers and languages. Learning
assembly language enables me to debug and fix problems that have puzzled people
for years. Learning Artificial Intelligence lets me understand how seemingly
"magical" things can be possible through the application of a lot of data to
statistical analysis.

You will write better code faster because you aren't fighting with your own
faulty assumptions. Nor will you waste time trying to get the computer to do
something that it just can't do.

Many say that they do not wish to be bogged down by lots of fiddly details.
Neither do I, but for me it is a conscious choice that I can make, not a
limitation imposed upon me by the circumstance of my ignorance.
