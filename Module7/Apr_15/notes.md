# CS1440 - Mon Apr 15 - Module 7

# Announcements

## Computer Science Department Spring Social and Awards Dinner
Tomorrow, 4/16 5–7 pm @ West Stadium Center

* Free Dinner - Taco Bar & Drinks from The Italian Place
* Prizes
* The RSVP list is full!  Contact cora.price@usu.edu to get on the wait list.



## FSLC Closing Social
* Come relax and de-stress before finals week sucks the fun out of life
* Pizza will be provided
* Wednesday, 4/17 7pm @ ESLC 053



# Topics:
* Assignment 6 retrospective



----------------------------------------------------------------------------
# Assignment 6 retrospective

[Plus/Minus Voting](http://www.funretrospectives.com/plus-minus-voting/)

* Each student gets 3 PLUS and 3 MINUS votes
  (each vote is represented by `+` or `–` on the sticky note).

* Place one vote on each card.

    – `+` represents agreement with a note, and you want to talk about it

    – `–` represents disagreement with a note, and you want to talk about it

* Vote on the items that you want to discuss about. The items with most votes
  will be picked up first.


Please leave your detailed feedback about this assignment on the Canvas Survey:

* [Section 001 Assignment 6 Effort Survey](https://usu.instructure.com/courses/531135/quizzes/715551)
* [Section 002 Assignment 6 Effort Survey](https://usu.instructure.com/courses/527950/quizzes/715554)
