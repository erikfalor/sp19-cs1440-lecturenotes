#!/bin/sh

# Change this to your favorite terminal if you don't have terminology
TERMINAL=terminology

# 1 argument is required
if [ $# -eq 0 ]; then
    echo "Usage: $0 PROGRAM_NAME"
    exit 1
fi

# start up the program in the background & note its PID

case $1 in
    *.class)
        java ${1%.class} & ;;
    *)
        if ! [ -x $1 ]; then
            echo $1 is not an executable program
            echo "Usage: $0 PROGRAM_NAME"
            exit 1
        else
            ./$1 &
        fi
        ;;
esac
PID=$!

# spawn a new terminology, running the watcher script with the background PID
$TERMINAL -e "sh proc_vm_watcher.sh $PID" 2>/dev/null &

read WAIT_FOR_IT
kill $PID
