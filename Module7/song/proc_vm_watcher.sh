#!/bin/sh

# hide the cursor
tput civis

# Set Terminology font to 32 pts
[ -n "$TERMINOLOGY" ] && /bin/echo -e -n "\e]50;:size=43\e\a"

if [ $# -gt 0 -a -f /proc/$1/status ]; then
    #echo watch -n 0.5 grep -E 'Vm|Name' /proc/$1/status
    watch -d -e -n 0.5 grep -E '"Vm|Name"' /proc/$1/status
else
    echo "Usage: $0 PID"
fi
