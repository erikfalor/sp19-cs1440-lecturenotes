# Using watcher.sh

    Usage: watcher.sh PROGRAM_NAME

`watcher.sh` will run a program in the background and launch a new instance of
Terminology running `proc_vm_watcher.sh` to watch memory use of the background
process.

Press ENTER in `watcher.sh`'s window to send `SIGINT` to the backgrounded
process.
