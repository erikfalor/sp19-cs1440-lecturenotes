;;; The Scheme code in this file is compatible with Chicken Scheme version 5
;;;
;;; It may require tweaking to run in a different Scheme implementation.

(import
  srfi-1   ; circular-list
  srfi-18) ; thread-sleep!

(define *DELAY_SEC* 0.00001)

(define lyrics
  (apply circular-list (string->list
"This is the song that never ends
It just goes on and on my friends
Some people started singing it, not knowing what it was
And they'll continue singing it forever just because
")))


(define sing
  (lambda (song)
    (print* (car song))   ; The print* function is Chicken-specific; it flushes STDOUT after each write.
    (thread-sleep! *DELAY_SEC*)
    (sing (cdr song))))


;; If this program is compiled, immediately call function `sing`
;; Otherwise we're in the REPL, and await the user's input
(cond-expand
      (compiling
        (print "This is the Scheme version\n"
               "Singing recursively\n")
        (sing lyrics))
      (else #t))
