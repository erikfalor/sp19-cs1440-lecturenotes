#include <unistd.h>
#include <iostream>
#include <string>
#include <sys/types.h>

# define DELAY_uS 1000


struct Letter {
    char c;
    Letter* next;

    Letter(char i) : c(i), next(nullptr) {};
};


Letter* circular_list(std::string s) {
    Letter *head = nullptr;
    Letter *f = nullptr;

    for (char c : s) {
        if (!head) {
            head = new Letter(c);
            f = head;
        }
        else {
            f->next = new Letter(c);
            f = f->next;
        }
    }

    f->next = head;
    return head;
}


void iterative(Letter *cl) {
    while (cl) {
        std::cout << cl->c << std::flush;
        usleep(DELAY_uS);
        cl = cl->next;
    }
}



void printer(char c) {
    (void)(std::cout << c << std::flush);
}


void recursive(Letter * cl) {
    usleep(DELAY_uS);

    // using cout in this function causes a stack allocation
    // casting cout's return type to void didn't have an effect
    //     (void)(cout << cl->c << flush);
    // neither did putting the call to cout into its own scope.

    // To make this function tail-call optimized, comment out this line...
    std::cout << cl->c << std::flush;

    // And uncomment this line:
    // printer(cl->c);

    return recursive(cl->next);
}


std::string lyrics = "This is the song that never ends\n"
        "It just goes on and on my friends\n"
        "Some people started singing it, not knowing what it was\n"
        "And they'll continue singing it forever just because\n";


int main(void) {
    std::cout << "This is the C++ version\nPID: " << getpid() << std::endl;

    Letter *cl = circular_list(lyrics.c_str());

    bool recur = true;

    if (recur) {
        std::cout << "Singing recursively\n\n";
        recursive(cl);
    }
    else {
        std::cout << "Singing iteratively\n\n";
        iterative(cl);
    }

    return 0;
}
