# CS1440 - Fri Jan 11 - Module 0

# Announcements

Complete the Class Rules & Syllabus quiz by midnight Jan 13 or you will be
dropped from the class.



# Topics:
* "Get to know you" mud card activity
* Apply: Solve the Integer Knapsack Puzzle
* Problem Solving Strategies
* Unix/Windows command line basics


----------------------------------------------------------------------------
# "Get to know you" mud card activity

I'll often ask you to turn in a "mud card" at the end of class.  The name comes
from "muddiest point".  You will reflect upon what was discussed in a lecture
and write a brief note explaining what you did not understand or a new question
raised by what you learned.

This serves the following purposes:

1. Provides a quick head-count of who attended today
2. Putting into writing what you learned in a day increases recall
3. Lets me know which topics were well received, and which need another approach
4. Gives another opportunity for you to ask a question in a non-intimidating way


### Today I want to get to know you

On a sheet of paper please write the following:

1. Today's date, your name and A#
2. What is your class (Sophomore, Junior) and major?
3. Why are you enrolled in this course?
4. What is one thing you *like* to do?
5. What is one thing you *really do not* like to do?
6. What is your favorite meme?
7. Names of at least 3 of your neighbors and something unique or interesting about them
8. Who are your study buddies in this class?
9. What questions do you have about git or this class?


----------------------------------------------------------------------------
# Apply: Solve the Integer Knapsack Puzzle

> The knapsack problem or rucksack problem is a problem in combinatorial
> optimization: Given a set of items, each with a weight and a value, determine
> the number of each item to include in a collection so that the total weight is
> less than or equal to a given limit and the total value is as large as
> possible. It derives its name from the problem faced by someone who is
> constrained by a fixed-size knapsack and must fill it with the most valuable
> items.
>
> -- https://en.wikipedia.org/wiki/Knapsack_problem

Here are two frames each 127 inches in length.  Each team's challenge is to
find a combination of their blocks which *exactly* fits the frame, or in other
words, adds up to 127".

As with the puzzles we did on Monday, I want you to be mindful of the process
you take to find a solution.  As they say it is the journey, not the
destination that matters.


Blue Team
=========
Fill up the frame with the big pieces first to leave a smaller gap to solve
Leave yourself more options later on (more small pieces to try)

White Team
==========
Observe that the frame is an odd length, so we can only have an odd number of
    odd-lengthed blocks


----------------------------------------------------------------------------
# Problem Solving Strategies

Q: What is an algorithm?

A: a specific set of steps to get to a solution to a problem


Q: What is a meta-algorithm?

A: Steps to develop a specific set of steps to get to a solution to a problem



Think Like A Programmer
=======================
In this class we will practice techniques which apply to any and all
programming situations.  Some tasks will also require specialized approaches,
but there is a core set of meta-algorithms which underlie their proper use.

A great resource to learn these meta-algorithms is the book "Think Like A
Programmer" by V. Anton Spraul.  This was a required text in this course last
semester, and is still available as an e-book through the school library.

    https://ebookcentral-proquest-com.dist.lib.usu.edu/lib/USU/detail.action?docID=1137575

Due to the switch away from C++ to Python I am unable to continue using this
book for this class.  Nevertheless, I *highly* recommend that you read it for
free while you're a student here.  I would also *highly* recommend buying a
copy of your own to keep - it's a really great book.


General Problem-Solving Techniques
==================================
* Always Have a Plan
  Look for patterns, understand what you're getting into.
  Identify subtasks within the problem, and decide the order in which they must
  be addressed.


* Divide the Problem
  Break the problem into do-able chunks.  Most problems naturally contain
  smaller sub-problems.

  Solving these pieces will give you a sense of progress.


* Reduce the Problem
  Put constraints on the problem to rule out large sets of possibilities.
  Reducing the amount of ground you need to cover is the difference between a
  naive brute-force approach and an elegant solution.



* Start with What You Know
  Begin with the easy sub-problems, saving the unknowns for later.  Often
  you'll find that the solutions to the sub-problems you can grasp will unlock
  your understanding of the trickier ones.


* Restate the Problem
  If you're able to put it into your own words, you have a better understanding
  of it.  By putting the problem into your words you'll notice features or
  constraints which weren't visible before.


* Look for Analogies
  Find another familiar problem (that you know how to solve) which is similar
  to this one.

  As you write more programs and solve more problems, keep an inventory of
  code.  You never know when that program you wrote N years ago will come in
  handy!


* Experiment
  Be willing to try different approaches, don't get too invested in any one
  approach because you may need to throw it out and start over.

  Python is a great language for experimentation because you can quickly write
  a prototype program without investing too much time into it.

  REPL - Read Eval Print Loop


* Don't get Frustrated
  If you have properly broken up the problem and are following a plan, you can
  avoid feeling lost in a trackless wasteland with no sense of direction or
  progress.

  Take breaks when you are feeling stuck and switch to another task.  How often
  have you found the answer you are looking for while in the shower or driving
  home from work?  Put your downtime to work!


--------------------------------------------------------------------------------
# Unix/Windows command line basics

See [Module 0 Readings and Resources](../Readings_and_resources.md) for a table
of the most common commands.
