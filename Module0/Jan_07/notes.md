# CS1440 - Mon Jan 07 - Module 0


# Announcements


## Silicon Slopes Conference

https://siliconslopes.com/



### Logan Silicon Slopes Meetup Group

https://siliconslopes.com/events/details/silicon-slopes-logan-presents-building-a-software-company-in-cache-valley/#/




## DC435 Cybersecurity Meetup group

https://dc435.org/about

> DC435 is a Defcon group of people in Cache Valley, UT who are passionate
> about information security. It is made up of students, professionals,
> researchers, and hobbyists that meet and discuss, learn, and interact with
> like minded people.
>
> DC435 is always free and open to everyone regardless of skill, age, career,
> gender, etc.
>
> If you want to present or request topics please email info(at)dc435(dot)org
>
> The group is community-driven, and allows its members to be active so they
> can learn or teach about a subject they are passionate about.



## Free Software and Linux Club meetings

This semester the FSLC will be meeting on Wednesdays in ESLC 053 @ 7PM.

Our first meeting is this Wednesday night.  The club is a good place to gain
skills and experiences that you are not likely to find in the classroom.
Together we'll learn and do things of *practical* importance that will make a
big difference to your resume.



# Topics:

* You're hired!
* Problem-Solving Activity


----------------------------------------------------------------------------
# You're hired!

Welcome to DuckieCorp, the consultancy firm for programmers who talk to
themselves.

The purpose of this course is to prepare you to become a competent
problem-solver who understand and uses the best Software Engineering
techniques.

Assignments in this course are meant to give you an idea of what working in the
real-world is like.  You have been brought in to DuckieCorp as a software
development intern to gain experience and to prepare you for a full-time
career.


## Class Rules

As part of your new-employee orientation you will need to read and understand
the DuckieCorp Employee Handbook (Syllabus and Class Rules).  There will be a
quiz.  The quiz doesn't count against your grade and you may take it as many
times as you need.  You will be unable to proceed in the course until you
complete the quiz with a 100% score, and failure to complete the quiz before
the due-date will result in your immediate termination from DuckieCorp.

The Syllabus is found on Canvas, and the class rules are on Bitbucket.

* [Lecture Notes on Bitbucket](https://bitbucket.org/erikfalor/sp19-cs1440-lecturenotes/src/master/)
* [Ten Thousand](https://xkcd.com/1053/)



## TAs and the Tutor Room

Professional software development is a competitive field; one might say it's
sink or swim.  Unlike many software engineering firms, DuckieCorp goes to great
lengths to keep its employees afloat (it's in our DNA).

To help you succeed in the face of these challenges, DuckieCorp offers the
following resources:


### Open-door policy

You are not only welcome to drop in to the office of DuckieCorp's CEO, but you
are encouraged to do so.  My office is on the 4th floor of Old Main, room 421.
My dedicated office hours are posted in the Syllabus, but you are welcome to
drop by anytime I'm in.


### Teaching Assistants

* Anny Chen
* Fei Xu
* Paxton Alexander

You will find their contact information and office hours in the Syllabus


### Tutor Room Hours

DuckieCorp is so committed to your success that there is even a tutor room
right across from the CEO's office where you are able to take your questions
and work on your programs.

Old Main 4th floor, room 419

Mon - Fri: 10AM to 9PM
Sat: 11AM to 9PM



----------------------------------------------------------------------------
# Problem-Solving Activity

Take 10 minutes to join up with your classmates who have the same 
[Sam Loyd puzzle](Sam_Loyd_Puzzles/) as you and work to find a solution.  Some
of the puzzles are easier than others, and most groups will not find the
correct solution in such a brief time.

Your goals are:

* Make some friends and be on the look-out for your Study Buddies
* Become mindful of the process of problem solving



Nominate one person from your group to be the spokesperson who will
describe your puzzle to the class and explain your group's approach
to solving it.

* Did you find a solution to your puzzle?
* How did you find your solution?
* Did you get stuck?  How did you know that you were stuck?
* Did you get unstuck?  What did you do to overcome your obstacle?
