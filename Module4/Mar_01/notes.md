# CS1440 - Fri Mar 01 - Module 4

# Announcements

## Next Week's Assigned Reading

* Wednesday Mar 6: Chapter 6 of *The Pragmatic Programmer* - "While you are coding"
* Chapter 2 of *The Mythical Man-Month* - "The Mythical Man-Month"



## Next FSLC meeting

### Scheme: the reason your programming language has any redeeming qualities at all

> A language that doesn't affect the way you think about programming,
> is not worth knowing.
>
> -- Alan Perlis

Take a step towards enlightenment by exploring Scheme, a practical and simple
dialect of LISP.  This talk will demystify those concepts functional
programming novices find the most confusing, giving you a whole new way of
thinking about programming.

March 6th, 7pm
ESLC 053





# Topics:
* Basic Definitions of software reliability
* Errors vs. Faults vs. Failures
* What is "Turing's Curse"?


--------------------------------------------------------------------------------
# Basic Definitions of software reliability

## Phase 1

* Form small groups of 3-6 students with papers of the *same color*
* Read the passage
* Discuss the questions


## Phase 2

* Form *new* small groups of 3-6 students, 50/50 split of colors
* Explain to your counterparts of opposite color the conclusions your original
  group came to


## Phase 3

* Randomly selected students will explain their findings to the class




Laprie J.C. (1992) Dependability: Basic Concepts and Terminology. In: Laprie
J.C. (eds) Dependability: Basic Concepts and Terminology. Dependable Computing
and Fault-Tolerant Systems, vol 5. Springer, Vienna


## Dependability

> **Dependability** is defined as the trustworthiness of a computer system such
> that reliance can justifiably be placed on the service it delivers [Car 82].
> The service delivered by a system is its behavior as it is perceived by its
> user(s); a user is another system (human or physical) which interacts with
> the former.
>
> Depending on the application(s) intended for the system, different emphasis
> may be put on different facets of dependability, i.e. dependability may be
> viewed according to different, but complementary, properties, which enable
> the attributes of dependability to be defined:
>
> * with respect to the readiness for usage, dependable means **available**;
> * with respect to the continuity of service, dependable means **reliable**;
> * with respect to the avoidance of catastrophic consequences on the
>   environment, dependable means **safe**;
> * with respect to the prevention of unauthorized access and/or handling of
>   information, dependable means **secure**.

## How does **reliable** differ from **available**?

* You roommate is available, not not reliable


## How does **safe** differ from **secure**?

* Security is protection against other users
* Safe means "if this program crashes nothing is going to get hurt"


## Errors, Faults and Failures

> A system **failure** occurs when the delivered service no longer complies
> with the **specification,** the latter being an agreed description of the
> system's expected function and/or service. An **error** is that part of the
> system state which is liable to lead to subsequent failure: an error
> affecting the service is an indication that a failure occurs or has occurred.
> The adjudged or hypothesized cause of an error is a **fault**.
>
> The development of a dependable computing system calls for the *combined*
> utilization of a set of methods which can be classed into:
>
> * **fault prevention**: how to prevent fault occurrence or introduction;
> * **fault tolerance**: how to provide a service complying with the
>   specification in spite of faults;
> * **fault removal**: how to reduce the presence (number, seriousness) of
>   faults;
> * **fault forecasting**: how to estimate the present number, the future
>   incidence, and the consequences of faults.
>
> Fault prevention and fault tolerance may be seen as constituting
> dependability **procurement**: how to *provide* the system with the ability
> to deliver a service complying with the specification; fault removal and
> fault forecasting may be seen as constituting dependability **validation**:
> how to *reach confidence* in the system's ability to deliver a service
> complying with the specification.


--------------------------------------------------------------------------------
# Errors vs. Faults vs. Failures

The terms error, fault and failure are often used interchangeably, but do have
different meanings:

* A failure is the unacceptable departure of a program operation from program
  requirements

* A fault is a software defect that causes a failure

* An error (bug) is usually a programmer action or omission that results in a
  fault


[Stack Overflow](https://stackoverflow.com/a/47963772) has a code example of
this concept.



### Discussion

If the formal description of your system is not precise enough, you may have
many errors (bugs) which do not rise to the criterion of failures due to a
technicality.  If they don't breach the expectation set forth by the
specification, the error isn't *technically* a failure.  I would consider this
to be a failure of your specification.


There are really only two ways your program can go wrong:

1. Your specification contains a flaw, contradiction or omission
2. Your code contains a flaw, contradiction or omission

Isn't that reassuring?


If you use an implementation as your specification (e.g. as in the case of the
Ruby programming language created by Yukihiro Matsumoto), you may enshrine
errors in the code into the specification.  Remember, no program may be said to
be free of errors; the best we can hope for is that we have excised all errors
we have detected.



--------------------------------------------------------------------------------
# What is "Turing's Curse"? (15 mins, 12 mins at 1.25x)

Looking back on 30 years of programming: there's nothing new since 1983.

[OSCON 2013 talk by John Graham-Cumming](https://www.youtube.com/watch?v=hVZxkFAIziA)


## TL;DW
* There is nothing new under the sun - all of the technologies we know and love
  were invented by 1983

* Programmers spend too much of their time on bugs

* Unreliability is what you need to work on next.  We need to
    - help programmers *make fewer* mistakes
    - help programmers *find their* mistakes


#### Turing's Curse: There are certain things that machines, provably, are not going to be able to do for us.

There are limits to what computers can do for us.

* On the one hand, we aren't going to be able to make tools that will be able
  to solve all of our problems :(

* On the other hand, it means that humans will still have some utility after
  the robot overlords take over the world ;)
