# CS1440 - Wed Feb 27 - Module 4

# Announcements

## Next Week's Assigned Reading

* Wednesday Mar 6: Chapter 6 of *The Pragmatic Programmer* - "While you are coding"
* Chapter 2 of *The Mythical Man-Month* - "The Mythical Man-Month"



## FSLC "Ricing" Night

Wednesday Night 7pm room 053 in ESLC is our ricing night!  You heard me right,
Ricing.  I do not mean throwing small grains at wedding.  Or adding rice to all
your meals.  I am referring to customizing your machine to look the way you
want it to so you gain street cred.  Lab cred?  You know what I mean.



## HackUSU March Madness Month Long Mini-Hackathon

Come Create a Project, Eat Food, and Win Prizes! 

*Too busy or started too late?*  Create something simple and show up to the
judging for a chance to win an award or raffle. Education will be documented in
judging so do not be afraid if you think you don’t enough, you just might WIN!

| Important Date    | Activity
|-------------------|--------------------
| Monday March 4th  | Opening, team-forming (optional), project working, food
| Monday March 18th | Project working, food
| Monday March 25th | Project working, food
| Monday April 1st  | Judging, awards, food




## DC435 Capture The Flag

Thursday, March 7th @ 7pm

Bridgerland Main Campus - 1301 North 600 West - Room 840

Make sure you bring a laptop with RDP (Remote Desktop Protocol) and/or SSH
clients (e.g. PuTTY) so you can play!





# Topics:
* Correctly submitting your work
* Example of a transparent PNG
* Discuss Chapters 6,7 of *The Pragmatic Programmer*
* Unit tests and Assignment 4.1


----------------------------------------------------------------------------
# Correctly submitting your work

Part of participating in this class is following the guidelines for assignment
submissions.  You are allegedly adults now, so you must take up the
responsibility for this.  Moreover, there are just too many of you for me and
the graders to monitor and hand-hold.

From our perspective, a private repository without *read* access looks the same
as no submission at all: no code.  We can't but treat it as though it is not
submitted.

[How a private repo you are not invited to appears](https://bitbucket.org/fadein/top-secret/src/master/)


Clicking those "Add" buttons may seem like a trivial thing for me to penalize
your grade so harshly, but it really makes all the difference.

[Submitting work on Bitbucket](../../Class_Rules.md#markdown-header-your-bitbucket-account)




----------------------------------------------------------------------------
# Example of a transparent PNG

This is an ![example](transparent_bg.png) of what an unacceptible UML diagram
may look like.  Apart from being quite incomplete, this submission is
unreadable and will be treated like a program containing a serious error
(eligible for at most 50% credit).



----------------------------------------------------------------------------
# Discuss Chapters 7, 8 of *The Pragmatic Programmer*

* Dig for requirements - the customer probably doesn't know what they want
  until you show them
* Complexity in software gets out of hand really fast
* Automation is our friend - automate build tasks, automate testing, automate
  all the things!
* If something is important to do, consider automating it; if it's not fun to
  do manually, it just won't happen
* The earlier you can find and fix a bug, the cheaper it is
* QA folks are your friends because they will think differently than you do; be
  nice to them!



----------------------------------------------------------------------------
# Unit tests and Assignment 4.1

* [Section 001](https://usu.instructure.com/courses/531135/assignments/2606370)
* [Section 002](https://usu.instructure.com/courses/527950/assignments/2606381)


TL;DR: You have been given 11 non-trivial unit tests.
Your final submission must contain at least 11 non-trivial unit tests.



### How to run an individual unit test from the command line

`$ python -m unittest Testing.TestDeck.TestDeck.test_getCard`

| Word           | Meaning                                                   
|----------------|---------------------------------------------------------------------------
| `-m unittest`  | Instruct Python to import the unittest module before running any more code 
| `Testing`      | The name of the directory containing Unit Test files 
| `TestDeck`     | A file's name, minus '.py'
| `TestDeck`     | Name of a class within `TestDeck.py`
| `test_getCard` | Name of a unit test function within the class `TestDeck`
         

### How to run a unit test suite from the command line

`$ python -m unittest Testing.TestDeck`




## War stories: How Gamers Killed Ultima Online's Virtual Ecology (8 min)
https://www.youtube.com/watch?v=KFNxJVTJleE

#### The moral of the story

Testing is good but you may still be woefully unprepared for the realities of
the production environment.  Due to the sheer scale of the userbase combined
with their unique perspectives, your in-house tests will not address the issues
they will discover.


Despite your best efforts, there are problems with your system that you'll just
never discover until it becomes heavily used.

* Scale of data; number of files, records as well as the sheer volume of data

* Scale of network I/O

* Scale of concurrent users

* Your users will do things with your system that neither you nor your testers
  imagined possible

Beta-tests involving a sizable subset of your userbase is one way to grapple
with this problem.
