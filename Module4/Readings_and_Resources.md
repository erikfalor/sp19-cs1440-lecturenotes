# Table of Contents


* Assigned Reading
* Unified Modeling Language
* Software Testing
* Important Software Testing Jargon
* Writing and running Unit Tests





-------------------------------------------------------------------------------
# Assigned Reading

* Wednesday Feb 27: Chapters 7 and 8 of *The Pragmatic Programmer* 
* Wednesday Mar 6: Chapter 6 of *The Pragmatic Programmer* - "While you are coding" **and** Chapter 2 of *The Mythical Man-Month* - "The Mythical Man-Month"


-------------------------------------------------------------------------------
# Unified Modeling Language

#### What is UML?

From wikipedia [UML](https://en.wikipedia.org/wiki/Unified_Modeling_Language) a
general-purpose, developmental, modeling language in the field of software
engineering, that is intended to provide a standard way to visualize the design
of a system.

Study these slides as they will help you create your own UML diagrams.  Plus,
their contents will come up on a future quiz/exam.

* ![03 - Objects and Classes](03 - Objects and Classes.pptx)
* ![04 - More About Classes](04 - More About Classes.pptx)
* ![05 - Associations and Multiplicity Constraints](05 - Associations and Multiplicity Constraints.pptx)
    



#### What sorts of things can I say in this language? And what am I trying to accomplish with UML?

The goal of Assignment 4 is to ensure that, for at least once in your career,
you have the experience of designing a system before you sit down to code it.

What you are trying to capture in this diagram is the large-scale structure of
your system. UML class diagrams aren't particularly suited to describing the
small, fiddly details of a complicated system.

This is by design.

Use the UML class diagram as a tool to separate the "Essence" of the Bingo!
Cards system from the "Accidental" details.



#### Rule of thumb

If it's hard to capture in a UML class diagram, it's likely an "Accidental"
quality of your system




-------------------------------------------------------------------------------
# Software Testing

The process which aims to *validate* and *verify* a software system


#### Validation

A process that aims to answer the question "is my program doing the right
thing?"

#### Verification

A process that aims to answer the question "is my program doing the thing right?"

_Companies that hire from USU have been known to quiz applicants on these two
concepts!_


#### Software failure

Runtime behavior that is not expected



#### Software error

A problem in the code, the design, or requirements that lead to the unexpected
behavior

* Software error == bug
* Software error != software failure



Through testing, developers try to uncover software failures. Software testing
should

-   be systematic
-   be repeatable
-   cover whatever is being tested

Don't get you're hopes up too high, though:

> Testing shows the presence, not the absence of bugs  
> -- Edsger W. Dijkstra  
> [http://homepages.cs.ncl.ac.uk/brian.randell/NATO/nato1969.PDF](http://homepages.cs.ncl.ac.uk/brian.randell/NATO/nato1969.PDF) p. 16



#### Debugging

The process of finding the causes of software failures and then correcting
those errors

-   Testing => what is wrong?
-   Debbugging => why is it wrong, and how may I fix it?




##  I'm sold. How do I do it?

There are many different methods for software testing which vary by:

-   what they try to verify
-   how to approach the testing activity
-   which kinds of errors that they can uncover

#### No testing method can uncover all possible types of errors in all programs

What experiences have you had which attest to this?

The key is to choose one or more testing methods that are appropriate to the
kind of software you are building




-------------------------------------------------------------------------------
# Important Software Testing Jargon

#### Test Case

Inputs, conditions, and expected outputs of a piece of software

#### Test Suite

A collection of test cases, related to a function or feature

#### Manual Testing

Human testers run through a test suite looking for unexpected behavior.

#### Automated Testing

The test suite is a program which can detect unexpected behaviors, and may be
run by a computer.

#### Unit Testing

Fine-grained test cases designed to exercise the fundamental units of your
program; most often this means testing individual functions/methods.

#### Integration Testing

Testing two or more parts of a system together with a focus on detecting
unexpected behavior in the interactions between the parts.

#### System Testing

Testing the entire software system as a whole - at the opposite end of the
granularity spectrum from Unit Testing. The most integrated of tests.

#### Non-Functional Testing

Tests with a focus on the non-functional aspects of a system (did the system
become slower, less usable, stable, or secure?), rather than on the measurable
functionality that a Unit Test would cover.

#### Regression Testing

Ensuring that a new software change hasn't created new problems or
re-introduced old problems.

#### Smoke or Sanity Testing

A quick, in-exhaustive test to make sure that the important aspects of a system
are stable, and that the system may be subjected to more thorough testing.
(e.g. did the build succeed without errors, does the program even launch, did
that bugfix work or make things seriously worse, etc.)

Some sources make a distinction between these two types of testing, but for our
purposes we'll just consider these to be simple, informal tests.


##  Testing at scale

[War Stories: Ultima's Virtual Ecology  ](https://www.youtube.com/watch?v=KFNxJVTJleE)

Testing at your desk or in-house with a limited dataset or at limited scale is
a different beast entirely.

Despite your best efforts, there are problems with your system that you'll just
never discover until it becomes heavily used.

-   Scale of data; number of files, records as well as the sheer volume of data
-   Scale of network I/O
-   Scale of concurrent users
-   Your users will do things with your system that neither you nor your
    testers imagined possible

Beta-tests involving a sizable subset of your userbase is one way to grapple
with this problem.


-------------------------------------------------------------------------------
# Writing and running Unit Tests

[Unit Testing in Python](https://docs.python.org/3/library/unittest.html)
