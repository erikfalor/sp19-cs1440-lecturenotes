# CS1440 - Wed Feb 20 - Module 4

# Announcements

## FSLC - Wireshark Tutorial

Tonight 2/20
7pm - ESLC room 053

Wireshark® is a network protocol analyzer. It lets you capture and
interactively browse the traffic running on a computer network. It has a rich
and powerful feature set and is world's most popular tool of its kind. It runs
on most computing platforms including Windows, MacOS, Linux, and UNIX. Network
professionals, security experts, developers, and educators around the world use
it regularly. It is freely available as open source, and is released under the
GNU General Public License version 2.



## ACM-W Homework Night  -  Thu Feb 21

ACM-W is hosting homework nights every third Thursday of each month
from 6:00pm - 9:00pm in ENGR 202. 

The first one is tomorrow night, Thursday, Feb 21  

ACM-W is especially for women majoring or minoring in Computer Science, but all
are still welcome.

Snacks will be provided!

*(ACM-W is the USU women’s chapter of the Association of Computing Machinery)*


----------------------------------------------------------------------------
# Topics:

* Discuss Brooks' "Passing the Word"
* Assignment 4 Overview


----------------------------------------------------------------------------
# Discuss Brooks' "Passing the Word"

* What is the big idea Brooks wants to get across?

    * Documentation is an important part of a healthy and complete programming product.

    * The manual should be written with the end-user in mind; Explain what the
      user will see, no more, no less

* What tools do you have to explain the designed intent of a program?

    + Backus-Naur Form
    + Requirements document
    + UML class diagram
    + The source code itself
    + Technical documentation
    + Users' manuals

* How does this concept relate to the "Programming Product" we discussed in
  "The Tar Pit"?

    + A program without a manual isn't a "Product"

* What are advantages of defining a system formally before creating it?

    + System has a unified purpose, less rough edges
    + Some careful thought was spent on it


* What are advantages of using an implementing of a system as its own formal
  definition?

    + Offers a final answer to obscure questions
    + Spend less time on documentation, more time on writing code


* Who (or what) is the intended audience of our programs?
    * People are, just as much as machines

* What is the value of Flow-Charts?
    * Despite being invented by real-life wizard John von Neumann, they lost
      much of their utility by the time structured programming languages were
      created



## On writing comments

* What is a "self-documenting" program?
    * A program which puts to good use identifiers which are required to be
      there anyway. By consistently applying a good naming scheme, you can help
      a human reader understand what the program is doing

    *  Bugs are going to be much easier to find in a program adhering to the
       "self-documenting" principle. Compare these files to see for yourself:

       readability_poor.c
       readability_better.c

* Are code comments a blessing or a burden?
    * The good: 

        + Comments don't make a program run any slower, nor do they have any
          noticeable affect on compilation times (even if your source-code
          contains the entire text of War and Peace by Leo Tolstoy

        + They can be used explain aspects of the program which cannot be made
          clear by reading the source code itself. Concepts such as "why is
          this code written this way?" are explained by a comment

        + Important information is immediately at hand.  Comments are the only
          place another programmer is guaranteed to see your wisdom.  External
          documents or revision control histories don't need to be consulted.


    * The bad:

        + Some programmers neglect to update comments which they didn't write
          themselves. Out-of-date comments can be worse than no comments at all.

        + If something is important for ordinary users to know about, they'll
          never find it in the source code



### How do you know when a comment is appropriate?

Some programmers advocate to *never* use comments at all, instead relying on
[Self Documenting Code](http://wiki.c2.com/?SelfDocumentingCode) to get their
point across.  Other programmers seem to regard comments as their personal
Twitter account and overshare.

My recommendation lies in the middle of these two extremes and is based on the
observation that executable code is a poor way to communicate the programmer's
intent and rationale.


*Don't* use a comment to describe what the code itself already explains:

```C
int i = 1337; // initialize i to 1337

/* Make an XML Parse Tree from the data stream */
XMLParseTree* xmlParseTree = makeXmlParseTree(*dataStream);
```


*Do* write a comment when a fragment of code is used in an unexpected way, or
to explain *why* you did it this way:

```C
// intentional NOOP; delay for 3.5 milliseconds (assuming 48MHz processor)
for (int j = 0; j <= 168000; ++j)
    ;
```

Better still is to find a way to make the puzzling code fragment *self-documenting*:

```C
// This figure is derived from a CPU clock frequency of 48MHz
#define NANOSEC_PER_CLOCK_TICK 20.83333333333

/* Delay program for the given number of microseconds.

   IMPORTANT: This only works when compiler optimizations are disabled!
*/
void sleepus(unsigned int microseconds) {
    int ticks = microseconds * NANOSEC_PER_CLOCK_TICK * 1000.0;

    for (int j = 0; j <= ticks; j++)
        ;
}


// Later on in the program, we can sleep for 3.5 milliseconds like this:
sleepus(3500); 
```


### It's always a good idea to [CodeForTheMaintainer](http://wiki.c2.com/?CodeForTheMaintainer)

> Always code as if the person who ends up maintaining your code is a violent
> psychopath who knows where you live.




## Backus-Naur Form: a Formal Specification Language

Brooks name-drops this on p. 64, and it's something that all of you should have
seen by now, at least if you've been picking up on my pleas to please read
documentation:

    https://docs.python.org/3/reference/lexical_analysis.html#literals

BNF is something that you'll study again: you should see it again if you take
*CS4700 - Programming Languages* and *CS5300 - Compiler Construction*.  At any
rate, since it is used in so many places to describe syntax it ought to be on
your short list of things to pay attention to.



## Ruby: A programming language defined by its implementation

Brooks weighs the pros and cons of using an implementation as a *de facto*
formal specification.  Most programming languages are defined first in a
specification document (usually containing pages upon pages of BNF descriptions
of language constructs).

The [Ruby Programming Language](https://www.ruby-lang.org/en/) created by
Yukihiro Matsumoto (Matz) is an example of a real-world system where an
implementation *is* the specification.  Matz's Ruby Implementation (a.k.a. Ruby
MRI) is considered the final word as to what is considered correct behavior for
the Ruby language.  There had been an attempt to formalize the language, but
that project was abandoned due to lack of support from the core development
team.

As Brooks points out, the pros of using an implementation as the specification
is that there are always clear-cut answers to obscure edge cases.  The answers
may not be the ones you like, but you can find them.  The downsides are that
you may enshrine bugs into the specification.  Remember, no program may be said
to be free of errors; the best we can hope for is that we have excised all
errors we have detected.

[Ruby Implementations on Wikipedia](https://en.wikipedia.org/wiki/Ruby_(programming_language)#Implementations)
  



----------------------------------------------------------------------------
#  Assignment 4 Overview

The next assignment is split into two deliverables.

In the spirit of what we read about this week in "The Mythical Man-Month", we
will create supporting documentation describing the design for our program.



# Assignment requirements

## Section 001

* [Assn 4.0: Bingo! UML design](https://usu.instructure.com/courses/531135/assignments/2606369)
* [Assn 4.1: Bingo! implementation](https://usu.instructure.com/courses/531135/assignments/2606370)

## Section 002

* [Assn 4.0: Bingo! UML design](https://usu.instructure.com/courses/527950/assignments/2606380)
* [Assn 4.1: Bingo! implementation](https://usu.instructure.com/courses/527950/assignments/2606381)




I cannot emphasize this enough:
-------------------------------
You must submit your design document before you even *begin* to write any code.
The tutors will not help you with your code until you can show them your UML
diagram.

You will undoubtedly make changes to your design once you begin to write the
code. You will be permitted to re-submit updated versions of your UML Class
Diagram to Canvas.

We will grade the last Assignment 4.0 submission that arrives before the 4.0
due date.  We will take into account the last update to 4.0 (the design) when
we grade 4.1 (the code).


## Your task:

Create a UML Class Diagram describing both the provided code along with any
extra code you must write to complete the program.
