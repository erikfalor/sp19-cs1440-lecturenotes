# CS1440 - Fri Feb 22 - Module 4


# UML Lecture by Paxton Alexander

## Notes and Powerpoint slides

[The Unified Modeling Language](../Readings_and_Resources.md)

[Objects and Classes](../03 - Objects and Classes.pptx)


--------------------------------------------------------------------------------
## Chicken class and driver

[Chicken.py](Chicken.py)

[main.py](main.py)
