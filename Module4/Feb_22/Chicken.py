
class Chicken:

    def __init__(self, name, age, color, molting):
        self.__name = name
        self.__age = age
        self.__color = color
        self.__molting = molting

    def lay_egg(self):
        if self.__molting:
            print('{} layed and Egg!'.format(self.__name))
        else:
            print('{} is not molting'.format(self.__name))

    def cluck(self):
        print('{} clucks'.format(self.__name))

    def eat(self):
        print('{} eats.'.format(self.__name))

    def get_name(self):
        return self.__name

    def get_age(self):
        return self.__age

    def get_color(self):
        return self.__color
