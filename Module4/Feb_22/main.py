from Chicken import Chicken


if __name__ == '__main__':
    chicken1 = Chicken('Lucy', 1, 'brown', True)
    chicken2 = Chicken('Ann', 2, 'white', False)

    chicken1.cluck()
    chicken1.lay_egg()
    chicken2.lay_egg()
    chicken2.eat()

    print(chicken2.get_age())
