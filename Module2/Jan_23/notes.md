# CS1440 - Wed Jan 23 - Module 2

# Announcements

## Cyber Security Club

The opening meeting will occur tonight @ 6pm - ESLC 053



## FSLC InstallFest

Tonight @ 7pm - ESLC 053

Do you want to rock Linux on your own computer, but don't know how to start?
Get experienced help installing it!

Bring a laptop and a flash drive, or install on one of our machines




# Topics:
*   Absolute vs. Relative paths
*   "The Tar Pit"
*   Introducing Assignment #2


--------------------------------------------------------------------------------
# Absolute vs. Relative paths

*TL;DR* Write your programs such that the user is fully responsible for
specifying the path to input files.  Your program should *not* encode any
information about files or directories on your computer.  It is too much
information (TMI) and makes your program less portable.


#### Current Directory

Every program which runs on your computer runs in a "location" on your file
system.  In some programs, such as a command prompt or a file explorer, this
information is made very obvious to the user.

##### Also known as

*   Working Directory
*   Current Working Directory (CWD)



#### Absolute path

Tells the computer where to locate a file anywhere on the system.  An absolute
path contains all location information about a file:

Examples:

    C:\Users\Erik Falor\Desktop\New Text Document.txt
    /home/fadein/anaconda3/


#### Relative path

Combined with a program's current directory to create an absolute path.  Tells
the computer how to locate a file relative to some location


## Laziness and paths

Any time time your computer uses a file it **needs** an absolute path.  Because
every program on your computer already has a CWD you are not obligated to
always spell out an absolute path; relative paths are automatically combined
with the CWD for you.



## File path best practices by example

Unless I go out of my way to tell you otherwise, please *never* hard code path
information into your programs.  The reason for this is illustrated below:



### Bad:
    f = open("C:\\Users\\Mr.CoolGuy\\school\\cs1440\\homeworks\\cs1440-falor-erik-assn4\\data" + sys.argv[1])

*Problem*: This directory doesn't exist on the graders' computers, so your
program will always fail for them.  They must edit your code in order to simply
run the program.


### Bad:
    f = open("../data/" + sys.argv[1])

*Problem*: This program only works when I launch it from src/ or some other
sibling directory to data/.  The graders must open your program in an editor to
figure out where it's looking for files.


### Good:
    f = open(sys.argv[1])

*Virtues*: Short and sweet.  Leave the details of where files are to the user,
who knows best.




--------------------------------------------------------------------------------
# "The Tar Pit"

[The La Brea Tar Pits in California](https://tarpits.org/)


## Who is this Fred Brooks guy, and why should I care?

* [Fred Brooks Jr.](https://www.cs.unc.edu/~brooks/)
* [Wikipedia Article](https://en.wikipedia.org/wiki/Fred_Brooks)



Turn to your neighbors and spend a few minutes discussing these questions and
jotting down your thoughts and insights.  Get through as many of these
questions as you can, but don't feel rushed.

*   What is Fred Brooks trying to say in this essay?

    *   Programmers are, perhaps, overly optimistic
    *   Creating a program is far more difficult than other human endeavors
        because of the tractable nature of software
    *   But, software's tractability is one of its saving graces

*   This essay was first published in 1975.  Do you think its message is still
    relevant in 2019?

    *   With the advent of cloud computing and other large-scale projects, it
        is more relevant than ever
        
*   What is a program?

    *   Code which stands by itself, and doesn't need other stuff to work

*   What is a programming system?

    *   It is a collection of programs which are designed to work together

    *   Making a simple program is easier in terms of time and money than
        making a Programming System - by a factor of 3 - due to extra design
        work and testing

*   What is a programming product?

    *   A program shipped with documentation: users manuals, troubleshooting
        guides, support documents, etc.

    *   Making a simple program is easier in terms of time and money than
        making a Programming Product - by a factor of 3


*   What is a programming systems product?

    *   An integrated, tested and fully documented system of programs

    *   Making a simple program is easier in terms of time and money than making a
        Programming Systems Product - by a factor of 9!



Key Take-aways:
===============

*   Whether you like it or not, whether your program is bug-free or not,
    maintenance is an issue you will inevitably face.  Technology marches on,
    necessitating changes to your program to adapt to new platforms, languages,
    etc.

*   When the going gets tough, always keep in mind the reasons why you're a
    programmer.  For you it might be the challenge of solving problems, the joy
    of creating new things, the satisfaction which comes from making something
    that is useful to others, or the thrill of always having something new to
    learn.




--------------------------------------------------------------------------------
# Introducing Assignment #2

Like the Caesar Cipher program, this program will be a text-based tool which
takes command-line arguments which refer to files on your computer.  These
tools will need the user to provide a path (relative or absolute) to reach
those files.


*   [Section 001](https://usu.instructure.com/courses/531135/assignments/2606366)
*   [Section 002](https://usu.instructure.com/courses/527950/assignments/2606377)
*   [Starter Code](https://bitbucket.org/erikfalor/cs1440-falor-erik-assn2)

You should begin by cloning the starter code to your own computer and
configuring your repo to allow you to push your work to your own repo on
Bitbucket.  See the README.md file included in the starter code for
instructions.
