def sumStrings(args):
    s = 0.0
    for n in args:
        s += float(n)
    print(s)


def productStrings(args):
    s = 1.0
    for n in args:
        s *= float(n)
    print(s)


def averageStrings(args):
    l = len(args)
    s = 0
    for d in args:
        s += float(d)
    ave = s / l
    print("The average value is " + str(ave))

def shout(args):
    for filename in args:
        f = open(filename)
        for line in f:
            print(line.upper(), end='')
        f.close()

