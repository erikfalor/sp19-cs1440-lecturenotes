import sys

import George

def usage(program):
    print("Usage: " + program + " sum|product|average|shout|l33t")





if len(sys.argv) < 2:
    usage(sys.argv[0])


elif sys.argv[1].lower() == "sum":
    if len(sys.argv) == 2:
        print("Usage: sum N ... where N is a number")
    else:
        George.sumStrings(sys.argv[2:])


elif sys.argv[1].lower() == "product":
    if len(sys.argv) == 2:
        print("Usage: product N ... where N is a number")
    else:
        George.productStrings(sys.argv[2:])


elif sys.argv[1].lower() == "average":
    if len(sys.argv) == 2:
        print("Usage: average N ... where N is a number")
    else:
        George.averageStrings(sys.argv[2:])


elif sys.argv[1].lower() == "shout":
    if len(sys.argv) == 2:
        print("Usage: shout F ... where F is a file name")
    else:
        George.shout(sys.argv[2:])


elif sys.argv[1].lower() == "l33t":
    print("TODO: I'll figure this out later")


else:
    usage(sys.argv[0])
