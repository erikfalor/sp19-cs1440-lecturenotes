# CS1440 - Mon Jan 28 - Module 2

# Announcements

## The USU Computer Security Team is Hiring

They are looking for Security Minded Students
You can apply on [Aggie Handshake](https://app.joinhandshake.com/jobs/2331049)



## Free Software and Linux Club - How to use the Shell

Wednesday, January 30th, at 7:00pm in ESLC 053

We will be discussing navigation of your shell this week.  We will not try to
overwhelm anyone so it's going to be the basics and will provide everyone with
tools and resources to pursue Shell mastery on their own!

We hope to see everyone there!



## Assigned Reading discussion on Wednesday 1/30
Chapter 3 of "The Pragmatic Programmer" The Basic Tools




# Topics:

* Assignment 1 Retrospective
* Mud Card Follow-up
* Code Organization with Modules


----------------------------------------------------------------------------
# Assignment 1 Retrospective

### Aha! and Oh No! Moments

Come get two sticky notes, one of each color.

On each note write down your A# and

    * Magenta: an epiphany when something 'clicked'
    * Blue: your moment of deepest despair

Post them on the whiteboard along the timeline, indicating when that event
occurred to you.  An interesting picture will emerge of how this assignment
went for the entire class.


### Questions for you to consider:

* What was the single biggest problem for the class?
* What was the most beneficial thing that happened to the class?

What can you take away from this timeline?

* What is one thing that you will *begin doing* for Assignment 2?
* What is one thing that you will *stop doing* for Assignment 2?



----------------------------------------------------------------------------
# Mud Card Follow-up

Impediments and how to overcome them
====================================

* Many of you reported struggling with git

> I'm going to experiment on git more. Make more commits, push more, make more
> practicing files and repositories

> I am finding that using git makes me more brave.  And it helps me feel
> confident.



* Processing command-line arguments with `sys.argv` and using the CLI in general.

> Take some time to practice using the console.

> Clone the lecture notes so I can run code Erik wrote in class



* "Getting started"

> I'm going to get together with my Study Buddies to work on it tomorrow



* Misunderstanding requirements; missing something in the 1st pass at reading

> More carefully read Erik's lecture notes, the assignment description, and
> other resources like the Python Intro



* Some reported struggling with the Python programming language; some are rusty
  at it and for others this is their very first time using it.

> REPL

> Get help from tutor lab and the TAs

> Reach out to instructor



----------------------------------------------------------------------------
# Code Organization with Modules

Let's revisit the program we wrote in class last Friday: `driver.py`.
I have improved upon it by writing an implementation of `average` and
cleaning up the comments.  This program can be improved further.  Let's read
through it and see what stands out.


### Areas for improvement:

* Comments would be nice
* The big if/elif/else tree is unwieldy and hard to follow
* The "Usage" message at the top & bottom of the program is duplicated 
* Because the code refers to `sys.argv[2:]`, it is really only usable in a
  program which has an argument list that looks like this:

    ['driver.py', 'sum', '1', '2', '3']


Because of the assumption that `sys.argv` will have this form it will be hard
to re-use any portions of useful code elsewhere within this program or in
another program.

I can easily fix these problems by removing portions of code from the if/else
tree and move it into functions with good names.  These functions will accept a
list as an argument, meaning that their behavior isn't directly related to
`sys.argv`.  This will at once make the code more readable as well as more
usable.

After I move the code into well-named functions, I realize that I can take this
a step further and put these functions into a *module*.

[Code Modules](https://bitbucket.org/erikfalor/sp19-cs1440-lecturenotes/src/master/Readings_and_Resources.md#markdown-header-code-modules)


## Do as I say, not as I do

Naming code entities is one of the hardest things about programming.  I took
the low road and named my module `George`.  In the future I must try to give
better names than this.
