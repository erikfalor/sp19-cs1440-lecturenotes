# CS1440 - Fri Feb 01 - Module 2

# Announcements

## Mastery Quizzes have been updated for Modules 1 & 2

This is in preparation for the upcoming Exam #0,
next week from Tues Feb 5 - Thurs Feb 7.

I will add more questions to M2's MQ based upon how far we make it today.




# Topics

*   Modules and Namespaces
*   How to debug anything


--------------------------------------------------------------------------------
# Modules and Namespaces

[Namespaces](../Readings_and_Resources.md#markdown-header-namespaces)

While modules and namespaces share a syntactic appearance with classes and
objects, they aren't the same things.  Namespaces can contain classes and
objects, but they *aren't* classes or objects themselves.


When importing identifiers into the current namespace, be aware of collisions
between locally defined identifiers and those from the imported module.

## Collision Example:

See the code in the directory `namespace_collision/`




--------------------------------------------------------------------------------
# How to debug anything

[Debugging](../Readings_and_Resources.md#markdown-header-debugging)
