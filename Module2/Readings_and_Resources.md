# Table of Contents

* Assigned Reading
* Code Modules
* Debugging
* The "Wolf Fence" algorithm for finding bugs



----------------------------------------------------------------------------
# Assigned Reading

Chapter 3 of *The Pragmatic Programmer* - "The Basic Tools"

* The Power of Plain Text
* Shell Games
* Power Editing
* Source Code Control
* Debugging
* Text Manipulation
* Code Generators


----------------------------------------------------------------------------
# Code Modules

a.k.a. "Modular Programming"

[Python Tutorial: 6. Modules](https://docs.python.org/3.7/tutorial/modules.html)



#### Modular Programming as defined by Wikipedia

> Modular programming is a software design technique that emphasizes separating
> the functionality of a program into independent, interchangeable modules,
> such that each contains everything necessary to execute only one aspect of
> the desired functionality.


Related pieces of code are collected into larger entities called 'Modules'.
Modules may or may not be useful as programs unto themselves, but, what we most
often think of as a module is *not* a complete, standalone program.

Modules may be selectively included into a useful program.  Modules encourage
code reuse by enabling libraries of commonly desired general-purpose code to be
readily shared between different applications.

Examples: Java/Python packages, C/C++ header files (#include <iostream>)




## What goes into a module?

Pieces of code which serve a *related* purpose can be put together into a
module.  In this sense the related purpose is not that "this code is used in
the same program".  Code sharing a module should be unified by purpose.

Variables and functions can be moved out of a program and put into a module.
This makes it convenient for other programs to use that same code without
copying and pasting it from file to file.

The names of variables and functions are collectively known as *identifiers*.

#### Identifier: A textual token that identifies a programming entity

[Identifier on Wikipedia](https://en.wikipedia.org/wiki/Identifier#In_computer_languages)





## How to write a module in Python

You've been writing modules all along; every Python source file is a module!



## How to use modules in Python

Modules are *imported* into a running Python program with the `import` keyword.
When importing a file you import it by its file name, dropping the '.py'
extension.


You should match the case of the name of the file when importing.  Don't write

    import coolcode

To import a file named `CoolCode.py`.  Instead, you must

    import CoolCode

When a module is in a file under a subdirectory (for instance
`CoolModules/CoolCode.py` on Linux/Mac or `CoolModules\CoolCode.py` on a game
console), replace the '/' or '\' with a dot '.':

    import CoolModules.CoolCode

This works for modules nested under an arbitrary depth of subdirectories.  As
another example, the file
`This/Is/Getting/Ridiculous/CoolModules/CoolCode.py` is imported as

    import This.Is.Getting.Ridiculous.CoolModules.CoolCode





## Namespaces

When a module is imported all global identifiers (i.e. names of variables and
functions which are not contained within functions) defined therein become
accessible in your program.  To access them you first write the name of the
module from the import statement followed by another '.', followed by the
identifier.

As an example, suppose that the file `CoolModules/CoolCode.py` defines a
function named `srslyCool()` and a variable named `temperature`.  You would use
them in your program like so:

    import CoolModules.CoolCode

    CoolModules.CoolCode.srslyCool()

    print("The temperature was")
    print(CoolModules.CoolCode.temperature)
    CoolModules.CoolCode.temperature += 1
    print("Now the temperature is")
    print(CoolModules.CoolCode.temperature)


In this context the name of the module is known as a **namespace**.

#### Namespace: A mapping from names to objects

[9.2 Python Scopes and Namespaces](https://docs.python.org/3/tutorial/classes.html#python-scopes-and-namespaces)

Namespaces protect the identifiers you create from being overridden by
identifiers defined in other modules, and vice versa.

For instance, many Python modules define a variable named `VERSION` which helps
you judge whether the module is out of date.  Without putting each of these
versions of the identifier `VERSION` within a namespace you'd only be able to
access one version of `VERSION`.  Furthermore, you could never be quite sure
which version of `VERSION` you were inspecting.



### Import identifiers into the current namespace with `from`

You may import identifiers from a module directly into the current namespace by
writing an import statement in this form:

    from MODULE import IDENTIFIER_0, IDENTIFIER_1 ...

For example:

    from CoolModules.CoolCode import srslyCool, temperature

From this point on in the file, the function `CoolModules.CoolCode.srslyCool()`
may be referred to simply as `srslyCool()`, and the variable
`CoolModules.CoolCode.temperature` can be accessed just as `temperature`.

If you want to import a long list of identifiers from a module, instead of
awkwardly listing them all out you can import '*':

    `from CoolModules.CoolCode import *`

The risk of doing this, of course, is that you might import an identifier from
one module which overrides an identifier defined in another.




## When will the code in a module be executed?

The code in the module is run at the moment it is imported.  This means that
any code which is not contained inside of a function will be executed at that
time.  You can test this out by writing this code in a file called `Running.py`:

```
print("You have loaded the module named 'Running'")

def run():
    print("You have called 'Running.run()'")
```

Then, import the `Running.py` module in the REPL.  Notice that the message is
printed immediately, before the REPL prompt returns.

```
$ python
Python 3.7.1 (default, Dec 14 2018, 19:28:38)
[GCC 7.3.0] :: Anaconda custom (64-bit) on linux
Type "help", "copyright", "credits" or "license" for more information.

>>> import Running
You have loaded the module named 'Running'

>>> Running.run()
You have called 'Running.run()'
```

You should consider whether any code must run at the time a module is imported.
Code which should not run at the time of import should be placed within a
function so that the user of the module can choose when to use it.



## Can a Python file tell when it is being used as a program or a module?

You may have seen this construct in Python examples you've read online:

    if __name__ == '__main__':
        pass


The special variable `__name__` is defined for every Python program and module.

* When a Python script is being run as a program `__name__` contains the string
  `'__main__'`

* When a Python script is imported as a module, `__name__` contains the name
  the module was imported as


We might, then, rewrite `Running.py` such that it does not hard-code its own
name:

```
print("You have loaded the module named '" + __name__ + "'")

def run():
    print("You have called '" + __name__ + ".run()'")
```

Then, import it into the REPL and notice what is printed.
Now, rename the file, import it under its new name and see the difference.

Just as it is sometimes useful for a program to learn its own name by
inspecting `sys.argv[0]`, a Python script can look at the value of `__name__`
to determine how to behave.

Real-world Python modules may use this feature to run a self-test when called
from the command line as a stand-alone program.  Or, a Python file may place
its main code inside an `if __name__ == '__main__'` block to simultaneously be
a stand-alone program and a module which is used by other stand-alone programs.



## System-Defined Names and Staying Off Guido's Lawn

`__name__` is an example of a *system-defined name*.  Ordinary programmers are
discouraged from creating identifiers beginning and ending with double
underscores because these identifiers are reserved for the use by the creators
of Python (i.e., the aforementioned Guido van Rossum).  New system-defined
names can be created and existing ones can be removed from the language at any
time.

While the identifier `__cool__` may be unused today, there is nothing
preventing Python 3.8 from taking it up.  If that happens your program may
cease working as expected when upgrading from Python 3.7 to 3.8.

[2.3.2. Reserved classes of identifiers](https://docs.python.org/3/reference/lexical_analysis.html#reserved-classes-of-identifiers)




----------------------------------------------------------------------------
# Debugging

> If debugging is the process of removing bugs, then programming must be the
> process of putting them in.
>
> -- Edsger W. Dijkstra



## Origin of the term

The terms "bug" and "debugging" are popularly attributed to Admiral Grace
Hopper in the 1940s.

While she was working on a Mark II computer at Harvard University, her
associates discovered a moth stuck in a relay and thereby impeding operation,
whereupon she remarked that they were "debugging" the system. However, the term
"bug", in the sense of "technical error", dates back at least to 1878 and
Thomas Edison.

In the ACM's digital library, the term "debugging" is first used in three
papers from 1952 ACM National Meetings. Two of the three use the term
in quotation marks. By 1963 "debugging" was a common enough term to be
mentioned in passing without explanation on page 1 of the CTSS manual.


#### Debugging
The process of finding software errors (the causes of failures) and then
correcting those errors.



## How do I debug anything in 4 easy steps?

### 0. Test the software to find failures.

It is a best practice to *not* have programmers do their own testing. As
paradoxical as this may sound, a programmer's expectations about what the
program is and how it should work prevents them from really pushing the
boundaries and being effective at finding problems.

Testing provides critical information about failures:

* The circumstance in which the failure occurs
* Test cases which reliably reproduce the failure



### 1. Uncover the error which gives rise to the failure.

Once the failure is known, the developer can use a variety of techniques to
uncover the cause and produce a fix.  The techniques in this list are not
mutually exclusive; professionals use a combination to zero in on bugs.

* Interactive debugging - stepping through a program with the aid of a tool
  which lets the developer inspect the internal state of the program as it runs
    - variable inspector
    - execute function calls
    - examine memory, CPU registers
    - call stack
    - watchpoints

* Remote debugging - interactive debugging of a program running on a different
  system from the debugger. For example, you may connect to another system over
  the internet and attach an interactive debugger. Another example is debugging
  an embedded device from a PC

* Protocol analysis - using diagnostic tools to analyze network messages
  sent/received by the system, examine interactions with external hardware or
  an OS.

* Post-mortem debugging - debugging a program after it has already crashed. An
  example of this is inspecting a core dump file. Also includes reading an
  error message in the log file or on the console after a crash.

* Print debugging - causing the program to print messages to a console or a log
  file which indicate how a program's execution evolves over time.

* Log file analysis - carefully reading through the logs that some software
  systems create in the normal course of operation.  These log files may
  contain error messages raised by the runtime environment, or they may contain
  messages written by programmers in situations where an error occurs.

* Crafted data - some errors only occur when the program's input contains
  unexpected data.  Errors which may be difficult to reproduce under normal
  operating conditions can be made to occur on demand with specially crafted
  inputs.  Other errors are only evident after a program has consumed a large
  quantity of data, making the task of isolating the problem likewise large.  A
  specially crafted small input file can be used to locate the problem much
  more quickly.

* "Wolf fence" algorithm - locate a bug in a program by dividing it into
  halves, further partitioning the half containing the bug until you pinpoint
  it.  You might do this with breakpoints, log messages, or specially-crafted
  function calls (hooks).  Git bisect is an example of this technique applied
  to the revision history of a project.



### 2. Create a fix

Once the problem is uncovered, fix it (if possible). Hopefully it's simply a
problem with your code and *not* with your design.

At this point you may elect to put *assertions* into the code which cause the
program to terminate with a specific error message, alerting developers (or
users) when this specific error recurrs.



### 3. Re-run the tests

Return to the test cases created in step #3.1 to ensure that

* The failure no long exists, once the error has been found and corrected.

* The fix did not introduce new errors.  This is harder to guarantee and
  necessitates further testing.

Many programmers, after investing significant time and effort into fixing a
bug, fail at this step.  Don't undermine all of your work by slacking off in
the home stretch!



----------------------------------------------------------------------------
# The "Wolf Fence" algorithm for finding bugs

> The most effective debugging tool is still careful thought,
> coupled with judiciously placed print statements.
>
>    -- Brian Kernighan

The "Wolf Fence" algorithm was explained in a brief article in *Communications
of the ACM* in 1982.

The original article [The "Wolf Fence" algorithm for debugging](https://dl.acm.org/citation.cfm?id=358695)

Uh oh, it's behind a paywall :(

[Use this cheatcode to log into the USU library](http://dist.lib.usu.edu/login?url=http://portal.acm.org/dl.cfm?coll=portal)

Then access it again using the first URL
