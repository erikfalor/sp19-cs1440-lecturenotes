###
### Notice: This version of quadratic.py has been modified to accept
### command-line parameters instead of prompting the user for input
###

import math
import sys

def firstRoot(a, b, disc):
    return (-b + disc) / (2 * a)     # <======= PLACE A BREAKPOINT HERE

def secondRoot(a, b, disc):
    return (-b - disc) / (2 * a)

def demo(a, b, c):
    d = b ** 2 - 4 * a * c
    if d > 0:     # <======= PLACE A BREAKPOINT HERE
        disc = math.sqrt(d)
        root1 = firstRoot(a, b, disc)
        root2 = secondRoot(a, b, disc)
        return root1, root2
    elif d == 0:
        return firstRoot(a, b, 0)
    else:
        return "This equation has no roots"



a = float(sys.argv[1])
b = float(sys.argv[2])
c = float(sys.argv[3])
result = demo(a, b, c)     # <======= PLACE A BREAKPOINT HERE
print(result)








































