# CS1440 - Wed Jan 30 - Module 2

# Announcements

## Free Software and Linux Club - Bash Course Crash Course

Learn how to use the Command Shell

We will be discussing navigation of your shell this week.  We will not try to
overwhelm anyone so it's going to be the basics and will provide everyone with
tools and resources to pursue Shell mastery on their own!

We hope to see everyone there!

Wednesday, January 30th, at 7:00pm in ESLC 053



## BSidesSLC Student Scholarship Program
https://www.bsidesslc.org/scholarship

*Interested students must submit a proposal by Friday, February 1st, 2018*



## Exam 0 is next week

The exam is available at the testing center from Tues Feb 5 - Thurs Feb 7.

The exam will cover all material from the beginning of the semester through
this Friday.

We will have an exam review on Monday to prepare.  The Mastery Quizzes for
Modules 0, 1 and 2 are your best way to review; I'll be updating the Mastery
Quizzes with new material from today and Friday's lectures before the exam.




# Topics

* Discuss Chapter 3 of *The Pragmatic Programmer* - "The Basic Tools"
* Direct Debugging in the IDE
* Rubber Duck Debugging

--------------------------------------------------------------------------------
# Discuss Chapter 3 of *The Pragmatic Programmer* - "The Basic Tools"

*   The Power of Plain Text
*   Shell Games
*   Power Editing
*   Source Code Control
*   Debugging
*   Text Manipulation
*   Code Generators




--------------------------------------------------------------------------------
# Direct Debugging in the IDE

> Everyone knows that debugging is twice as hard as writing a program in the
> first place.  So if you're as clever as you can be when you write it, how
> will you ever debug it?
>
>    -- Brian Kernighan

[Debugging with PyCharm](https://www.jetbrains.com/help/pycharm/2019.1/part-1-debugging-python-code.html)

Use the debugger in PyCharm to walk through your code and observe what's going
on. I recommend that you spend some time becoming familiar with the debugger so
that you can use it to its full advantage.

The debugger, once mastered, whill let you **Visualize your Data** and will
empower you to live Pragmatic Tip #27: **Don't assume it - Prove it**


*   Running vs. Debugging
*   Breakpoint
*   Inline debugging
*   Stepping
    *   Stepping over
    *   Stepping into
    *   Stepping into my code
    *   Stepping out of
    *   Run to cursor
*   Frames (call stack)
*   Watches
*   REPL









--------------------------------------------------------------------------------
# Rubber Duck Debugging


			   ,-.
		   ,--' ~.).
		 ,'         `.
		; (((__   __)))
		;  ( (#) ( (#)
		|   \_/___\_/|
	   ,"  ,-'    `__".
	  (   ( ._   ____`.)--._        _
	   `._ `-.`-' \(`-'  _  `-. _,-' `-/`.
		,')   `.`._))  ,' `.   `.  ,','  ;
	  .'   .    `--'  /     ).   `.      ;
	 ;      `-       /     '  )         ;
	 \                       ')       ,'
	  \                     ,'       ;
	   \               `~~~'       ,'
		`.                      _,'
	hjw   `.                ,--'
			`-._________,--'

	http://www.ascii-art.de/ascii/pqr/rubber_duck.txt

https://rubberduckdebugging.com/

https://rubberduckdebugging.com/cyberduck/
