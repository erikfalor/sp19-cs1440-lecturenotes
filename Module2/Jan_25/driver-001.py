import sys

# We'll call the first argument to a program like `git` a *subcommand*.  What the
# remaining arguments mean depend upon what the subcommand is. Let's write a
# program with these subcommands:

print(sys.argv)

if len(sys.argv) <= 1:
    print("Usage: " + sys.argv[0] + " sum|product|average|shout|l33t")
elif sys.argv[1].lower() == 'sum':
    if len(sys.argv) <= 2:
        print("Usage: sum N ... where N is a number")
    else:
        s = 0.0
        for n in sys.argv[2:]:
            s += float(n)
        print(s)


# 1.  **sum** add the remaining numeric arguments together and print the result


elif sys.argv[1].lower() == 'product':
    if len(sys.argv) <= 2:
        print("Usage: product N ... where N is a number")
    else:
        s = 1.0
        for n in sys.argv[2:]:
            s *= float(n)
        print(s)



elif sys.argv[1].lower() == 'average':
    print("You want to take the average")

elif sys.argv[1].lower() == 'shout':
    if len(sys.argv) <= 2:
        print("Usage: shout F ... where F is a file name")
    else:
        for filename in sys.argv[2:]:
            f = open(filename)
            for line in f:
                print(line.upper(), end='')
            f.close()



elif sys.argv[1].lower() == 'l33t':
    print("Talk geeky to me")
else:
    print("Usage: " + sys.argv[0] + " sum|product|average|shout|l33t")



# 2.  **product** multiply the remaining numeric arguments together and print the result
# 3.  **average** compute the arithmetic mean of the remaining numeric arguments and print the   result
# 4.  **shout** print the contents of the files given as arguments, but in upper-case
# 5.  **l33t** print the contents of the files given as arguments, but in l337$pe4|<


#  Let's write a program fulfilling these requirements:
#
#  3.  Display a specific help message when a correct subcommand is given, but too
#     few arguments are supplied.  Exit immediatly.


