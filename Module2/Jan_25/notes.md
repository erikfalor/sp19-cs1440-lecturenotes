# CS1440 - Fri Jan 25 - Module 2

# Announcements

## FSLC website is online!

https://usufslc.com/


## BSidesSLC 2019 Cyber Security Conference

https://www.bsidesslc.org

Build, Break, Network, Learn, & Give Back!   

Friday, February 22

10333 S Jordan Gateway, South Jordan, Utah, 84095

### BSidesSLC Student Scholarship Program
https://www.bsidesslc.org/scholarship

*Interested students must submit a proposal by Friday, February 1st, 2018*


# Topics:
* Scrum stand up meeting
* Using command line arguments to dispatch control


----------------------------------------------------------------------------
# Scrum stand up meeting

DuckieCorp strives to be an agile software shop by employing the most trendy
software development methodologies.  One trendy methodology is called "Scrum",
where teams meet for a short time on a daily basis and talk about how their
work is progressing.  Since our class doesn't meet every day we'll start by
doing stand ups on an as-needed basis (if you ask any developer who works at a
Scrum shop they'll tell you that they aren't doing "pure" Scrum... so this is
my excuse for this discrepancy ;)

It is important to stay on track as you enter the final stretch of this
assignment.  You don't have time to waste spinning your wheels on unproductive
tasks.  The stereotype in software development is that a 1 week project morphs
into a 2 week quagmire.  The gimmick behind the Scrum "stand up" meeting is to
notice this schedule slippage early and address it ASAP.

It's called a "stand up" meeting because it's supposed to be brief.
Out-of-shape programmers don't like to stand for more than a few minutes.
Each member of the team takes turns answering these questions:

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?

#### Impediment: Something that impedes; a hindrance or obstruction
> -- from The American Heritage(R) Dictionary of the English Language, 4th Ed

Of these three questions, the last is the magic one.  You will probably realize
your own solution by putting your impediments into words ("Restate the
Problem").  Failing that, it's quite likely that another member of your team
has encountered a similar problem and can offer good advice.


Write your answers for me on a mud card for today's participation points.


*   What are some things that are working well for you?
    *   Experimentation - Trial & Error - __REPL is life__
    *   Break it down into smaller pieces - Divide the Problem
    *   Make an outline
    *   Setting deadlines
    *   Google it - ask for help
    *   Use git to commit & push work - lets you see that you're actually making progress

*   What are some impediments that you're encountering?
    *   Unfamiliarity with the Python language
    *   PC Configuration issues
    *   Unfamiliarity with the tools, esp. git
    *   Misunderstanding of assignment requirements
    *   How do I use sys.argv?

*   What is your plan to overcome your barriers?
    *   Tutoring lab - reaching out to others
    *   Start with what you know - identify the "unknown unknowns" so you know what to research
    *   The Python Console (REPL) is very helpful
    *   The Python Intro - refresh you memory on the basics








----------------------------------------------------------------------------
# Using command line arguments to dispatch control

Let's write a program which does something different depending on the arguments
it is given.  It should work like `git` in the sense that one program called
`git` can do many different things depending upon the first argument we give
it.

### What are some of the things that git can do?

* add changes
* init a repository (or clone a repository)
* create branches
* view the log

We'll call the first argument to a program like `git` a *subcommand*.  What the
remaining arguments mean depend upon what the subcommand is. Let's write a
program with these subcommands:

1.  **sum** add the remaining numeric arguments together and print the result
2.  **product** multiply the remaining numeric arguments together and print the result
3.  **average** compute the arithmetic mean of the remaining numeric arguments and print the result
4.  **shout** print the contents of the files given as arguments, but in upper-case
5.  **l33t** print the contents of the files given as arguments, but in l337$pe4|<


Let's write a program fulfilling these requirements:

1.  Display a generic help message if the user enters no subcommand, or if an
    incorrect subcommand is given.  Exit immediatly.
2.  Convert the user's subcommand into lowercase before checking if it's
    supported.  This will make it be friendly to Windows users who are not used
    to case-sensitive systems.  Exit immediatly.
3.  Display a specific help message when a correct subcommand is given, but too
    few arguments are supplied.  Exit immediatly.


What Python concepts will we need to use to do this?

* sys.argv to get user's input
* convert letters into 133t5p34|<
* conditionals - if/else, compare strings with == operator
* loop over file input
* loop over numbers
* convert case of letters
* handle input from cmdline - import sys
* string methods such as conversion to lower/uppercase
* compare strings with ==
* if/else tree


Refer the example programs [driver-001.py](driver-001.py) and [driver-002.py](driver-002.py)
