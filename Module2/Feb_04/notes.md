# CS1440 - Mon Feb 04 - Module 2


# Announcements


## SDL/USU Technical Lecture - Engineering: Finding the Great Parts of the Grind
Tom Russell, EE - SDL

Please join us for the next SDL/USU Technical Lecture Series at 4:30 pm on
Tuesday, February. 5 in ENGR 201. Free pizza after lecture. 



## AIS - Real World Cyber Security

Tuesday Feb 5 6pm - 7pm
Huntsman Hall 322

Jon Parker, IT Director  of Enterprise Info Management, Melaleuca
Josh Rolfe, IT Senior Manager of Sec Ops, Melaleuca



## FSLC Meeting

The Vim vs. Emacs Smackdown!!!
Wednesday Feb 6
7pm - ESLC 053



## DC435 Meetup - Packet Capture Games

Have you ever wanted to know what your ISP can see about you? What about your
employer? Or someone else on your hotel wifi with you?

Come learn about network packets and play some **Wireshark** packet capture games

https://dc435.org/blog/2016-12-17-making-sense-of-the-scaas-new-flavor-wheel/

Thursday, Feb 7th 7pm
B Tech West Campus - 1410 North 1000 West





# Topics:

* Exam #0 Review



## Problem Solving

*   What is a meta-algorithm?
    The algorithm you follow to solve your programming problems

*   Explain in your own words or give an example of each of the General
    Problem-Solving Techniques:
    *   Always Have a Plan
    *   Divide the Problem  
    *   Reduce the Problem - reducing the search space
    *   Start with What You Know
    *   Restate the Problem
    *   Look for Analogies
    *   Experiment
    *   Don't get Frustrated


*   Python's Read Eval Print Loop (REPL) supports which problem-solving technique?
    Experiment - try new stuff, see what happens

*   Wolf-Fence debugging represents which problem-solving technique?
    Divide the problem - home in on the solution



## "The Tar Pit"

*   What is a Program?
    A standalone executable

*   What is a Programming Product?
    Can be deployed to other systems, is documented
    3x as big as a program

*   What is a Programming System?
    Several programs designed to work together; integrated
    3x as big as the component programs

*   What is a Programming Systems Product?
    the union of a Programming Product with a Programming System
    9x as big as a standalone program

*   According to Brooks, why is debugging hard?
    We won't see our own bugs
    The first few bugs are easy to find; as you go on they become progressively more difficult



## IDEs

*   What is an IDE?
    Integrated Development Environment
    Several tools in one place under a unified interface

*   What is a Text Editor?
    Programmer's Editors:
    *   Vim,
    *   Emacs,
    *   Notepad++,
    *   Sublime,
    *   Atom


*   Which is the best, an IDE or a Text Editor?
    Whichever you like more.  Stay out of holy wars if you can

*   Do you have to use an IDE in order to write programs?



## Python Language

*   What does a "slice" look like in Python syntax?
    l[startindex : endindex]

*   Which variable contains command-line arguments for the entire program?
    sys.argv

*   Which type of collection are a Python program's command-line arguments stored in?
    a list

*   What is the type of elements in the command-line argument collection?
    string

*   What is contained in element 0 of the command-line argument collection?
    sys.argv[0] = THE NAME OF THE PROGRAM

*   What are the four basic operations that all programming languages allow you to perform on files:
    open, read, write, close

*   Which file method returns the next line of text from an open file?
    .readline()

*   What happens when your program attempts to read from the end of an open file?
    an empty string is returned

*   What is the Python idiom for reading a file one line at a time?
    for line in file:
        pass # Do something useful

*   When in doubt, which two functions should you consult in the REPL?
    help() and dir()



## Paths

*   Explain the difference between absolute vs. relative paths
    Relative path == directions that make sense in Logan "go to 10th west"
    If you're not in Logan "10th west" means something different.
    An absolute path would be "10th west in Logan, UT, USA, Earth, Sol System,
    Saggitarius Arm, Milky Way Galaxy, Local Group, Virgo Supercluster"

*   What is the "current directory"?
    "Where you are right now" on the filesystem; the base on which relative
    paths may be understood.

*   Why is it considered bad practice to hard code paths into your program?
    That program may not work on another computer, or it will fail if you run
    it from a different directory.



## Modules

*   Are Modules == Classes?
    NO!!!

*   How do you write a module in Python?
    just write a file with '.py' extension

*   In the moment that a module is imported, what happens to any code in the
    module that is not contained within a function?
    It is immediately run upon import

*   What's the difference between `import Concatenate` and `from Concatenate import cat`?
    The former makes it so I must write `Concatenate.cat(),` whereas the latter
    lets me simply write `cat()`

*   What happens when two identifiers sharing the same name but residing in
    different modules are both imported into the current namespace?
    The last one encountered overrides the first



## Git

*   Should you save up all of your changes in order to make a single git commit at the end of the workday?

*   Should you put all code for of your projects into the same repository?

*   How do I do the following?
    *   See which files have been changed in the working tree since the last commit?

    *   List the commit history of a repo?

    *   Select the files that should be included in a new commit?     

    *   Permanently records the staged changes into the repository?     

    *   Get help when I'm stuck?
