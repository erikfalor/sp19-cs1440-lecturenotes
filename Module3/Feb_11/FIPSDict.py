
# A dictionary created with the dict() constructor function
d = dict(name="Erik Falor", anumber="Nice Try ;)", phone=4357974118)

# Insert new key/value pairs by assigning to subscripts
d['eye color']  = "Brown"
d['hair color'] = "Brown"

# Assinging a new value to an existing subscript overwrites the old value; each
# key is associated with one and only one value
d['eye color']  = "Blue"
d['hair color'] = "Blue"

# I can "invert" a dictionary's key/value pairs at the risk of losing data when
# a duplicate value becomes a key; one of the old keys (a new value) is lost
newDict = {}
for k in d:
    val = d[k]
    newDict[val] = k



## Practical example: converting a CSV file into a dictionary for quick access
f = open('states_postal_fips.csv')

f.readline() # throw the CSV header line away

# empty dictionaries to fill
fipsToState = {}  # map FIPS area codes to State Names
abbrToState = {}  # map USPS state abbreviations to State Names
abbrToFips = {}   # map USPS state abbreviations to FIPS area codes

for line in f:
    line = line.strip()
    fields = line.split(',')
    fipsToState[fields[2]] = fields[0]
    abbrToState[fields[1]] = fields[0]

f.close()
