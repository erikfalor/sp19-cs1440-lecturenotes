# CS1440 - Mon Feb 11 - Module 3

# Announcements

## AIS Security SIG meeting

Military Intelligence personnel will present on "Social Engineering" and
Interrogation.

6pm Tuesday Feb 12 
Huntsman Hall 326



## FSLC Movie Night

This Wednesday night take a load off and laugh at a Hollywood movie's
representation of "hacking" and "computering".

7pm Wednesday Feb 13
ESLC 053




## Engineers Without Borders (EWB)

Travel to Peru, Mexico or Southern Utah and make a difference with your
engineering skills!

ewbusu@gmail.com


* Peru - Install Water filters
Thursday 7pm ENGR 304

* Mexico - bypass pipeline
Monday 5:30pm ENGR 221

* Domestic team - 2ndary irrigation system for Navajo
Wednesday 5pm ENGR 204




# Topics:

* Retrospective insights
* Assignment #3 stand up meeting
* Python's Advanced Built-in Data Structures


----------------------------------------------------------------------------
# Retrospective insights
        _
    .__(.)<
     \___)   

## A selection of remarks from your sticky notes 

> I don't need to rewrite Python's built-in functions

> I learned that programming at 11:00pm is a very bad idea

> I went through the effort of figuring out how to use the debugger

> Plan before coding!!! It actually helps!!

> I've come to realize that experimenting is a really big part of learning CS

> When I got stuck I used to stare at my code until I got an idea, but I
> realized on this assignment how valuable experimenting is, even if your
> experiment fails.  It's also SUPER helpful to have a pen & paper in front of
> you when you code

----------------------------------------------------------------------------
# Assignment #3 stand up meeting

`scrum`

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?


*   What are some things that are working well for you?
    *   Actually made a plan before coding
    *   The Text Tool is *awesome*
*   What are some impediments that you're encountering?
    *   Figuring out what the assignment is asking
    *   Time management
*   What is your plan to overcome your barriers?
    *   Proactive - get an early start
    *   Write notes as I go and bring my questions to the tutors
    *   Notice when I'm getting stuck, and switch to something else




----------------------------------------------------------------------------
# Python's Advanced Built-in Data Structures

[../Readings_and_Resources.md](Python's Advanced Built-in Data Structures)
