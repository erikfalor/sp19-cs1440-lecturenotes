# CS1440 - Wed Feb 13 - Module 3

# Announcements

## STEM career fair TODAY!!!!!

TSC Ballroom - until 7pm (or so)



## Next Week's Assigned Reading

Wednesday Feb 20: Chapter 6 of *The Mythical Man-Month* - "Passing the Word"
Wednesday Feb 27: Chapters 7 and 8 of *The Pragmatic Programmer* 


## FSLC 90's Hacker Movie Night

It is roughly that time for mid-terms and we thought since many of you have
either taken your tests or studying your hearts out we should have a chill
night tonight. We are looking at watching a cheesy 90s movie called *Johnny
Mnemonic*: a Keanu Reeves action movie that has VR, Brain implants, and more!
Here is the trailer: https://www.youtube.com/watch?v=U_8BVWHSU_o

Tonight at 7pm in room 053 in the ESLC.




## Lucid Code Kerfuffle | Coding Competition for Prizes Totaling $50k

Free to enter
Saturday, March 9th 10am - 1pm
Register and fight online: https://codekerfuffle.com/
Requires a HackerRank account

Top 32 performers win $500 and an invitation to the on-site championship




# Topics:
* Discussion: "No Silver Bullet" by Fred Brooks, Jr.
* Improving Performance


----------------------------------------------------------------------------
# Discussion: "No Silver Bullet" by Fred Brooks, Jr.

There are people who speak of a day when computer programming as a profession
will become obsolete as technology improves to the point where humans will not
be needed to write software.

### Essence & Accident in Software Engineering

This idea is borrowed from Aristotle's Metaphysics


#### Essence  = aspects inherent to Software Engineering

#### Accident = aspects which attend Software Engineering, but are not inherent to it


### Questions for discussion:

* What are some difficulties in programming which you deem to be "essential"?
    *   Logic
    *   Understanding the client's needs
    *   Understanding someone else's code
    *   Making it work efficiently
    *   Deciding how to distribute tasks/work between workers
    *   Deciding how to divide a problem into subproblems

* What are some difficulties in programming which you deem to be "accidental"?
    *   Debugging
    *   Syntax
    *   Formatting
    *   CS 1, 2 & 3 - learning specific techniques

* To you, what is the difference?




### What is the gulf between the Essential and the Accidental?

Legend:
-------
    Essential  #
    Accidental -

Writing a compiler today:
##########-----
##########--

Writing the 1st FORTRAN compiler in 1953:
###########################-----------------------------------------------------



Rebuilding a building
------------------------------

Modern tools and building techniques enable us to build more-or-less identical
structures much more quickly today than in the past.


Examples of buildings which were completely destroyed and then rebuilt to the
same floorplan:


### Dresden Frauenkirche

https://en.wikipedia.org/wiki/Dresden_Frauenkirche
The first time the Dresden Frauenkirche was built, it was constructed between
1726 and 1743 (17 years).

It was rebuilt after WWII between 1994 and 2005 (11 years)

Original ######################################################################
Rebuild  #############################################




### Nauvoo Illinois LDS Temple

Original:
Cornerstone     April 6, 1841
Dedicated       April 30, 1846

Rebuild:
Announced      April 4, 1999
Groundbreaking October 24, 1999
Dedicated      June 27, 2002

Original ######################################################################
Rebuild  ####################################




### What does "No Silver Bullet" tell us about Software Engineering?

* "Program complexity depends on the language used to write it."

The complexity of solving a problem shouldn't be exacerbated through the use of
an overly complicated programming language. 


* "We need to change our mentality around how programs are created. We don't
  build programs, we grow them."

In order to build even larger systems, we need to think about providing a core
of functionality and adding other features onto it gradually. Instead of
building the whole thing all at once, we plant a seed and cultivate it.

* "Silver Bullet means the perfect solution. But programming doesn't have one
  specific answer. It has lots of complicated components that work together to
  create an answer."

In Fred Brooks' career software made huge advances in usability and
productivity. His assertion is that those gains came by solving the most
obvious problems with software - so-called "low-hanging fruit".

All of the easy problems had been solved by the time he wrote this essay, and
what we are left with are the truly difficult challenges inherent in software
engineering. Instead of waiting around for a new silver bullet to, his goal is
to encourage us to work towards slow and steady progress.



* Can AI or better programming languages save us?

You'll be relieved to note that Brooks doesn't expect to see AI putting you out
of a job.  Likewise for "Automatic" programming; if you've ever used a code
generator, you'll understand what he's talking about.  The sorts of things
which are easily tackled with this sort of automation were not the essentially
difficult parts to begin with.

What about "graphical" progamming languages?  People have been anticipating and
predicting such systems to finally make programming tractable.  To which I must ask:

    * How many of you have programmed in Scratch?
    * How big was you largest Scratch Sketch?




### "Assembly industry" vs. "process industry": what's the difference?

     _ _
    ( | ) First, one must observe that the anomaly is not that software
     V V  progress is so slow, but that computer hardware progress is so fast.
     No other technology since civilization began has seen six orders of
     magnitude in performance price gain in 30 years. In no other technology
     can one choose to take the gain in either improved performance or in  _ _
     reduced costs. These gains flow from the transformation of computer  ( | )
     manufacture from an assembly industry into a process industry.        V V
                                                    -- Frederick P. Brooks, Jr.

https://en.wikipedia.org/wiki/Discrete_manufacturing
https://en.wikipedia.org/wiki/Process_manufacturing

#### Assembly industry: where products are put together from parts

Examples: Automobiles, furniture, toys, smartphones, and airplanes


#### Process industry: products are undifferentiated
Examples: oil, natural gas and salt

Computers became cheap and plentiful because microchips result from a process
industry instead of a labor-intensive assembly industry.



### Promising Attacks on the Conceptual Essence

Be sure to study this section, as there are exam questions related to this.
Indeed, much of the purpose of this course is learning about the advances which
Brooks predicted.  These advances have come to pass and are, broadly speaking,
considered to be perfectly ordinary tools and techniques in 2018.





----------------------------------------------------------------------------
# Improving Performance

I have updated the section about
[Unordered Collections](../Readings_and_Resources.md#markdown-header-unordered-collections-sets-and-dictionaries)
in this module's Readings and Resources.


Knowing what to do to make your program faster largely comes down to
experience.  You need to gain a good understanding of what your program is
doing, how often it is doing it, and the relative expense of each operation.
You don't want to optimize the the wrong thing!

Consider the case of the wordlist lookup in the [Smart Caesar Cipher](smart_caesar/src/caesar.py)

By simply changing the file-based lookup to a Python list we see a massive
improvement in speed - at least two orders of magnitude.

By exchanging the list for a set another two-orders-of-magnitude speedup is
achieved.

https://en.wikipedia.org/wiki/Program_optimization#When_to_optimize



#### Storage Hierarchy

Storage is a trade-off between speed, size and cost.

1. CPU registers
2. CPU cache
3. Main memory (RAM)
4. Solid-State Disk (SSD)
5. Magnetic Disk (Hard Disk)
6. Optical Disk (DVD, CDROM, etc.)
7. Magnetic Tapes

* Given that both traditional hard disks and SSDs are slower than RAM, how many
  times should I read through my CSV files?

* In a language like Python, how might you ensure that your data stays in the
  CPU cache?

