#!/usr/bin/python3

## Look for TODO comments and add your code there

import sys
import time


## Functions to check whether a plaintext word is English

def wordInCollection(word, collection):
    """Unified interface for checking whether a word occurs in a collection"""
    if isinstance(collection, str):
        return wordInFile(word, collection)
    else:
        return word in collection


def wordInFile(word, fil):
    """Predicate: is `word` contained in file `fil`?
    Note: `item in TextIOWrapper` matches lines in a textfile; this means that
    `item` must end with the EOL symbol.
    
    Therefore, I must manually append "\n" for this use case
    """
    word += "\n"
    f = open(fil)
    found = word in f
    f.close()
    return found


def wordsToList(fil):
    """Given a file containing a list of words, return a list of those words"""
    f = open(fil)
    words = []

    for line in f:
        words.append(line.strip())

    f.close()
    return words


def wordsToTuple(fil):
    """Given a file containing a list of words, return a tuple of those words"""
    return tuple(wordsToList(fil))


def wordsToSet(fil):
    """Given a file containing a list of words, return a set of those words"""
    return set(wordsToList(fil))



## Functions for decrypting text files

def rotate(c, n):
    """Rotate a single alphabetic character.
    Non-alphabetic characters are returned unchanged"""
    o = ord(c)
    if c.islower():
        if o + n > ord('z'):
            return chr(o + n - 26)
        else:
            return chr(o + n)
    elif c.isupper():
        if o + n > ord('Z'):
            return chr(o + n - 26)
        else:
            return chr(o + n)
    else:
        return c;


def unscramble(fil, dist):
    """Given a filename, return a list of characters rotated by distance `dist`"""
    buf = []
    secret = open(fil)
    for line in secret:
        for c in line:
            buf.append(rotate(c, dist))
    secret.close()
    return buf



# Main body of code
def usage():
    print(f"Usage: {sys.argv[0]} FILENAME -file|-list|-tuple|-set\n")
    sys.exit(1)


if len(sys.argv) > 2 and sys.argv[2] in ('-file', '-list', '-tuple', '-set'):

    # Note the time when this program began
    began = time.time()

    if sys.argv[2] == '-file':
        print("Checking words against an English dictionary stored in a file")
        wordlist = 'data/words'
    elif sys.argv[2] == '-list':
        print("Checking words against an English dictionary stored in a list")
        wordlist = wordsToList('data/words')
    elif sys.argv[2] == '-tuple':
        print("Checking words against an English dictionary stored in a tuple")
        wordlist = wordsToTuple('data/words')
    elif sys.argv[2] == '-set':
        print("Checking words against an English dictionary stored in a set")
        wordlist = wordsToSet('data/words')
    else:
        usage()

    cryptedFile = sys.argv[1]
    rotation = 0
    while (rotation < 26):
        plaintext = ''.join(unscramble(cryptedFile, rotation))
        plainwords = plaintext.split()
        nPlainwords = len(plainwords)

        # count the number of legible words in the deciphered plaintext
        goodWords = 0
        for word in plainwords:
            if wordInCollection(word, wordlist):
                goodWords += 1

        # print plaintext when it's mostly legible
        if goodWords / nPlainwords > .2:
            print("\n=====================================================\n"
                    f"Input was rotated by {26 - rotation:2} positions "
                    f"({goodWords / nPlainwords:.2%} confidence)\n"
                    "=====================================================")
            print(plaintext)

        rotation += 1
    finished = time.time()
    print(f"Done in {finished - began:.3f} seconds!")

else:
    usage()
