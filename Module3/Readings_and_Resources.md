# Table of Contents


* Assigned Reading
* Running your Python Text Tools from another directory
* Intermediate git
* Python's Advanced Data Structures



----------------------------------------------------------------------------
# Assigned Reading

* Wednesday Feb 13: Chapter 16 of *The Mythical Man-Month* - "No Silver Bullet"
* Wednesday Feb 20: Chapter 6 of *The Mythical Man-Month* - "Passing the Word"
* Wednesday Feb 27: Chapters 7 and 8 of *The Pragmatic Programmer* 




----------------------------------------------------------------------------
# Intermediate git

Here are some new git commands that will give you more detailed information
about the state and structure of your repository.

### `git help glossary`

* Display a manual page defining bits of git jargon


### Working tree

* The files tracked by the git repository which are presently checked out,
  along with any new or changed files.
* New commits are formed with the `git add` command by computing the
  differences in the contents of the working tree with the most recently
  recorded commit.  This new commit is then permanently recorded by running the
  `git commit` command.


### `git diff`

* Display the difference between your source code files (the working tree) and
  the contents of git's last commit.  This shows you what will be recorded with
  `git add` followed by `git commit`

**Important**: press `q` to quit the diff viewer.



### `git config --global log.decorate true`

* Display extra branch/commit name information in the output of `git log`



### `git log`

* Display the commit history from the current commit back to the genesis of the
  repository.
* Also: `git log --stat` - displays a brief summary of files and their changes
* Also: `git log --patch` - displays the diff for that patch
* Also: `git log --stat --patch` - show all of it at once

**Important**: press `q` to quit the log.



### SHA-1 Object Name

* This *true name* of a commit is a cryptographic checksum of the author's name
  and email address, timestamp, commit message, as well as the checksum of the
  contents of the commit.  This cryptographically-strong digital fingerprint of
  the commit prevents tampering with the history of the repository.
* A commit's SHA-1 object name also incorporates the SHA-1 object name of its
  parent commits, which, in turn, depend upon their parents' checksums, etc.
  Thus, git repositories were blockchains before blockchains were cool.
* This name takes the form of a 40 character-long string of hexadecimal digits
  (0 through 9, A through F), e.g. `f7e8295498512363f5cd0b12459e548ca80f329f`  

You can always refer to commit with this universally unique identifier.
Because sequences of 40 arbitrary characters are hard for humans to remember,
git gives us a few shortcuts, explained below.  Any commit may have multiple
names at once, but it always has at least one SHA-1 object name.

### `HEAD`

* This is always the name of the commit you are on RIGHT NOW.  Analogous to the
  notion of *current working directory* to the command shell.


## Git commands which operate on commit objects

You may use any of the following names in the commands described below when
that command needs you to refer to an *OBJECT*.  Understand that in the
foregoing discussion `OBJECT` refers *only* to git commits.  A file or
directory is *never* an `OBJECT`.

1.  An SHA-1 object name, which may be abbreviated to the first 7 or 8
    characters
2.  A relative reference such as `HEAD`
3.  A tag name
4.  A branch name such as `master`



### `git log OBJECT`

* This form of the `git log` command displays the commit history beginning from
  the commit denoted by OBJECT back to the genesis of the repository.
* Any commits which were added *after* `OBJECT` are not listed in this output.


### `git tag TAGNAME`

* Names the current (HEAD) commit `TAGNAME`.
* The tag sticks to this commit and does not follow the branch along as you add
  commits.


### `git tag TAGNAME OBJECT`

* Grant the name `TAGNAME` to the object referred to by `OBJECT`.
* `OBJECT` can be any object anywhere in your repository.
* A tag is a more human-friendly name for a commit.
* A tag always refers to the same commit, even after new commits are added to
  the repository.
* A tag exists only in your local repository until you push it.


### `git tag -d TAGNAME`

* Remove a tag from a commit.
* This command does not modify or delete the commit itself.



### `git push REMOTE TAGNAME`

* Send the name of a tag along with the commit to which it points to REMOTE
  repository.
* Tags are not pushed unless you explicitly instruct git to push them.  If you
  forget to explicitly push your `Assn2` tag, for instance, we won't know which
  commit to grade.  


### `git push --delete REMOTE TAGNAME`

* Remove a tag from REMOTE repository.  Use this if you tagged the wrong
  commit.


### `git diff OBJECT`
* List the differences between `OBJECT` and `HEAD`.
* In other words, what changes would I need to make to the code as checked-in
  at `OBJECT` to make it become the same as `HEAD`?


### `git checkout OBJECT`
* Make the working tree become identical to the state captured by `OBJECT`.
  Where you are is now `HEAD`.
* This is how you travel back in time with git
* You can return to your previous location with `git checkout -`
* Return to your latest commit on the master branch with `git checkout master`



----------------------------------------------------------------------------
# Running your Python Text Tools from another directory


## Linux and Mac

On your OS executable programs are not designated by their name but by the
*mode* of the file.

Mark `tt.py` with the executable mode with the `chmod` program `$ chmod +x tt.py`

Find the name of the directory containing `tt.py` with the `pwd` command:

```shell
$ pwd
/home/fadein/1440/Assn/2/src
```

Add that directory to your shell's `$PATH` environment variable.  If you are
using the **bash** shell (most of you are) you will add a line of code
resembling this example to the bottom of a file in your user's home
directory named `.bashrc`.  Be sure to replace the name of my directory with
the name you found above.  

```
PATH=/home/fadein/1440/Assn/2/src:$PATH
```

If the name of your home directory happens to contain space characters, surround your home directory's name with quote marks:
```
PATH="/home/Erik Falor/1440/Assn/2/src":$PATH
```


## Windows

On your computer the OS decides what to do with a file based upon its name,
specifically the last characters following the final '.' in the filename.
While we *could* configure Windows to recognize '.py' files as executables to be
run with the help of `python.exe`, your command-line arguments are lost in the
process.  Instead, we will create a Batch file ('.bat' extension) that will in
turn run Anaconda's `python.exe` with your `tt.py` script, forwarding your
command-line arguments from the Batch file to Python.

In the `src/` directory of your project, create a file called `tt.bat` with
contents like this:

```batch
@ echo off
python "%USERPROFILE%\Desktop\cs1440-falor-erik-assn2\tt.py" %1 %2 %3 %4 %5 %6 %7 %8 %9
```

Modify the absolute path to your copy of `tt.py` as appropriate.  The double
quotes are present in case the name of your home directory contains spaces.


Next, configure the %PATH% environment variable for your user by going into the
Control Panel and searching for "Environment Variables".  You will find an
option with a name like "Edit environment variables for your account".  If a
variable call "Path" is shown, select and edit it.  Otherwise, add a new entry
with the name "Path".  Add the absolute path name of the directory containing
the file `tt.bat`.  Take care to not add the name of the batch file itself; you
need only enter the name of the directory it is in.

You must open a new Anaconda Prompt to enjoy the effect.  You may now run your
text tools program with the command name `tt`.


----------------------------------------------------------------------------
# Python's Advanced Built-in Data Structures


In this assignment you'll need the right data structure for the job.  So far we
have been using *scalar* and *list* values in our programs.

_**Scalar**_: an atomic quantity that can hold only one value at a time

_Example_: The number `1` is a single value


_**List**_: an ordered sequence of values

_Example_: `sys.argv` is a list of strings


Strings seem to blur the line a little.  We tend to thing of them as a single
value, but they are actually an ordered sequence of characters.

Python has are other types of sequences besides lists and strings.


## Ordered Sequence Collections: Strings, Lists, Tuples and Ranges

-   [Python Intro - Strings](https://usu.instructure.com/courses/474722/pages/strings)
-   [Python Intro - Lists](https://usu.instructure.com/courses/474722/pages/lists)
-   [Python Intro - Tuples and Ranges](https://usu.instructure.com/courses/474722/pages/tuples-and-ranges)
-   [Python Intro - Slicing](https://usu.instructure.com/courses/474722/pages/slicing)
-   [PythonWiki - HowTo/Sorting](https://wiki.python.org/moin/HowTo/Sorting)

There area two ways to create each of these data structures: **constructor
functions** and **syntax**.

_**Constructor**_: A function which initializes and returns a new object

_**Syntax**_: the set of rules that defines the combinations of symbols that
are considered to be a correctly structured program


Sequence types store their contents *in order*.  There is a first element, a
last element, and the elements in between all know their place.  A list is
created with the `list()` constructor function or with square brackets `[]`.
Contrary to your expectations (and the beautiful example set forth by LISP),
the `list()` constructor function does not accept multiple arguments; `list()`
is meant to convert other sequence types (tuples, strings, ranges...) into a
list.

Python is a "dynamically typed" language.  This means that variables in Python
aren't fixed with respect to the type of value they may refer to.  For instance
a variable `h` may refer to an integer value at one moment, then it may be
assigned a list value, and later it can refer to an object.

This is in contrast to a "statically typed" language such as Java, where a
variable is permanently associated with a type, and that variable may only
refer to values of that type for the duration of the program.  In such a
language it is an error to attempt to assign a `string` value to a variable
declared as type `int`.

You may use the Python function `type()` to discover the type of a value.

### Lists

    >>> l = [1, 2, 3, 4, 5] # list created with square brace syntax
    >>> l
    [1, 2, 3, 4, 5]
    >>> type(l)
    <class 'list'>  
    >>> l[2] = 2222         # reassigning an element within a list
    >>> l  
    [1, 2, 2222, 4, 5]
    >>> s = list("list of chars in string")  # list created with the list() constructor function
    >>> s
    ['l', 'i', 's', 't', ' ', 'o', 'f', ' ', 'c', 'h', 'a', 'r', 's', ' ', 'i', 'n', ' ', 's', 't', 'r', 'i', 'n', 'g']
    >>> empty = list()
    >>> type(empty)
    <class 'list'>


### Tuples

A tuple is an immutable list. Tuples are created with the tuple() constructor
or with parentheses:

    >>> t = tuple([1, 2, 3, 4, 5]) # convert a list into a tuple
    >>> t
    (1, 2, 3, 4, 5)
    >>> type(t)
    <class 'tuple'>  
    >>> t[2] = 2222   # OOPS!  
    Traceback (most recent call last):  
      File "<stdin>", line 1, in <module>  
    TypeError: 'tuple' object does not support item assignment  
    >>> t = (7, 8, 9, 10)    # syntax of a tuple literal


### Ranges

A range gives us a way to efficiently store a huge sequence of integers in
memory without actually storing all of the integers required. Basically, it
only needs to store the beginning, the end, and the "step" (counting by twos,
counting by fives, etc.). The range can be turned into an honest to goodness
list with the list() constructor (provided you have enough RAM)

    >>> r = range(10000000000000)
    >>> r
    range(0, 10000000000000)
    >>> l = list(range(10000000000000))
    Traceback (most recent call last):
      File "", line 1, in 
    MemoryError

Sequence types are ordered and may be sorted. The Python Wiki has an article
about [sorting](https://wiki.python.org/moin/HowTo/Sorting) with some useful
recipes for sorting lists of varying degrees of complexity.



## Unordered Collections: Sets and Dictionaries

In contrast to sequences, sets and dictionaries are *not* ordered. The concept
of "first" or "next" doesn't make sense with regard to these collections.

Of course, the computer literally stores them in _some_ order in memory.  To
the user of the data structure this ordering may appear random.  Your programs
should not count on the contents of these structures being in any particular
order as the order can vary between versions of Python, across different
computers, or even between different runs of the same program.

Like lists and tuples, there are two ways to make each of these types, a
constructor function and syntax.


### Sets

Sets are constructed with the `set()` constructor function or as a
comma-separated list of values within curly braces {}

    >>> s = set(range(7))
    >>> s
    {0, 1, 2, 3, 4, 5, 6}
    >>> type(s)
    <class 'set'>
    >>> s.add(20)
    >>> s
    {0, 1, 2, 3, 4, 5, 6, 20}
    >>> 6 in s
    True


* How do we add an item to a set?
    s.add('an item')

* How do we ask whether an item is present in a set?
    'the key' in s

* How do we retrieve an item from a set?
    This is a trick question - there are no items in a set.  Only keys

* How do we remove an item from a set?
    s.discard('a key')

* n.b. with sets, the subscript notation just doesn't work.





### Dictionaries

Dictionaries are constructed with the `dict()` constructor function, or as a
comma-separated list of 'key': value pairs within curly braces {}.  The
presence of the colon `:` character between pairs is syntactically significant;
if you forget to use it Python will create a set instead.


    # There are two ways to make a dictionary:
    d = dict(name="Erik Falor", anumber="Nice Try ;)", phone=4357974118)
    d = {'name' : "Erik Falor", 'anumber' : "Nice Try ;)", 'phone' : 4357974118}

    # When writing a literal value, the only difference between a dict and a
    # set is the presence of colons ':'
    d = {'name', "Erik Falor", 'anumber', "Nice Try ;)", 'phone', 4357974118}
    type(d)   # what does this say?


Items in a dictionary are accessed with a subscript written within square
brackets [].  Unlike a list which is subscripted with integers, the subscript
to a dictionary may be a string, an integer, a tuple, among others.

    >>> d = dict(name="Erik Falor", anumber="Nice Try ;)", phone=4357974118)
    >>> d
    {'name': 'Erik Falor', 'anumber': 'Nice Try ;)', 'phone': 4357974118}
    >>> type(d)
    <class 'dict'>
    >>> d['name']
    'Erik Falor'  
    >>> d['astrological sign'] = 'Aries'


* How do we add an item to a dictionary?

    d['the key'] = 'a value'


* How do we ask whether an item is present in a dictionary?

    'the key' in d


* How do we retrieve a value from the dictionary?

    v = d.get('the key')
    v = d['the key']


* How do we remove an item from a dictionary?

    v = d.pop('the key')
    del d['the key']


* How do we "rename" an item in a dictionary?

By deleting it and adding under a new name:
    d['tres'] = d.pop('dos')
    


#### Uniqueness of keys

Keys in a dictionary are unique: there can be only one of each key.  When you
add an element to a dictionary under a key which already exists, you overwrite
the previous value that was there.

You can "invert" a dictionary's key/value pairs at the risk of losing data when
a duplicate value becomes a key; one of the old keys (a new value) is lost:

    >>> d = dict(name="Erik Falor", phone=4357974118, eyes="brown", hair="brown")
    >>> newDict = {}
    >>> for k in d:
    ...     val = d[k]
    ...     newDict[val] = k
    ...
    >>> print(len(d))
    4
    >>> print(len(newDict))
    3


-   [Sets](https://docs.python.org/3.6/library/stdtypes.html#set-types-set-frozenset)
-   [Dictionaries](https://usu.instructure.com/courses/474722/pages/dictionaries)
-   [More Data Structures](https://usu.instructure.com/courses/474722/pages/data-structures)
-   [Python Data Structures Tutorial](https://docs.python.org/3.6/tutorial/datastructures.html)



### Why are the contents of my sets and dictionaries in order in Python?

As you experiment with dictionaries and sets in Python you will undoubtedly
notice that the content of these collections don't appear to be random at all.

What you are seeing is an *accident*, or a side-effect, of the implementation
of Python these collections in Python version 3.6.  These collections have not
always behaved this way, nor should you expect that they will continue to
behave so in the future:

> The order-preserving aspect of this new implementation is considered an
> implementation detail and should not be relied upon (this may change in the
> future, but it is desired to have this new dict implementation in the
> language for a few releases before changing the language spec to mandate
> order-preserving semantics for all current and future Python implementations;

[Changes in Python 3.6](https://docs.python.org/3/whatsnew/3.6.html#new-dict-implementation)


For now, you can observe this behavior for yourself by running this code in the
REPL.  First, create a randomly shuffled list:

    >>> l = list(range(10))
    >>> l
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    >>> from random import shuffle
    >>> shuffle(l)
    >>> l
    [6, 4, 0, 2, 8, 9, 3, 1, 7, 5]

When you convert it into a set by calling the `set()` constructor function its
contents appear to snap into order:

    >>> s = set(l)
    >>> s
    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

You can abuse this "feature" of Python 3.6 to recover a sorted list by
converting the set back into a list through the `list()` Constructor:

    >>> l = list(s)
    >>> l
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]



## Questions to consider when choosing to use ordered or unordered collections

The theme of this module is "the right data structure for the job".  How do you
know which structure to use and when to use it?  Here are some questions to ask
yourself as you design your program:


#### Does it matter whether your input data is already sorted?  

Data contained in files is ordered; it makes sense to say that one line occurs
before another.  Whether the data is *sorted* is a different question.

* Does it matter to your program what data these lines appear in that file?

* Do you need to preserve the ordering of that data to obtain a correct result?

In the case of `area_titles.csv`, the data is sorted after a fashion: the FIPS
area codes for the entire U.S.A. come at the top of the file, then the states
and territories come next, in alphanumeric order, then, finally, the CSA and
MSA FIPS codes are presented.  


#### How will you access the data

* Lists versus Dictionaries/Sets: which data structure offers the easiest way
  to count unique and distinct elements?

* Can you use sets and dictionaries to more easily generate a top-10 list?

* Does it matter if you know that one piece of data "comes before" or "follows"
  another?  Ordered sequence containers contain this extra bit of information
  that is simply lost in the unordered ones.
