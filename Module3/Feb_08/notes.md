# CS1440 - Fri Feb 08 - Module 3

# Announcements


## Assigned Reading for next Wednesday, Feb 13

Chapter 16 of *The Mythical Man-Month* - "No Silver Bullet"


## AIS Security SIG meeting

Military Intelligence personnel will present on "Social Engineering" and
Interrogation.

6pm Tuesday Feb 12 
Huntsman Hall 326


# Topics:
* Exam #0 Recap
* Text Tools Retrospective
* Using Text Tools with Assignment 3



----------------------------------------------------------------------------
# Exam 0 Recap

__Question 1__ (74% answered correctly)
* Q:    What is a meta-algorithm?
* A:    An algorithm one follows to develop an algorithm


__Question 20__ (79% answered correctly)
* Q:    When in doubt, which two functions should you consult in the REPL?
* A:    `dir()` and `help()`



----------------------------------------------------------------------------
# Text Tools Retrospective

## Assignment 2 Effort Survey

This optional survey will help me develop better homework assignments.
Your participation is not required and all responses are anonymous.

[Section 001](https://usu.instructure.com/courses/531135/quizzes/714278)
[Section 002](https://usu.instructure.com/courses/527950/quizzes/714277)


## Retrospective: Profoundness and value of insights

For this retrospective I want you to consider the value of insights you gained
while working on this assingment, and the cost you paid to have it.

Think of an insight you gained.  It can be big or small, it can be something
you came up with on your own or something that your ducky told you.  Just pick
the first thing that comes to mind.

Now, think about how *big* of an idea it is.  Is it something that you're
surprised you (or your ducky) came up with?  How big of an impact did it have
on your work?

Post it on the whiteboard according to these axes:

Axes:
    * Profoundness - how big or important this insight was to you
    * Value - the cost of not realizing it; how useful it was once obtained

This assignment ran from Weds 1/23 through Weds 2/6


`retrospect`



----------------------------------------------------------------------------
# Using Text Tools with Assignment 3

[Running your Python Text Tools from another directory](../Readings_and_Resources.md#markdown-header-running-your-python-text-tools-from-another-directory)


## Things we can do with our text tools

* Measure the size our input files

* Count occurances of substrings

* Cut out undesired columns


## Forming smaller data files with our tools


## Write the "startgrep" text tool
