# CS1440 - Fri Feb 15 - Module 3

# Announcements

## No class on Monday in observance of President's day.



## Next Week's Assigned Reading

Wednesday Feb 20: Chapter 6 of *The Mythical Man-Month* - "Passing the Word"
Wednesday Feb 27: Chapters 7 and 8 of *The Pragmatic Programmer* 



## FSLC - Wireshark Tutorial

Wednesday 2/20
7pm - ESLC room 053

Wireshark® is a network protocol analyzer. It lets you capture and
interactively browse the traffic running on a computer network. It has a rich
and powerful feature set and is world's most popular tool of its kind. It runs
on most computing platforms including Windows, macOS, Linux, and UNIX. Network
professionals, security experts, developers, and educators around the world use
it regularly. It is freely available as open source, and is released under the
GNU General Public License version 2.



# Topics:

* Scrum stand-up meeting
* [Intermediate Git](../Readings_and_Resources.md#markdown-header-intermediate-git)


----------------------------------------------------------------------------
# Scrum stand-up meeting

Due to the upcoming holiday, this is our last meeting before the day the assignment is due.

Let's do a stand-up to check your progress:


*   What are some impediments that you're encountering?
    -   Understanding what the assignment is expecting
    -   Figuring out what we're supposed to be doing
    -   I haven't started yet
        -   Talk it over with your mom, roommates, ducky
        -   Translate the problem description into a plan in your own words
        -   Ask your Study Buddies and classmates on Piazza

    -   Reading and using the existing code
    -   I don't understand what the output samples mean
        -   Realize that the starter code is a working program
        -   Run it in the debugger and see how your changes affect it

    -   Converting area_titles.csv into a dictionary
        -   Examine code examples from previous lectures for similar algorithms
        -   Write a simple stand-alone program that does this for testing
            purposes

    -   Understanding how the FIPS codes work
        -   Review the BLS documentation provided on the assignment
        -   Realize that your program can decide whether to keep or discard a
            FIPS area solely by considering its FIPS code

    -   It takes a long time to run the program
        -   Work with smaller data sets
        -   Use your Text Tools to craft custom data sets for testing
        -   Worry about performance only *after* your program gives correct results
        -   In the worst case, leave a note for the grader begging for patience
            because your program does *eventually* finish

    -   I'm running out of Mt. Dew and Cheetos
        -   Don't code while you're hangry



----------------------------------------------------------------------------
# [Intermediate Git](../Readings_and_Resources.md#markdown-header-intermediate-git)


As you augment and modify your Python Text Tools, you should continue to commit
and push your changes to Bitbucket.  To prevent the TA's from regarding your
new commits as late submissions you should *tag* the final commit that formed
your Assignment 2 submission so that we can easily tell what you're up to.

Use a combination of the `git log`, `git tag` and `git push` commands as
explained in this module's Readings and Resources to do this.  In case you make
mistakes, the R&R document also describes git commands you can use to remove
tags both from your local repo as well as Bitbucket.


[../Readings_and_Resources.md](Intermediate Git)



## Command-line example of pushing a tag to remote repository

You may use these commands like this to create a tag for a particular commit
and push it to Bitbucket:

Read your commit history to find the ID of your final Assn2 commit

```
$ git log
```

Tag this commit in your local repo with the name `Assn2`

```
$ git tag Assn2 COMMIT_ID
```

Push that tag from your local repo to Bitbucket such that the grader will be
able to see it, too.

```
$ git push myrepo Assn2
```


