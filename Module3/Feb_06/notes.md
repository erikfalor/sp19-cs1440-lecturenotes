# CS1440 - Wed Feb 06 - Module 3

# Announcements


## FSLC Meeting

The Vim vs. Emacs Showdown!!!
Wednesday Feb 6
7pm - ESLC 053



## DC435 Meetup - Packet Capture Games

Have you ever wanted to know what your ISP can see about you? What about your
employer? Or someone else on your hotel wifi with you?

Come learn about network packets and play some **Wireshark** packet capture games

https://dc435.org/blog/2016-12-17-making-sense-of-the-scaas-new-flavor-wheel/

Thursday, Feb 7th 7pm
BTech West Campus - 1410 North 1000 West



## USU Spring STEM Career Fair

Wednesday Evening, February 13, 2019
3:00 pm – 7:00 pm
Taggart Student Center Ballroom

> Utah State University's Spring STEM Career Fair is a wonderful chance for
> students interested in working with technology-based organizations to spend
> some time networking with employers. It is also a chance for these employers
> to see what high quality students we have here at Utah State.






# Topics:

*   Text Tools Problems and Solutions
*   Introduce Assignment 3: Big Data Processing
*   Problem Solving: Divide vs. Reduce




----------------------------------------------------------------------------
# Text Tools Problems and Solutions


What are some problems you've encountered working on this assignment?

What are solutions that you have found?

Two for loops, the outer iterates over filenames
    The inner iterates over contents of the file
    

* grep is hard
    Use the 'in' operator
* uniq is hard
    You only need to consider the current line and the previous line.
* paste is hard
    try using ''.split() and/or ''.join()
* sort is hard
    read it into a list of lines, then use `.sort()`, then print the sorted loop
* tac is hard
    read it into a list of lines, then use `.reverse()`, then print the reversed loop


----------------------------------------------------------------------------
# Introduce Assignment 3: Big Data Processing

* [Section 001](https://usu.instructure.com/courses/531135/assignments/2606367)
* [Section 002](https://usu.instructure.com/courses/527950/assignments/2606378) 


## What is the problem that you have been asked to solve?


## Tour of the code

    git clone https://bitbucket.org/erikfalor/cs1440-falor-erik-assn3

*   What do you make of this code?
*   What tasks have been done for you?
*   What is left for you to do?


## Why can't I just use XYZ program to find the answer?

The full data set is 507916122 bytes or 485 megabytes in size.  Let's see what
happens when we attempt to open this file in some likely programs:

*   PyCharm
*   Atom
*   Sublime 3
*   LibreOffice Calc (Spreadsheet)
*   Nano
*   Emacs
*   Vim
*   Ed

