from colour import Color

# This is the OG gradient from the Assn5 starter code
hardcoded = [
        '#ffe4b5', '#ffe5b2', '#ffe7ae', '#ffe9ab', '#ffeaa8', '#ffeda4',
        '#ffefa1', '#fff29e', '#fff49a', '#fff797', '#fffb94', '#fffe90',
        '#fcff8d', '#f8ff8a', '#f4ff86', '#f0ff83', '#ebff80', '#e7ff7c',
        '#e2ff79', '#ddff76', '#d7ff72', '#d2ff6f', '#ccff6c', '#c6ff68',
        '#bfff65', '#b9ff62', '#b2ff5e', '#abff5b', '#a4ff58', '#9dff54',
        '#95ff51', '#8dff4e', '#85ff4a', '#7dff47', '#75ff44', '#6cff40',
        '#63ff3d', '#5aff3a', '#51ff36', '#47ff33', '#3eff30', '#34ff2c',
        '#2aff29', '#26ff2c', '#22ff30', '#1fff34', '#1cff38', '#18ff3d',
        '#15ff42', '#11ff47', '#0eff4c', '#0bff51', '#07ff57', '#04ff5d',
        '#01ff63', '#00fc69', '#00f970', '#00f677', '#00f27d', '#00ef83',
        '#00ec89', '#00e88e', '#00e594', '#00e299', '#00de9e', '#00dba3',
        '#00d8a7', '#00d4ab', '#00d1af', '#00ceb3', '#00cab7', '#00c7ba',
        '#00c4be', '#00c0c0', '#00b7bd', '#00adba', '#00a4b6', '#009cb3',
        '#0093b0', '#008bac', '#0082a9', '#007ba6', '#0073a2', '#006b9f',
        '#00649c', '#005d98', '#005695', '#004f92', '#00498e', '#00438b',
        '#003d88', '#003784', '#003181', '#002c7e', '#00277a', '#002277',
        ]


# Use Python's `colour` module to duplicate the `hardcoded` gradient
start = Color('#ffe4b5')
computed = list(start.range_to('#002277', len(hardcoded)))


# Make this gradient take 32 steps instead
short = list(start.range_to('#002277', 32))


# Make this gradient span over 256 steps
longer = list(start.range_to('#002277', 256))


# Make this gradient take 1024 steps instead
# Do you see a problem?
really_long = list(start.range_to('#002277', 1024))


# Unfortunately, `really_long` contains lots of duplicated colors.  By
# stretching my gradient over this many colors I have reduced the difference
# between them to such a degree that one step isn't always enough to take me
# from one color to the next.


# The solution is to create a gradient which spans its steps over multiple
# colors.  I can further increase the apparent contrast by interspersing the
# color black in between my bright, pure colors.
blk = Color('black')
red = Color('red')
grn = Color('green')
blu = Color('blue')

grad = list(red.range_to(blk, 128))

# Slice off the 0th element so that I don't have two adjacent copies of 'black'
grad += list(blk.range_to(grn, 128))[1:]
grad += list(grn.range_to(blk, 128))[1:]
grad += list(blk.range_to(blu, 128))[1:]
grad += list(blu.range_to(blk, 128))[1:]
grad += list(blk.range_to(red, 128))[1:]  # There and back again, a round trip back to red
