from math import sqrt


class Fib:
    phi = (1.0 + sqrt(5)) / 2.0
    psi = (1.0 - sqrt(5)) / 2.0
    recip_sqrt5 = 1.0 / sqrt(5)

    """Stream from the interval [start, end); end=None is an inf. stream"""
    def __init__(self, start=0, end=101):
        self.start = start
        self.pos = start
        self.end = end
        self.n = None

    def set_pos(self, new_pos):
        """Set the current position in the stream with error checking"""
        if self.start > new_pos:
            raise IndexError(f"New position {new_pos} can't come before the beginning position of {self.start}")
        elif (self.end is not None and new_pos >= self.end):
            raise IndexError(f"New position {new_pos} can't be >= the ending position of {self.end}")
        else:
            self.pos = new_pos

    def next(self):
        """Advance the stream by 1 position"""
        if self.end is None or self.pos < self.end:
            self.pos += 1

    def prev(self):
        """Rewind the stream by 1 position"""
        if self.pos >= self.start:
            self.pos -= 1

    def rewind(self):
        """Rewind the stream to start position"""
        self.set_pos(self.start)

    def run(self):
        """Automatically advance the stream, printing each value
        one line at a time, until the stream is exhausted"""
        while self.end is None or self.pos < self.end:
            self.next()
            n = self.current()
            if n is not None:
                print(n)

    def current(self):
        """Return the value of the stream at the current position"""
        if self.start < self.pos \
                and (self.end is None or self.pos < self.end):
            self.n = int(Fib.recip_sqrt5 * (Fib.phi ** self.pos - Fib.psi ** self.pos))
        else:
            self.n = None
        return self.n


class FizzBuzz:
    """Stream from the interval [start, end); end=None is an inf. stream"""
    def __init__(self, start=0, end=101):
        self.start = start
        self.end = end
        self.pos = start
        self.n = None

    def get_pos(self):
        """Return the current position in the stream"""
        return self.pos

    def set_pos(self, new_pos):
        """Set the current position in the stream"""
        if self.start > new_pos:
            raise IndexError(f"New position {new_pos} can't come before the beginning position of {self.start}")
        elif (self.end is not None and new_pos >= self.end):
            raise IndexError(f"New position {new_pos} can't be >= the ending position of {self.end}")
        else:
            self.pos = new_pos

    def next(self):
        """Advance the stream by 1 position"""
        if self.end is None or self.pos < self.end:
            self.pos += 1

    def prev(self):
        """Rewind the stream by 1 position"""
        if self.pos >= self.start:
            self.pos -= 1

    def rewind(self):
        """Rewind the stream to start position"""
        self.set_pos(self.start)

    def run(self):
        """Automatically advance the stream, printing each value
        one line at a time, until the stream is exhausted"""
        while self.end is None or self.pos < self.end:
            self.next()
            n = self.current()
            if n is not None:
                print(n)

    def current(self):
        """Return the value of the stream at the current position"""
        if self.start < self.pos \
                and (self.end is None or self.pos < self.end):
            val = ""
            if self.pos % 3 == 0:
                val = "Fizz"
            if self.pos % 5 == 0:
                val += "Buzz"
            if val == "":
                val = self.pos
            self.n = val
        else:
            self.n = None
        return self.n


class Pell:
    phi = (1.0 + sqrt(2.0))
    psi = (1.0 - sqrt(2.0))
    two_sqrt2 = 2.0 * sqrt(2.0)

    """Stream from the interval [start, end); end=None is an inf. stream"""
    def __init__(self, start=0, end=101):
        self.start = start
        self.pos = start
        self.end = end
        self.n = None

    def set_pos(self, new_pos):
        """Set the current position in the stream with error checking"""
        if self.start > new_pos:
            raise IndexError(f"New position {new_pos} can't come before the beginning position of {self.start}")
        elif (self.end is not None and new_pos >= self.end):
            raise IndexError(f"New position {new_pos} can't be >= the ending position of {self.end}")
        else:
            self.pos = new_pos

    def next(self):
        """Advance the stream by 1 position"""
        if self.end is None or self.pos < self.end:
            self.pos += 1

    def prev(self):
        """Rewind the stream by 1 position"""
        if self.pos >= self.start:
            self.pos -= 1

    def rewind(self):
        """Rewind the stream to start position"""
        self.set_pos(self.start)

    def run(self):
        """Automatically advance the stream, printing each value
        one line at a time, until the stream is exhausted"""
        while self.end is None or self.pos < self.end:
            self.next()
            n = self.current()
            if n is not None:
                print(n)

    def current(self):
        """Return the value of the stream at the current position"""
        if self.start < self.pos \
                and (self.end is None or self.pos < self.end):
            self.n = round((Pell.phi ** self.pos - Pell.psi ** self.pos)
                           / Pell.two_sqrt2)
        else:
            self.n = None
        return self.n


if __name__ == '__main__':
    import sys

    def usage():
        print(f"Usage: {sys.argv[0]} fib|fizzbuzz|pell [[START] END]")

    if len(sys.argv) >= 4:
        if sys.argv[1].lower() == 'fib':
            seq = Fib(start=int(sys.argv[2]), end=int(sys.argv[3]))
        elif sys.argv[1].lower() == 'fizzbuzz':
            seq = FizzBuzz(start=int(sys.argv[2]), end=int(sys.argv[3]))
        elif sys.argv[1].lower() == 'pell':
            seq = Pell(start=int(sys.argv[2]), end=int(sys.argv[3]))
        else:
            usage()
            sys.exit(1)

    elif len(sys.argv) == 3:
        if sys.argv[1].lower() == 'fib':
            seq = Fib(end=int(sys.argv[2]))
        elif sys.argv[1].lower() == 'fizzbuzz':
            seq = FizzBuzz(end=int(sys.argv[2]))
        elif sys.argv[1].lower() == 'pell':
            seq = Pell(end=int(sys.argv[2]))
        else:
            usage()
            sys.exit(1)

    elif len(sys.argv) == 2:
        if sys.argv[1].lower() == 'fib':
            seq = Fib()
        elif sys.argv[1].lower() == 'fizzbuzz':
            seq = FizzBuzz()
        elif sys.argv[1].lower() == 'pell':
            seq = Pell()
        else:
            usage()
            sys.exit(1)
    else:
        seq = Fib()

    seq.run()
