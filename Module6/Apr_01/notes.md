# CS1440 - Mon Apr 01 - Module 6

# Announcements

## Assigned Reading for next Wednesday

Wednesday Apr 3: Chapter 11 of *The Mythical Man Month* - "Plan to Throw One Away"
Wednesday Apr 10: Chapter ?? of *The Mythical Man Month* - "Second System Effect"



## IDEA Surveys - Rare Extra Credit Opportunity

The regular three week long window for IDEA Student Rating of Instruction (SRI)
opens next week.  You should have already received a personalized email with a
link to complete your survey.

Your IDEA feedback is very important to me, and each semester I take many
useful suggestions and incorporate them into my future courses.  Much of what I
do I owe to your suggestions.  The more input I get from you the better I am
able to improve as an instructor.  My goal is to reach 80% participation.

To that end I am offering 25 points of sweet, sweet *extra credit* for your
response.  Your responses remain anonymous, and I will not even see them until
after finals week.  The extra credit is automatically applied to your grade by
Canvas within 48 hours of your taking the anonymous survey.



## Space Dynamics Lab / USU Technical Lecture
Please join us for the next SDL/USU Technical Lecture Series at 4:30 pm on
Tuesday, April 2 in ENGR 201. SDL Mechanical Designer Greg Hopkins will cover
design principles of mechanical components used in space.

Free pizza after the lecture. More information about this exciting event is
available on [our website](https://engineering.usu.edu/events/sdl-usu-lecture-series).
See you there!




## DC435 - NMAP 101 by @Santiago

One of the easiest ways an attacker can discover vulnerabilities on your
network is by doing what is called a port scan. Come learn how to do a port
scan and/or a little more.

Thursday, April 4th @ 7pm
Bridgerland Main Campus - 1301 North 600 West - Room 840



## Class Cancelled next Friday, Apr 12 for OpenWest conference


# Topics:

* Assignment #6 stand up meeting
* Generating color gradients with `Colour.Color`
* Abstraction and Encapsulation applied



--------------------------------------------------------------------------------
# Assignment #6 stand up meeting
        _
    .__(.)<  - Scrum!
     \___)

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?
    - Getting over my fear of the assignment
    - Installing the `colour` package didn't work

Remember to use the REPL to your advantage


If you haven't started on this assignment yet, what is one thing that you can
begin working on today?



--------------------------------------------------------------------------------
# Generating color gradients with `colour.Color`

Use Python's `colour` package to easily generate color gradients with an
arbitrary number of steps.

Let's first re-create the hard coded gradient from the starter code, then we'll
change it to have fewer/more steps.  Finally, we'll make some gradients which
go through more than two colors.


## At the Anaconda Prompt

Run this command:

    $ conda install colour

If that doesn't work, try this one:

    $ pip install colour


## In your program

`from colour import Color`


See the file `gradient.py` in this directory for an example




--------------------------------------------------------------------------------
# Abstraction and Encapsulation applied


See example code in [Sequences/sequences.py](Sequences/sequences.py)



As a review, two of the [4 fundamental OOP concepts](../Readings_and_resources.md#markdown-header-the-four-fundamental-concepts-of-oop)

1. **Abstraction**
2. **Encapsulation**


* How does this code example apply the principles of Abstraction and
  Encapsulation well?

    - Each class has a reasonably well-defined external interface of methods
    - Each method in the class has one responsibility, and does it well
    - Instances of each class can be used by the driver code interchangably;
      apart from initially creating an object, the program doesn't need to do
      anything special when a `Pell` object is used or when a `FizzBuzz` is
      used.


* What about this code can still be improved?
    
    - There is a lot of duplication in here
        - if one class's `.next()` method has a bug, will I really remember to
          check whether the rest of them share it?
    - One 215-line file is not too fun to read
