# Declarative Programming

Instead of describing to the computer *how* a computation is to occur, the
programmer describes instead *what* the result is to look like.  Starting from
this description and the input data the programming language system uses a
variety of problem-solving algorithms to arrive at the correct solution, or, in
failing, proving that no solution exists.

The resulting program may not run quickly, but may be easier to write and
reason about.  Depending upon how you formulate the problem, a declarative
program may not be that slow after all.

Examples: Prolog, SQL, Make, Haskell, Oz


## Declarative programming w/ list comprehensions

List comprehensions in Python were borrowed from the Haskell language:
https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-420003.11

They were introduced into Python by PEP 202
(https://www.python.org/dev/peps/pep-0202/) and became part of the language as
of Python v2.0.

A list comprehension is created by surrounding certain Python expressions in
between the square brackets '[' and ']', much as one would create a list
literal.  The expressions within the brackets result in a sequence of values, which forms the list.
