# Structured Programming

Related code is collected into blocks which may be skipped or repeated.

This paradigm intends to reign in the chaos that may prevail under the
Imperative style.  This is the paradigm which answers the advice "never use
GOTOs" or "Go To Statement Considered Harmful".

Structured code is divided into blocks which may be entered only from the top.
These blocks may be avoided under certain circumstances, or repeated.

This style is characterized by the ubiquitous if/then/else conditionals and
loops present in every language you've encountered.

Examples: C, Pascal, Java, Python
