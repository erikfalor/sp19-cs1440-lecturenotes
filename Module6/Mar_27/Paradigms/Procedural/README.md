# Procedural programming

Related code is collected into named functions which can only be entered from
the top.
  
The fundamental unit of code is a function.  A function cannot be entered
midway through as with a jump or goto instruction: you must begin at the top of
the function and proceed toward its end.  While a function may have only one
starting point, it may have multiple exit points or "return" statements.

Examples: C, FORTRAN, Pascal
