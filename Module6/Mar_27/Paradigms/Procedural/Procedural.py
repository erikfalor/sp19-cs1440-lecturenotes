def fizz(n):
    if n % 3 == 0:
        print('Fizz', end='')
        return True
    return False


def buzz(n):
    if n % 5 == 0:
        print('Buzz', end='')
        return True
    return False


def main():
    i = 1
    while i <= 100:
        fizzed = fizz(i)
        buzzed = buzz(i)
        if not (fizzed or buzzed):
            print(i, end='')
        print()
        i = i + 1


main()
