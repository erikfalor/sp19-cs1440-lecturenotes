# Functional Programming

Functional programming may be considered a subset of the declarative paradigm.

In this paradigm large programs are built of small, simple functions which may
be created and modified at runtime, and my be the input or output of other
functions.  Functional programming languages may be characterized by the notion
of functions being "first-class objects", meaning that a function may be stored
in a variable, just like numbers, strings, and booleans.

Functional programs emphasize *action* over *state*, meaning that collections
of global data are discouraged or imposible.  Functional programs perform their
computations with *expressions* rather than *statements*.  Expressions are
phrases of code which return a value, such as `2 * 4 + 1` or
`fips.isnumeric() and not fips.endswith("000")`.

Statements, by contrast, do not return a value.  For example, a for loop does
not result in a value that could be passed to a function.  You cannot assign
the result of an if/elif/else tree to a variable.

    # These don't work
    print( for i in range(100): i )

    msg = if x > 100:
              "X is a BIG dawg"
          elif x > 50:
              "X is a medium number"
          else:
              "X is just a teeny-weeny little guy"
    print(msg)


However, the equivalent expressions in a functional language are possible.

Examples: LISP, Scheme, Haskell, ML


## FizzBuzz in the Functional style w/ lambda expression

A "lambda expression" is a way to create a "function literal".  Just as you can
make a string literal "with quote marks",and a list literal 

  [ "with", "square", "brackets" ]

or a dictionary literal

  { 'using': "curly", 'braces': "like this" } 

you can make a new function value with the `lambda` keyword.  Like any other
literal value, you can either store this into a variable or use it in-place.

The example in this directory uses the function literal in-place without
storing it into a variable.  Such functions are called "anonymous" since they
are never given, nor need a name.  The notion of *not* assigning a name to a
function may seem strange to you, but it is a very, very common idiom in
functional languages 
