from Modules.Fizz import fizz
from Modules.Buzz import buzz


def main():
    i = 1
    while i <= 100:
        fizzed = fizz(i)
        buzzed = buzz(i)
        if not (fizzed or buzzed):
            print(i, end='')
        print()
        i += 1
