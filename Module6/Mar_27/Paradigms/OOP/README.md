# Object-Oriented Programming

Starting from primitive data types such as numbers, strings, booleans, etc.,
create new data types called 'classes' which represent collections of related
data representing some important aspect of the solution.  As the program runs
instances of classes are created and are called 'objects'.

Far from being inert collections of bits and bytes, objects are regarded as
being active participants in that they can act and react to events within the
system.  This is because the functions which are to operate upon the data
contained within the objects become part of the object itself, imbuing it with
active potential.

Put another way, the Object-Oriented approach is to classify data which exists
in the system, organize this data into discrete and distinct units which also
contain code expressing the operations which work upon this data.

Examples: Smalltalk, Java, C++, C#, Python
