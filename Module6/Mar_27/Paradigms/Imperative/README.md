# Imperative programming

This style is characterized through the step-by-step execution of instructions.

The fundamental unit of code is a single instruction.  The flow of the program
ordinarily proceeds from beginning to end, however some instructions can change
which instruction is to be executed next.

This style is characterized by GOTO statements or their equivalents.  Syntactic
loops and if/then/else are not present in this paradigm.


Examples: Assembly language, Machine language, IBM's FORTRAN and FORTRAN II
