# CS1440 - Wed Mar 27 - Module 6

# Announcements

## FSLC

Cal Coopmans of AggieAir

Wednesday, March 27th
7pm - ESLC Room 053




## Utah OpenSource Intercollegiate Cyber Sparring Event

Practice your CyberSec skills in a friendly environment.

Saturday, Mar 30 12pm through 6pm
ESLC 053

You'll need to install a piece of VPN software called
[ZeroTierOne](https://www.zerotier.com/download.shtml) to connect to the Utah
OpenSource VPN to play.

On ZeroTierOne's homepage you may see an insane command line which installs
their software.  Don't do it.  [Whoever puts instructions like this on their
website needs to be drug out into the street and
shot](https://www.idontplaydarts.com/2016/04/detecting-curl-pipe-bash-server-side/).
Just download the installer for your OS and install it like a normal person.


After ZeroTierOne is installed, launch the zerotier-one service

    $ zerotier-one -d

Next, join a ZeroTier network, and wait for your new IP address.  The
hexadecimal number is the ID of the UTOS cybersecurity sparring network:

    $ zerotier-cli join a0cbf4b62a48c5f9




## Computer Science Department Spring Social and Awards Dinner

April 16, 2019
5 – 7 pm
Haight Alumni Center, USU Campus

* Free Dinner - Taco Bar & Drinks from The Italian Place
* Prizes
* RSVP is required. RSVP to cora.price@usu.edu number of adults & kiddos, & dietary notes



# Topics:
* Resolving a merge conflict
* Which idiot wrote this stupid thing?
* Real-world git: Using 'git bisect' to track down a bug
* Reading discussion: The Pragmatic Programmer, Chapter 5
* What kinds of programming languages are there?



--------------------------------------------------------------------------------
# Resolving a merge conflict

Last time we worked in a repository with two branches that we wanted to merge
into `master`.  However, doing so resulted in a *merge conflict*.


#### Merge conflict
When two or more commits change the same portion of a file, git cannot
automatically proceed without loss of data and halts the merge.  A developer
must manually review the conflicting changes and bring about resolution.

Git will annotate the affected files with *merge markers*.  These boundaries
surround the code that it appears in your branch and in the branch that is
being merged in.

For example the first hunk between 
`<<<<<<<` and `=======` denotes the code in your currently-checked out commit
(that's what `HEAD` means, after all).  Code between `=======` and `>>>>>>>`
came from the `factorial` branch.

    <<<<<<< HEAD
    def hello(who, n):
        for i in range(fib(n)):
            print(f"Hello {who}")
    =======
    def hello(who, n):
        for i in range(factorial(n)):
            print(f"Hello {who}")
    >>>>>>> factorial


Do you see the conflict?  I have two different versions of the `hello()`
function.  One way to resolve this would be to create two functions:

    def helloFib(who, n):
        for i in range(fib(n)):
            print(f"Hello {who}")

    def helloFactorial(who, n):
        for i in range(factorial(n)):
            print(f"Hello {who}")


Then I'd have to visit every place in the program where `hello()` had been
used and change those lines of code to call one of the above functions.

Alternatively, I could stick with one version of `hello()` but change the
number of parameters it takes.  In this version I pass either the `fib` or
the `factorial` function as a parameter:

    def hello(who, fn, n):
        for i in range(fn(n)):
            print(f"Hello {who}")

    # Is it lunch time yet?
    hello('Fries!!', fib, 10)
    hello('Hamburgers!!', factorial, 10)


I'll still need to edit other lines of code by adding a new argument to each
call to the function `hello()`.  Whatever my choice, it will be much more
sophisticated than what git could come up with.  Once I have changed the code
and deleted the conflict markers, I run `git add` followed by `git commit` to
complete the merge.



--------------------------------------------------------------------------------
# Which idiot wrote this stupid thing?

This is question that will cross your mind from time to time.  You can find out
with this command:

* `git blame -- FILENAME`
Annotate each line of the current version of FILENAME with the date & committer
who last changed it.  I hope it wasn't you!  Keep in mind that your
psychopathic co-worker knows this command, too.

<cd ~/build/chicken-core.git/; git blame modules.scm>



--------------------------------------------------------------------------------
# Real-world git: Using 'git bisect' to track down a bug

Git's commit history of your code is an invaluable resource to help
understand the evolution of your project. However, it can become an
unmanageable resource due to its size.

Git bisect is a power tool which tames the unmanageable size of your
repository. Use it to perform a binary search on your git repo to identify the
commit which introduced a bug.

We can use `git bisect` in Vim's Github repository to find when a particular
bug was introduced.

[Vim no longer de-indents shell script code: Issue #2151](https://github.com/vim/vim/issues/2151#issuecomment-331970759)


## Let's track this bug down for ourselves

<cd /tmp/vim.git>

You can try this out for yourself on a Unix-like system with the GNU C compiler.

    $ git clone https://github.com/vim/vim.git
    $ cd vim
    $ ./configure
    $ make -j$(nproc)


Launch the freshly-built Vim program `src/vim`:

    $ src/vim -N -u NONE -i NONE -c 'source runtime/indent/sh.vim'


If I type this text into Vim, the editor will auto-indent this shell script source code:

    if this
    then
        that
    fi

In the latest version of Vim this bug is fixed, so what we see is the correct
behavior.


Let's go back in time to when the bug was occurring to see what the problem
looked like.  I'll write a shell function to make the process of re-building
and re-launching vim more convenient:

    $ function rebuild() { ./configure; make -j$(nproc) && src/vim -Nu NONE -i NONE -c'so runtime/indent/sh.vim' }

    $ git checkout v8.0.1127
	$ rebuild


As I write that sample shell script code we will see the problem:

	if this
        then
                that
                fi


The question is *when* did this bug occur?

From the bug report, we read that Gary noticed the problem once he built
8.0.1127, and that it wasn't there in version 8.0.0691.  Somewhere between those
two commits the bug crept into the source code.

Let's do some quick math:  1127 - 691 = 436 git commits to look at.  It won't
take too long to try each commit one-by-one until we find the one which
introduced the bug, right?

LOL, no.  Remember, great programmers are both lazy and impatient.  We'll find
a better way to do this.


## git bisect HOWTO

You need three things to use git bisect:

1. The name of a commit in which the code worked
2. The name of a commit where the bug is evident
3. A test to determine the presence of the bug


To begin a bisect, checkout one of the boundary commits and run this command:

	$ git bisect start


Tell git whether this commit is a good one or a bad one.  Because I just
demonstrated the error to you, we are on one of the **bad** commits.

	$ git bisect bad


We want to search backwards in time to find where the bug was first introduced.
So we next tell git which commit represents the **good** boundary.  According
to the bug report we know that as of commit `v8.0.0691` Vim worked correctly.
This is the last commit which we know does not possess this bug:

	$ git bisect good v8.0.0691


Git will then take me to the commit right between our **good** and **bad**
boundaries.  There we can do whatever it takes to determine the presence of the
bug.  In this case that means re-building Vim, running it and typing some text
to see whether Vim automatically indents our code.

When we observe the absence or presence of the bug, we tell git whether this
commit was *good* or *bad* by running either

	$ git bisect good

or

	$ git bisect bad


and re-running our test.  This process is repeated until git converges on the
commit which introduced the bug.  You can automate this process by writing a
script that distinguishes between working and broken code and telling git to
run that.

    $ git bisect run ./myTestScript.sh


Once we're all done, we run

    $ git bisect reset


to tell git to restore our code to the way it was when we began with `git
bisect start`.


## Why this is awesome

Obviously, it's awesome because we were able to cover 436 commits in only 8
steps.  `git bisect` is an example of a binary search algorithm.  When applied
to debugging this is also known as the "Wolf Fence" debugging technique.

This technique works best when you create many small, focused commits.  We can
ask the question "would it be easier to locate this bug if there were 1/10th as
many commits which were 10x larger?"

The key to remember is that the hard part isn't locating the bad commit.
The key is that when we arrive at the bad commit our job's only just begun.

The difference between 436 commits and 43 commits isn't that great when we
apply a binary search.  We can cover 436 commits in about 8 steps, and 43
commits in 5 steps.

If the commit which introduced the bug is small and focused on a single
feature, it won't take too much work to figure out what went wrong.  Now
imagine that this commit involves 10x as many changes as before.  How long
will it take you to evaluate 120 lines of code instead of 12 lines of code?



### tl;dr

Make many small commits instead of a few big ones.





----------------------------------------------------------------------------
# Reading discussion: The Pragmatic Programmer, Chapter 5

## Decoupling and the Law of Demeter

* How can you tell if a piece of code "knows too much"?

* What's wrong with code which "knows too much"?

* How easy is it to refactor such code?

* Cite a violation of this principle in Assingment 5



## Metaprogramming

* Dynamic configuration enables a program to respond to changes which occur
  long after the program is written.

* What metadata does the starter code contain?

* How does our fractal program "reconfigure" itself in response to the user's input?



## It's Just a View

* Decouple a program into separate modules each with their own
  responsibilities.  Then you must decide how these modules will communicate
  with each other.

* How did you incorporate this principle in Assingment 5?



----------------------------------------------------------------------------
# What kinds of programming languages are there?


## Mud card Activity

As we discuss this topic, jot down any questions or impressions that you have.



* [Programming Paradigms on WP](https://en.wikipedia.org/wiki/Programming_paradigm)

There are many programming paradigms, among them are imperative, procedural,
structured, modular, functional, declarative, and object-oriented.

Each paradigm approaches the structure of code differently and encourages a
different approach to problem-solving.  Some programming languages were created
to encourage a single paradigm, while others have, over time, come to be
strongly associated with a particular paradigm.

However, these programming paradigms need not be mutually-exclusive.  Many
languages draw inspiration from and support many paradigms at once.  Python is
one such multi-paradigm language.  One of its design goals is to combine the
best features of many paradigms in a congruent way, giving the programmer the
freedom to write code however they see fit.


## The Fizz Buzz Program

FizzBuzz is a popular "job interview" program designed to filter out the 99% of
so-called "programmers" who can't.

* [FizzBuzz Test on C2 WIki](http://wiki.c2.com/?FizzBuzzTest)

* [FizzBuzz described](https://blog.codinghorror.com/why-cant-programmers-program/)

> Write a program that prints the numbers from 1 to 100.  But for multiples of
> three print "Fizz" instead of the number and for the multiples of five print
> "Buzz".  For numbers which are multiples of both three and five print
> "FizzBuzz".


The following examples show how the FizzBuzz program may be written in the
Python language, but in different paradigms.

I will also list the names of well-known languages which are tightly associated
with each paradigm.  You are encouraged to look up their entries on [Rosetta
Code](https://rosettacode.org/wiki/FizzBuzz) to see how many different ways
there are to tell a computer how to do the same thing.



#### Today I'll broadly separte languages along one dimension: *Imperative vs. Declarative*

There are lots of ways to categorize languages, and not all of them are
cut-and-dried.  Don't get your feelings hurt if you feel that I have
mis-characterized your favorite language :)

The main distinction that I draw between these two paradigms can be stated in terms
of their primary concerns:

* Imperative: concerned with how the computation *should proceed*
* Declarative: concerned with how the result *should appear*


--------------------------------------------------------------------------------
## The Imperative Paradigms

These paradigms are characterized by step-by-step, from-top-to-bottom execution
of instructions.  While most modern programming languages bear vestiges of this
style of programming, they do not hold as strictly to it as did the earliest
programming languages.



### Imperative

In its purest form, the fundamental unit of imperative code is the instruction.
The flow of the program ordinarily proceeds one instruction at a time from
top-to-bottom.  Computation occurs by executing instructions with
"side-effects" which change the state of CPU registers and memory locations.
However, some instructions (e.g. `GOTO` and `JUMP`) can change which
instruction will be executed next.  Syntactic loops and if/then/else are not
present in this paradigm.

Neither are named functions present; blocks of code called "subroutines" may be
`JUMP`ed into or out of, but there is no notion of passing arguments or
returning values.  Further, code may freely jump into the middle of a
subroutine without first executing any initialization code that may have been
present at the top of the subroutine.

Examples: Machine code, Assembly language, IBM's FORTRAN and FORTRAN II



### Procedural

Related code is collected into named functions which can only be entered from
the top.

The fundamental unit of code is a function.  A function cannot be entered
midway through as with a jump or goto instruction: you must begin at the top of
the function and proceed toward its end.  While a function may have only one
starting point, it may have multiple exit points or "return" statements.

Examples: C, FORTRAN, Pascal



### Structured

Related code is collected into blocks which may be skipped or repeated.

This paradigm intends to reign in the chaos that may prevail under the
Imperative style.  This is the paradigm which answers the advice "never use
GOTOs" or "Go To Statement Considered Harmful".

Structured code is divided into blocks which may be entered only from the top.
These blocks may be avoided under certain circumstances, or repeated.

You will recognize these as the ubiquitous if/then/else conditionals
and loops present in every language you've encountered.

Examples: ALGOL, PL/I, Pascal, C/C++/C#, Java, Python



### Modular

Related pieces of code are collected into larger entities called 'Modules'.
Modules may or may not be useful by themselves, but most often a module is
*not* a complete, standalone program.

Modules may be selectively included into a useful program.  Modules encourage
code reuse by enabling libraries of commonly desired general-purpose code to be
readily available.

Many languages incorporate this concept under various names, such as modules,
packages, imports, gems, eggs, shared objects, header files, etc.

Examples: Modula, Java, Python, C/C++/C#



### Object-Oriented

Starting from primitive data types such as numbers, strings, booleans, etc., an
object-oriented programmer creates new data types called 'classes'.

Far from being inert collections of bits and bytes, objects are regarded as
being active participants in that they can act and react to events within the
system.  This is because the functions which are to operate upon the data
contained within the objects become part of the object itself, imbuing it with
active potential.  Conceptually, the "same" operation might do different
things, depending on the classes of the data it is being applied to.

Put another way, the Object-Oriented approach is to classify data which exists
in the system, organize this data into discrete and distinct units which also
contain code expressing the operations which work upon this data.

Examples: Smalltalk, Java, C++, C#, Python


#### Best-Practices Object-Oriented FizzBuzz in Java

* https://github.com/EnterpriseQualityCoding/FizzBuzzEnterpriseEdition
LOL calm down, this is a joke.  It is an example of what happens when
"best-practices" are uncritically followed to their logical conclusion.



--------------------------------------------------------------------------------
## Declarative Paradigms

Instead of describing to the computer *how* a computation is to occur, the
programmer describes instead *what* the result is to look like.  Starting from
this description and the input data the programming language system uses a
variety of problem-solving algorithms to arrive at the correct solution, or, in
failing, proving that no solution exists.

The resulting program may not run quickly, but may be easier to write and
reason about.  Depending upon how you formulate the problem, a declarative
program may not be that slow after all.

Examples: Prolog, SQL, Make, Haskell, Oz


### Functional

Functional programming may be considered a subset of the declarative paradigm.

In this paradigm large programs are built of small, simple functions which may
be created and modified at runtime, and my be the input or output of other
functions.  Functional programming languages may be characterized by the notion
of functions being "first-class objects", meaning that a function may be stored
in a variable, just like numbers, strings, and booleans.

Functional programs emphasize *action* over *state*, meaning that collections
of global data are discouraged or impossible.  Purely functional languages do
not have the notion of assignment, as all side-effecting can be defined in
terms of functions that encapsulate the changed data.

Functional programs perform their computations with *expressions* rather than
*statements*.  Expressions are phrases of code which return a value, such as
`2 * 4 + 1` or `fips.isnumeric() and not fips.endswith("000")`.

Statements, by contrast, do not return a value.  For example, a for loop does
not result in a value that could be passed to a function.  You cannot assign
the result of an if/elif/else tree to a variable.

    # if/elif/else is a *statement*, not an expression; one cannot
    # assign the result of an expression to a variable:
    msg = if x > 100:
              "X is a BIG dawg"
          elif x > 50:
              "X is a medium number"
          else:
              "X is just a teeny-weeny little guy"
    print(msg)

    # Print a list of the 1st 100 square numbers using a for loop
    # *within* a call to print(); this doesn't work in Python!
    print( for i in range(100): i*i )


However, the equivalent expressions in a functional language are not only
possible, but downright commonplace.

This is the functional Python way to "print" the result of a `for` loop.  It
uses Python's `map()` function.

    # `map()` is an expression (meaning that it it returns a result),
    # which repeats a computation:
    print(* map(lambda i: i*i, range(100)) )


Examples: LISP, Scheme, Haskell, ML



# TL;DR

There are lots of ways to categorize programming languages.  You may group them
by feature, by syntax, by purpose, by memory-management strategy, by
multiprocessing strategy, etc.  Be aware that the mainstream programming
languages you'll likely encounter only represent a small, boring slice of the kingdom.
https://en.wikipedia.org/wiki/List_of_programming_languages_by_type
https://en.wikipedia.org/wiki/List_of_programming_languages_by_type#Curly-bracket_languages


Just remember that Scheme is the best language of them all!

    (use srfi-1)

    (for-each
      (lambda (n)
        (print
          (cond
            ((zero? (modulo n 15)) "FizzBuzz")
            ((zero? (modulo n 5)) "Buzz")
            ((zero? (modulo n 3)) "Fizz")
            (else n))))
      (iota 101))
