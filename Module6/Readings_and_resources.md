# Table of Contents

* Assigned Reading
* What is Object-Oriented Programming?
* Four Principles of Object-Oriented Programming
* How do you approach programming problems?
* Code reuse
* Design Patterns



--------------------------------------------------------------------------------
# Assigned Reading

* Wednesday Mar 27: Chapter 5 of *The Pragmatic Programmer* - "Bend, or Break"
* Wednesday Apr 3: Chapter 11 of *The Mythical Man Month* - "Plan to throw one away"



----------------------------------------------------------------------------
# What is Object-Oriented Programming?

In this module we will take a whirlwind tour through some of the most
influential programming paradigms.  Each programming paradigm has its benefits
and limitations.  Modern, mainstream programming languages don't slavishly
adhere to a pure interpretation of only one paradigm, but instead incorporate
features from many paradigms.  With experience you will come to understand when
and how to apply each paradigm to your programming problems.

Object-Oriented Programming (OOP) is one paradigm among many.  As can be said
of the others, it is not the best way to program, and it's far from the only
way to write a program.  OOP happens to be the dominant paradigm today,
enjoying broad support in all mainstream programming languages.  This means
that it is widely understood by a majority of programmers in industry today.
One of its best advantages is that OOP code will be approachable to as many
programmers as possible.


## What does OOP bring to the table?

When you studied UML with the Bingo! assignment you thought about the
relationships among different objects in that system.  For these relationships
to be meaningful, there must exist distinct classes, each with their own role
and purpose.

Objects are only useful in so far as there are boundaries between them.
Imagine if the Bingo! program was an amorphous mish-mash of code lacking any
organization.

* Where would you begin to make sense of such a code base?

* What could happen if you tried to change such a program?



When you consider a program as a collaboration between individual units of code
with distinct roles, you will find that the cognitive burden of understanding
what is going on is relieved.

* Why (or why not) do you think that this is the case?



The poet Robert Frost wrote ["Good fences make good neighbours"](https://www.poetryfoundation.org/poems/44266/mending-wall).
Maybe this idea has something to do with it.




## Classification

Object-Oriented languages are distinguished by possessing a concept called
_class_ which enables a programmer to express the concept of "units of data
imbued with behavior" to the computer.  OOP is chiefly concerned with
organizing code based upon how it relates and interacts with data.  Good
organization is achieved when data is isolated into distinct classes and code
which operates on that data is located with the data it operates upon.

![Figure 1](broken-watch-in-pieces.jpg)

Within philosophy is a school of thought called Ontology which studies the
nature of being, the categories of being and the relationships between these
categories.  The programming term _class_ comes from the ontological idea of
_Classification_.  Classification is the process of grouping objects together
into sets (read: classes) based on common features.

System designers study a system to identify _data_ (nouns) along with
_functions_ (verbs) which will operate on that data.  Nouns and their related
verbs are grouped together into a class with the aim of identifying and
removing accidental details and reducing the number of essential details
required to produce a working system.

The fruit of this labor is that the amount of effort which must be expended to
understand the entire system is minimized.  A system might be composed of many
classes, but not all of the classes must be understood all at once.
Programmers working in the system may only need a deep understanding of one
portion of the system, and a cursory understanding of the rest of the system
will suffice.

Classification exemplifies the problem-solving strategies of "dividing the
problem" and "reducing the problem".

* Consider how the Bingo! starter code divided the problem of creating a deck
  of cards into separate classes.  What classes were a part of that solution,
  and what were their responsibilities?

* What data is used by the Fractal program?

* What functions make up the Fractal program?

* What classes can you imagine creating by combining them?  Write a small list
  of classes and briefly describe their responsibilities.



![Bingo! UML Class Diagram](Bingo-class-diagram.png)



----------------------------------------------------------------------------
# Four Important Object-Oriented Programming Principles

As you classify data and behaviors in a system there are four fundamental
concepts that can be applied to classes which help you to create a system
which is easy to reason about.


The four fundamental concepts of OOP
====================================
1. Abstraction
2. Encapsulation
3. Inheritance
4. Polymorphism 




## 1. Abstraction

When you hear this word you may be tempted to think of something that is
abstruse or difficult to grasp.  In Computer Science we mean quite the
opposite.  The idea is to strip away accidental details from a system, reducing
it to the essential qualities that really matter.

By removing extraneous details we reduce the cognitive "surface area" of an
important concept, making it easier to understand in the context of the larger
system which we are trying to assemble.  There is, of course, a point at which
we are unable to further reduce the complexity of a concept.  Some things are
just complicated.  Our goal is to reduce a concept down to that appropriate
level of simplicity and no further.

![Figure 2](watch-face.jpg)

* What complex ideas are simplified with abstraction in your Bingo program?

* What complex ideas can you identify within the Fractal program?

* With an eye on the goal of refactoring, how might you use the principle of
  abstraction to simplify a code base?

Abstraction offers a way to "restate the problem" as well as to "reduce the
problem".




## 2. Encapsulation

Encapsulation means to hide complexity so other programmers need not be
concerned about details irrelevant to their immediate problem.

![Figure 3](watch-inside.jpg)

When faced with an object of irreducible complexity, you can further reduce its
cognitive surface area by separating _internal_ complexity from _external_
complexity.  This idea is best understood by putting yourself in the shoes of
another programmer who will use your class to solve their problems.  How much
do they *really* need to understand to be able to use it?  The pieces of
information they need to know about are _external_ details.  Every detail which
is not part of that answer should be kept on the _inside_ of the class.

Examples of external details
----------------------------

* Name of the Class
* How to create an object from that class
* What methods the class defines
* The parameters those methods take
* What types of values those methods return
* Which data members may be accessed, and their types


Examples of internal details
----------------------------

* The algorithm used to create a new instance 
* Which other objects this object makes upon creation
* How many iterations of a for loop are used to return a result from a method
* The fact that it is a for loop used within that method
* Types and names of temporary variables used within methods
* Names and signatures of methods used to produce results for external methods


Help your fellow programmers by protecting them from as many extraneous details
as possible.  Just because a concept is important to your class doesn't mean
that everybody working on the codebase needs to know about it.

Encapsulation reduces potential for accidental complexity caused by adding bad
dependencies between classes which need not be related.  Encapsulation also
frees you to change *how* your class works without the risk of causing another
programmer's code to break.  So long as you keep the *external* details the
same (such as the name of the class and signatures of external methods), you
can completely rewrite *how* the class works and other programmers will never
notice the difference.

Through encapsulation you offer the programmers who will use your code a way to
"start with what you know".


* How do the classes of the Bingo! program exhibit encapsulation?

* How have you been putting the principle of Encapsulation to work as you've
  refactored the fractal program?



## 3. Inheritance

The activity of classification results in a collection of interrelated classes.
By further studying these classes you may recognize relationships between
classes.  You might find that one class is essentially the same as another
class, only varying in a few particulars.  Such duplication feels wrong to a
programmer who recognizes the trouble caused by cut & paste programming and you
will want to rewrite the code in a way that says "this class is the same as
that other class _except_ for this single detail".

![Related Watches](related-watches.jpg)


Inheritance lets us avoid code duplication by reusing common behaviors and
properties among related classes, and adding unique aspects to those classes
which differ.  In an OOP language you may declare that a class is a "kind of"
another class, and the language will be able to make use of that knowledge in a
way that enables us to avoid duplicated code.

We do this by arranging related classes in a *hierarchy* organized from general
to specific.  We place all of the duplicated code into the class at the top of
the hierarchy.  This class embodies the most basic, general, common behaviors
and properties shared among all of the related classes.  You may say that it is
the least common denominator.

A class which represents a more specific version will *inherit* the common
properties from the general class.  Instead of copy-pasting common code into a
new file, the programming language copies the functionality into the
more-specific class for us.  Our files remain short, to the point, and are
without duplication.

A programming language supporting the principle of inheritance enables us to
write code which means "a self-winding wristwatch is a specific kind of
wristwatch".  When the programming language understands this relationship it
will give us the ability to write a SelfWindingWristWatch class which will
broadly behave the same as an ordinary WristWatch, but with the added feature
that it is self-winding.


We've already written and used classes which use another class in a way that
suggests ownership.

* What classes of the Bingo! program exhibited the "has a" relationship?

* Were there any classes of the Bingo! program which participated in a "is a"
  relationship?

* Which of the following statements best describes the Deck object from Bingo!?

    "A Deck object _is_ an array of Card objects"

    "A Deck object _has_ an array of Card objects"

* Do either of these statements reflect the idea of "inheritance"?


Common points of confusion around the idea of inheritance
---------------------------------------------------------

* Ownership != inheritance.  One object owning another is called *aggregation*.
  Example: the `UserInterface` class from Bingo! created and used `MenuOption`
  objects.  This relationship does not make "UserInterface is a `MenuOption`" a
  valid statement.

* "Being made out of" != inheritance.  One object being built of other objects
  is called "composition".  Example: a `Deck` is made of `Cards`.  In English
  we might say something like "that is a deck of cards", but it does not mean
  that the deck is a kind of card.



## 4. Polymorphism

Polymorphism is a Greek way of saying "many-shaped".

When many OOP classes each implement methods with the same names, to an outside
observer they all appear to be capable of the same operations.  *How* the
methods work might be different.  By following the OOP principle of
encapsulation we won't be too concerned about that at the moment.  What is
important right now is the fact that, by considering only the methods each
class implements, they all appear to basically do the same thing.

In other words, to an outside observer, each of these classes appear to be
*usable* in the same way.  If I write a function which can use a `FizzBuzz`
sequence, I could replace the instance of `FizzBuzz` with a `FogBag` and not
need to change any other code.


### Duck Typing

> If it walks like a duck and it quacks like a duck, then it must be a duck 

When one object happens to provide the same methods as another object, it may
be used in place of the latter.  An example of this in your assignment are your
concrete Fractal objects which descend from the Abstract `Fractal` class.  They
are only interchangable insofar as they all have a `count()` method which
accepts a Complex number as a parameter and return an integer.  The Python
language does not enforce that all of the `Fractal`-derived classes have a
method named `count` with compatible parameters and return values.

Polymorphism in strongly-typed languages such as C++, Java and C# is commonly
achieved through the mechanism of *inheritance*.  In these languages I can
instruct the compiler that a method with a particular name is required, and
that it must have a particular type signature.  Failure to supply this method
in the sub class is an error which is noticed at compile-time, and it is
impossible for such an error to make it into a production system.

In a dynamically-typed language such as Python, *duck typing* presents an
alternative.  *Duck typing* refers to an object which works as a drop-in
replacement for another object by virtue of the fact that both objects just so
happen to each have methods of the same name, with compatible parameter lists,
and identical return types.  Hence,

> If it walks like a duck and it quacks like a duck, then it must be a duck 

With Duck Typing the programming language cannot make any guarantees that a
particular line of code won't cause an error due to a missing or mismatched
method.  What is a compile-time error in C++/Java/C# becomes a run-time error
in a dynamically-typed language such as Python, JavaScript, Ruby, Perl, etc.

This is why I go out of my way to add a method in the "abstract" parent class
which intentionally crashes the program.  It is an attempt to raise the
visibility of the situation where a careless programmer forgets to supply
important functionality.


--------------------------------------------------------------------------------
# How do you approach programming problems?

It is my hope that through the course of this semester you have begun to build better habits that lead you to better solutions, faster. We've learned and discussed general problem-solving techniques, and have had ample opportunity to put them to work for us.

We've also learned that, although there are not likely to be any further advancements in the craft of software engineering that will make programming drastically easier than it already is, there are things that we can take advantage of which will make our work marginally simpler. It is through the steady accumulation of these small things that our work of creating large, complicated systems has become better than it's ever been.

## Sharpen the Saw

> Suppose you were to come upon someone in the woods working feverishly to saw
> down a tree. "What are you doing?" you ask.
> 
> "Can't you see?" comes the impatient reply. "I'm sawing down this tree."
> 
> "You look exhausted!" you exclaim. "How long have you been at it?"
> 
> "Over five hours," he returns, "and I'm beat! This is hard work."
> 
> "Well, why don't you take a break for a few minutes and sharpen the saw?" you
> inquire. "I'm sure it would go a lot faster."
> 
> "I don't have time to sharpen the saw," the man says emphatically. "I'm too
> busy sawing!"
> 
> Habit 7 is taking time to Sharpen the Saw. It surrounds the other habits on
> the Seven Habits paradigm because it is the habit that makes all the others
> possible.
> 
> -- Stephen R. Covey
> The 7 Habits of Highly-Effective People

It is of critical importance that we take the time necessary to stay abreast of
the latest advancements in our field so that we don't miss out on the very
thing that will prevent us from wasting five hours sawing a tree with a dull
blade.




--------------------------------------------------------------------------------
# Code reuse

Programmers use repetition at many levels to solve their problems. Computers are good at repetition. From loops within a program, to automated systems which repeatedly run a program, repetition is a fundamental part of what we do.

Programmers also like to repeat code. A proven solution, once written, is better reused than re-written.

## What kinds of reuse are there?

#### Cut & paste a block of code

A few lines or a paragraph of code

##### _pros_

-   Quick & easy to reuse

_cons_

-   If you didn't write it (perhaps even if you did) you may not  _really_  understand it
-   It can be difficult to adapt to the specific problem at hand (Variables to rename, etc.).

#### Abstract Data Types (ADT)

A data type defined by the  _behaviors_  it enables, or which methods it provides

##### _pros_

-   Once it's implemented, you can keep re-using it
-   knowing that you have the right tool for the job

##### _cons_

-   No Silver Bullet - beware of being a fanboy
-   Not understanding what they are or what their weaknesess/strengths are -> using them inappropriately

#### Libraries

A vast collection of reusable code. Code that is organized so that related things go together, designed to be reusable.

##### _pros_

-   Quick and Easy to use
-   Readily available, especially as part of modern programming languages

##### _cons_

-   May be difficult to integrate into your specific problem
-   Lose some flexibility in your solution

#### Algorithm

Solves a specific problem. Is not necessarily code: an algorithm an idea

##### _pros_

-   Not specific to a single language

##### _cons_

-   Not specific to a single language - you have to 'encode' it into a language to use it

#### Design Patterns

Solves a general problem. Is not necessarily code: a design pattern is an idea

_pros_

-   Like UML, understanding it makes it easier to communicate to other programmers
-   Enables "design by analogy", many problems are actually the same thing when you think about it

##### _cons_

-   Is not a ready-made solution, requires effort on the programmer's part
-   Must use judgement as to the right time & place to apply a pattern





--------------------------------------------------------------------------------
# Design Patterns

A design pattern is a general repeatable solution to a commonly occurring
problem in software design. A design pattern isn't a finished design that can
be transformed directly into code. It is a description or template for how to
solve a problem that can be used in many different situations.

[Design Patterns on SourceMaking](https://sourcemaking.com/design_patterns)



## The Abstract Factory Pattern

You need to create many different (but related) objects, but don't want to have
a big if/else if/else decision tree at every location where this creation needs
to take place.

The idea of this pattern is to separate the point of creation from the choice
of what to create.  Analagous to how we have all of our factories and
manufacturing businesses out on the west end of town, and keep the pretty,
consumer-friendly stores on main street.

The if/else if/else decision tree is hidden away in a Factory object on the
"bad side" of town.  We leave the nice store-front in the main-line of code so
that we can place an order as needed.

-   [Abstract Factory on Sourcemaking.com](https://sourcemaking.com/design_patterns/abstract_factory)
-   [Abstract Factory Pattern on Wikipedia](https://en.wikipedia.org/wiki/Abstract_factory_pattern)



## The Strategy Pattern

Allows your system (or a user) to select an algorithm or behavior at runtime.

Imagine a system where there are many ways to solve a problem, and each
approach has its own drawbacks; sorting algorithms are a good example.  The
user may know best which type of sort will be the fastest based upon the task
at hand.  The Strategy Pattern lets you write code which *uses* the algorithm
in a simple, straightforward way while abstracting the if/else if/else tree of
choices away into a separate location.

You may consider the Fractal Renderer's choice of which `count()` method to use
based upon the user's chosen fractal configuration file to be an example of the
Strategy Pattern.

-   [Strategy Pattern on SourceMaking.com](https://sourcemaking.com/design_patterns/strategy)
-   [Strategy Pattern on Wikipedia](https://en.wikipedia.org/wiki/Strategy_pattern)



## The Singleton Pattern

When you need to ensure that one and only one instance of a class exist in your
system.  Perhaps it is an expensive object and you wish to limit resource use.
Or many other objects depend upon it for coordination, and you wish to restrict
them to a single point of reference.

-   [Singleton Pattern on SourceMaking.com](https://sourcemaking.com/design_patterns/singleton)
-   [Singleton Pattern on Wikipedia](https://en.wikipedia.org/wiki/Singleton_pattern)

