# CS1440 - Fri Mar 29 - Module 6

# Announcements

## Assigned Reading for next Wednesday

Wednesday Apr 3: Chapter 11 of *The Mythical Man Month* - "Plan to throw one away"


# Topics:
* Mud Card Review
* Assignment 5 Retrospective - Anchors and Engine
* Assignment 6 Futurespective - Hopes and Concerns
* The four principles of Object-Oriented Programming



--------------------------------------------------------------------------------
# Mud Card Review

## Imperative vs. Declarative languages

> Are most of the popular languages imperative, declarative, or is it an even split?

Imperative is by far the most popular/common paradigm.  I think this is because
of a mixture of cultural inertia (imperative languages caught on first, and
have been passed down) as well as the fact that they are more approachable.  A
trained mathematician might find a declarative language to be more natural, but
for us muggles, it's easier to break a problem down into a sequence of steps.


> When would you use a declarative language as opposed to imperative?

It depends upon the nature of the problem.  If I'm writing an interactive
program (like a game or a menu system) an imperative language feels more
natural to me because its top-to-bottom flow matches well to the
cause-and-effect nature of the problem.

If I'm trying to solve a math problem or query a database, in a declarative
language I can define what the answer looks like and let the computer figure it
out.  SQL is a declarative language in which I describe what I want to come out
of the database without needing to worry about *how* it should happen.


> Why not visual programming languages?

Basically, they are poor tools for conquering complexity because they lead to
overly complex representations of a program.

Fred Brooks addresses this in his essay "No Silver Bullet".  Refer back to the
lecture notes from Feb 13 where we discussed this very question, and re-read
through that essay with this question in mind.


> What is a "goto"?

The fact that one of you asked this makes me think that Edsger Dijkstra is
smiling down upon you from heaven.

Edsger Dijkstra is a computer scientist who you should definitely get to know.
He was a prolific writer and very influential on our field.  He is the one who
kickstarted the Structured programming paradigm.  You should read the paper
that started it all. 

* [Letters to the editor: go to statement considered harmful](https://dl.acm.org/citation.cfm?id=362947)

Use the cheatcode mentioned in Module2/Readings_and_Resources.md to access this
ACM article for free:

* [Free ACM Article Access thru USU library](http://dist.lib.usu.edu/login?url=http://portal.acm.org/dl.cfm?coll=portal)

Edsger's reflections upon the matter:

* [What led to "Notes on Structured Programming"](https://www.cs.utexas.edu/users/EWD/transcriptions/EWD13xx/EWD1308.html)



## Efficiency

You guys are really obsessed with efficiency.  I find this to be a good thing;
I despaired that programmers of your generation wouldn't care about this
because modern languages make coding so easy.

Always keep in mind Donald Knuth's advice about efficiency:

> In non-I/O-bound programs, a few percent of the source code typically
> accounts for over half the run time.

> Premature optimization is the root of all evil.

* [Premature Optimization on C2 Wiki](http://wiki.c2.com/?PrematureOptimization)


> Is it worth it to sacrifice efficiency for readability?

99.9% of the time the answer is "yes".  Humans spend *far* more time reading
code than a computer spends executing it.


> Why do people still use inefficient languages?

Inefficient how?

* Hard to write/read: These languages are (usually) happily discarded when
  something objectively better comes along.  Sometimes, though, programmers
  have a tough time giving up hard-won knowledge.

* Slow for the CPU to execute: Hardware is cheap and is always becoming cheaper
  and more capable.  If I wait a few months my slow program won't seem so slow
  anymore.


> Why do people still use outdated languages?
> Why are they taught to the new generations?

They don't.  When was the last time you heard of APL, PL/I, or COBOL?
Or do you imply that C is outdated?  HERETIC!!!



## On the inevitablility of progress

As we looked through the various programming paradigms a couple of you picked
up on trend:

> The evolution of these paradigms seems to follow a pattern where it stops
> treating stuff in computer terms and uses human terms.

> It appears that programming languages are increasing the level of abstraction
> from raw machine code.

This is absolutely the case.  One field of study within Computer Science is
directed at languages.  Here our field actually has a lot of cross-over with
the field of linguistics.

There are two trends which work together to allow us to enjoy more convenient
ways to instruct computers to do our bidding.  Our understanding of languages
improves with more research, experimentation and experience, leading us toward
more humane interfaces.  At the same time, advancements in computing hardware
enable us to implement increasingly sophisticated software systems which demand
more processing overhead.

The result of these trends is that we get to live in a world where we can
express human ideas to machines in ways that favor our human sense of
intuitiion and familiarity.



--------------------------------------------------------------------------------
# Assignment 5 Retrospective - Anchors and Engine

Identify things that make you move faster and things that slow you down

- **Engine** What was the fuel for your engine?  What pushed you forward?

- **Anchor** What held you back or slowed you down?



## Assignment 5 Effort Survey

This optional survey will help me develop better homework assignments.
Your participation is not required and all responses are anonymous.

* [Section 001](https://usu.instructure.com/courses/531135/quizzes/715553)
* [Section 002](https://usu.instructure.com/courses/527950/quizzes/715556)



--------------------------------------------------------------------------------
# Assignment 6 Futurespective - Hopes and Concerns

* Identify requirements you must fulfill.  What outputs are you responsible for?

* What parts do you feel confident in?

* What parts are vague or confusing to you?

* Where will you start?



--------------------------------------------------------------------------------
# What is Object-Oriented Programming?

[OOP in a nutshell](../Readings_and_resources.md#markdown-header-what-is-object-oriented-programming)



--------------------------------------------------------------------------------
# The four principles of Object-Oriented Programming

[4 fundamental OOP concepts](../Readings_and_resources.md#markdown-header-the-four-fundamental-concepts-of-oop)

1. Abstraction
2. Encapsulation
3. Inheritance
4. Polymorphism
