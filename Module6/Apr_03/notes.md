# CS1440 - Wed Apr 03 - Module 6

# Announcements

## FSLC
Wednesday, April 3rd
7pm - ESLC Room 053

Building robots isn't easy, but thanks to open source software like ROS (Robot
Operating System), it's a bit less hard than it could be.  Come learn how FOSS +
Linux can help you build your first robot.



## DC435 - NMAP 101 by @Santiago
Thursday, April 4th @ 7pm
Bridgerland Main Campus - 1301 North 600 West - Room 840

One of the easiest ways an attacker can discover vulnerabilities on your
network is by doing what is called a port scan. Come learn how to do a port
scan and/or a little more.



## Assigned Reading for next Wednesday

Wednesday Apr 10: Chapter 5 of *The Mythical Man Month* - "Second System Effect"


## Class Cancelled next Friday, Apr 12 for OpenWest conference




# Topics:

* Assignment #6 stand up meeting
* Reading discussion: Chapter 11 of *The Mythical Man Month* - "Plan to Throw One Away"
* Use inheritance to reduce code duplication
* Inheritance and Abstract Classes


----------------------------------------------------------------------------
# Assignment #6 stand up meeting
        _
    .__(.)<  - Scrum!
     \___)

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?


----------------------------------------------------------------------------
# Reading discussion: Chapter 11 of *The Mythical Man Month* - "Plan to Throw One Away"

## Mud card questions

* Cite an experience you've had which exemplifies the "pilot plant" analogy.
* Brooks lists some concrete steps developers can take to plan for change.
  List the ones which stood out to you.
* Brooks claims that a program doesn't stop changing when it is delivered to a
  customer.  Why not?
* Do you people's expectations of hardware differ from their expectations of software?
* Brooks cites an anecdote from Betty Campbell of MIT's Laboratory for Nuclear
  Science where the number of bug reports in a system trended down *and then
  rose again*.
    - Do you think this is a widespread phenomenon?
    - Do you think this still happens today?


**Plan for changes**: *if you don't prepare for changes now you are only causing pain later on*


--------------------------------------------------------------------------------
# Use inheritance to reduce code duplication

The activity of classification results in a collection of interrelated classes.
By further studying these classes you may recognize relationships between
classes.  You might find that one class is essentially the same as another
class, only varying in a few particulars.

Duplication will feel *wrong* to a seasoned programmer who recognizes the
trouble caused by cut & paste programming.  You would like to remove the
redundancy in a way that says "this class is the same as that other class
_except_ for this single detail".

A programming language supports _inheritance_ if it provides a way for the
programmer to express the "is a" relationship between *generic* and
*specialized* classes.


## Denoting Inheritance in UML

The Inheritance relationship is expressed in UML by a white (open) arrow
pointing _from_ the specialized object _to_ the generic object.

![Shapes_Inheritance](Shapes_Inheritance.png)

From this diagram we learn the following facts:

* "A `Rectangle` is a type of `Shape`"
* "A `Square` is a type of `Rectangle`"
* "A `Circle` is a type of `Shape`"

The name of the `Shape` class is written in an _italic_ font.  This informs the
reader that no objects of type `Shape` are to be instantiated.  Rather, the
`Shape` class exists to lend its structure and common operations to its
descendants.  Furthermore, the operations `calculateArea()` and
`calculatePerimeter()` are written in _italics_.  This indicates that these
functions must be overridden by the child class.



What does inheritance bring to the table?
-----------------------------------------
The notion "This object is like that other object, but contains these
differences", when understood by your programming language, frees you from
writing duplicate code.  Inheritance lets us avoid code duplication by reusing
common behaviors and properties among related classes, and adding unique
aspects to those classes which differ.

We do this by arranging related classes in a *hierarchy* organized from general
to specific.  We place all of the duplicated code into the class at the top of
the hierarchy.  This class embodies the most basic, general, common behaviors
and properties shared among all of the related classes.  You may say that it is
the least common denominator.

A class which represents a more specific version will *inherit* the common
properties from the general class.  Instead of copy-pasting common code into a
new file, the programming language copies the functionality into the
more-specific class for us.  Our files remain short, to the point, and are
without duplication.




----------------------------------------------------------------------------
# Inheritance and Abstract Classes

An abstract class is a class that cannot be instantiated.  In the Inheritance
among shapes UML diagram above the "Shape" class is abstract, with its name is
written in an _italic_ font.

The opposite of an abstract class is known as a *concrete* class; these are the
ordinary sorts of classes you've been using all along.

An abstract class may provide no implementation, or an incomplete
implementation.

You might well wonder what the use of an intentionally hobbled class like this
is.  It was explained in class as representing an abstract idea such as
"animal".  We all agree what is meant by the term "animal" even no one among us
has ever seen this generic, prototypical creature.  We understand what "animal"
means through our encounters with individuals of various species.

Likewise, a language which admits the creation of abstract classes allows you
to define generic categories which include many related-yet-slightly-different
concepts.

The abstract `Gradient` class plays a supporting role in your system by
providing scaffolding for all of the other types of Gradient objects you can
dream up.  It's job is to be the central repository for all of the behaviors
that are common to Gradients.  Thanks to the useful methods it provides the
`Grad2Colors` classes' implementation is three lines long.  By Inheriting from
*Gradient* I was able to write a 3-color gradient in 9 lines of code (I'll
leave it to you to implement your own version).


#### Overriding a method

If a child class (a.k.a. *sub-class*) must perform a different operation than
its super-class it can simply define its own version of that operation by
implementing a method of the same name.  This is known as "overriding".

In assignment 6 we want to make abstract classes which contain methods which
should be overridden in each sub-class.  The compiler of a strongly-typed
language such as Java or C++ will ensure that this detail is not overlooked; an
error is issued when a sub-class is encountered containing a function which the
programmer has forgotten to implement.

Python, however, offers no such guarantees.  Is is up to us to ensure that such
functions are overridden.  One option is to write a method in the abstract
class which intentionally crashes the program with a helpful error message.

Here is an example from the file `Sequence/Sequneces.py`:

    def current(self):
        raise NotImplementedError("Subclass of Sequence must define its own current() operation")


A programmer creating a sub-class of `Sequence` is obligated to override this
method with a version of their own.
