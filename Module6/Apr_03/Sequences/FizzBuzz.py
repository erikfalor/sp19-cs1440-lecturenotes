from Sequence import Sequence


class FizzBuzz(Sequence):

    def current(self):
        """Return the value of the stream at the current position"""
        if self.start < self.pos \
                and (self.end is None or self.pos < self.end):
            val = ""
            if self.pos % 3 == 0:
                val = "Fizz"
            if self.pos % 5 == 0:
                val += "Buzz"
            if val == "":
                val = self.pos
            self.n = val
        else:
            self.n = None
        return self.n

