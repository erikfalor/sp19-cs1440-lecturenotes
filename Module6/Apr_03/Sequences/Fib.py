from math import sqrt

from Sequence import Sequence

class Fib(Sequence):
    phi = (1.0 + sqrt(5)) / 2.0
    psi = (1.0 - sqrt(5)) / 2.0
    recip_sqrt5 = 1.0 / sqrt(5)

    def current(self):
        """Return the value of the stream at the current position"""
        if self.start < self.pos \
                and (self.end is None or self.pos < self.end):
            self.n = int(Fib.recip_sqrt5 * (Fib.phi ** self.pos - Fib.psi ** self.pos))
        else:
            self.n = None
        return self.n

