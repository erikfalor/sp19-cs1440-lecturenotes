# CS1440 - Fri Apr 05 - Module 6

# Topics:

* Implementing an Abstract Factory
* How does Polymorphism simplify my code?
* How do you approach programming problems?
* Code reuse
* Design Patterns


----------------------------------------------------------------------------
# Implementing an Abstract Factory

See [source code in](Sequences/)



----------------------------------------------------------------------------
# How does Polymorphism simplify my code?

[M6R&R: Polymophism](../Readings_and_resources.md#markdown-header-4-polymorphism)



----------------------------------------------------------------------------
# How do you approach programming problems?

[M6R&R: Sharpen your Saw](../Readings_and_resources.md#markdown-header-how-do-you-approach-programming-problems)



----------------------------------------------------------------------------
# Code reuse

[M6R&R: Code Reuse](../Readings_and_resources.md#markdown-header-what-kinds-of-reuse-are-there)



----------------------------------------------------------------------------
# Design Patterns

[M6R&R: Design Patterns](../Readings_and_resources.md#markdown-header-design-patterns_1)
