from math import sqrt

from Sequence import Sequence


class Pell(Sequence):
    phi = (1.0 + sqrt(2.0))
    psi = (1.0 - sqrt(2.0))
    two_sqrt2 = 2.0 * sqrt(2.0)


    def current(self):
        """Return the value of the stream at the current position"""
        if self.start < self.pos \
                and (self.end is None or self.pos < self.end):
            self.n = round((Pell.phi ** self.pos - Pell.psi ** self.pos)
                           / Pell.two_sqrt2)
        else:
            self.n = None
        return self.n

