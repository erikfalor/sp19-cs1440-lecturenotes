import sys

from SequenceFactory import SequenceFactory, sequenceNames


def usage():
    print(f"Usage: {sys.argv[0]} {sequenceNames} [[START] END]")

if len(sys.argv) >= 4:
    seq = SequenceFactory(seqtype=sys.argv[1].lower(), start=int(sys.argv[2]), end=int(sys.argv[3]))

elif len(sys.argv) == 3:
    seq = SequenceFactory(seqtype=sys.argv[1].lower(), end=int(sys.argv[2]))

elif len(sys.argv) == 2:
    seq = SequenceFactory(seqtype=sys.argv[1].lower())

else:
    usage()
    sys.exit(1)

seq.run()
