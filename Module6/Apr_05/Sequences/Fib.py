from math import sqrt

from Sequence import Sequence

class Fib(Sequence):
    phi = (1.0 + sqrt(5)) / 2.0
    psi = (1.0 - sqrt(5)) / 2.0
    recip_sqrt5 = 1.0 / sqrt(5)
