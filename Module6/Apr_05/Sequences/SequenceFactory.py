from Fib import Fib
from FizzBuzz import FizzBuzz
from Pell import Pell
from FogBag import FogBag


# Collect names of all supported sequences for a usage message, or similar
sequenceNames = 'fib|fizzbuzz|fogbag|pell'


def SequenceFactory(seqtype='fib', start=0, end=101):
    if seqtype == 'fib':
        return Fib(start=start, end=end)
    elif seqtype == 'fizzbuzz':
        return FizzBuzz(start=start, end=end)
    elif seqtype == 'pell':
        return Pell(start=start, end=end)
    elif seqtype == 'fogbag':
        return FogBag(start=start, end=end)
