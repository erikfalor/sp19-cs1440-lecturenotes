from Sequence import Sequence


class FogBag(Sequence):

    def current(self):
        """Return the value of the stream at the current position"""
        if self.start < self.pos \
                and (self.end is None or self.pos < self.end):
            val = ""
            if self.pos % 4 == 0:
                val = "Fog"
            if self.pos % 7 == 0:
                val += "Bag"
            if val == "":
                val = self.pos
            self.n = val
        else:
            self.n = None
        return self.n
