# CS1440 - Mon Mar 25 - Module 6

# Announcements

## Assigned Reading for next Wednesday

Wednesday Mar 27: Chapter 5 of *The Pragmatic Programmer* - "Bend, or Break"


## FSLC

Cal Coopmans of AggieAir

Wednesday, March 27th
7pm - ESLC Room 053


# Topics:

* Assignment #5 stand up meeting
* Getting the most out of Git
* Experimentation with git branches
* Throwing away bad work


----------------------------------------------------------------------------
# Assignment #5 stand up meeting
        _
    .__(.)<  - Scrum!
     \___)   

1.  What did you accomplish yesterday?
2.  What are you going to accomplish today?
3.  What are your impediments?




----------------------------------------------------------------------------
# Getting the most out of Git

## A review of GIT basics

    $ git clone git@bitbucket.org:erikfalor/cs1440-hw3
    $ git add FILENAME ...
    $ git commit -m "Commit Message"
    $ git tag TAGNAME
    $ git log
    $ git push origin master TAGNAME ...
    $ git pull origin master

In the examples below, REVISION may be expressed in any of the following ways:

1. An SHA-1 commit name, a.k.a. the "true name" (e.g.
   `6b0fdef75fc709f95aedda60b9693093e086710e` or an abbreviation `6b0fdef7`)
2. `HEAD`, referring to the currently checked-out commit.  This is the git
   equivalent to the current working directory `.` in the terminal.
3. A name of a branch (i.e. `master`, `origin/master`)
4. The name of a tag (i.e. `Assn3`, `Assn4`)
5. A revision that is `N` generations older than a named commit.  Combine the
   name of the base commit with a generation number using a tilde `~`.  For
   example:
    *  `HEAD~1` is the previous commit
    * `master~3` is the great-grandparent commit of the tip of the branch `master`
    * `Assn3~2` was the 2nd-to-last commit before you submitted Assignment #3

Read `git help revisions` to learn all of the ways you can refer to commit
objects in the log.



--------------------------------------------------------------------------------
# Experimentation with git branches

* `git checkout -b BRANCHNAME`
Create a new branch named BRANCHNAME, and check it out.  `HEAD` still points to
the same commit that you were on before, but *new* commits will now be added to
this branch.  New commits will not become a part of the old branch you were on.


* `git branch`
List the branches you have checked out in this repo.


* `git branch -a`
List all of the branches in your repo, including branches from any remote
repositories you have fetched from.


* `git show-branch`
List all of the branches you have checked out in this repo, and show which
commits belong to each one. Branches contain the same commit when the `*` or
`+` symbols are present in each of their columns.


* `git show-branch --more=10`
Git's show-branch command stops displaying history as soon as it reaches the
earliest common ancestor for all commits. The --more argument tells it to go
back further, if possible.


* `git merge BRANCHNAME`
Make the commits in BRANCHNAME become part of the branch you're currently on.
Generally, you will first run `git checkout master` or a similar command before
you do a merge.
Run `git merge` when your exeperiment was a success and you want to put it
somewhere that the Nobel Prize Committee may notice it.


* `git branch -d BRANCHNAME`
Safely delete the branch BRANCHNAME.
Git will not allow you to delete a branch whose commits are not also present in
another branch.  Use `git branch -d` after you have successfully merged one of
your experimental branches into a permanent branch such as `master`.


* `git branch -D BRANCHNAME`
Dangerously delete the branch BRANCHNAME.

Use  `git branch -D` when you have determined that your experiment has gone
terribly wrong, and you're positive that you will never need to even look at
this code ever again.

It should be noted that the commits don't actually go anywhere; the name of the
branch is the only thing which is truly deleted. But without a name it is
difficult for you to recover those lost commits. This is why deleting branches
should only be done with care.



--------------------------------------------------------------------------------
# Throwing away bad work

## A word of warning

In the following section I will teach you how to use some commands which, used
unwisely, can result in a broken repository.  As git has a mind like a steel
trap it's actually quite hard to make git forget things entirely.  The trick
lies in cajoling git into remembering.

* `git reflog`
This powerful command displays the log of repository-modifying actions that git
has undertaken.  This information can be used to track down commits which may
have been pruned off of the tree.


## Get rid of *uncommitted* changes with 'git checkout'

Sometimes you find out pretty quickly that you're barking up the wrong tree.
Or, perhaps you have accidentally deleted an important file.  Whoops!  With
git, this is not a big deal.

* `git checkout -- FILENAME`
Discard changes in working directory.  This command lets you undo uncommitted
changes on a file-by-file basis.

* `git checkout -f`
The nuclear option.  Discard *all* changes in the working directory,
permanently undoing any changes which have not been committed.

The only way I know of to reverse the effect of this command is this unreliable procedure:

1. *If* your editor was running and had the affected file(s) open
2. *And* your editor doesn't _automagically_ re-read files from the disk when
   git updates them
3. *Then* re-save the file from the editor's memory back to disk


## Get rid of *committed but un-pushed* changes with 'git reset --hard'

Other times you realize that you have not only made a mistake, but have
committed to it by permanently recording it in your repository.  Well, perhaps
"permanent" is too strong a word... if you haven't yet pushed your latest
changes to a remote repository you can erase these commits and make this appear
like nothing happened at all.  As they say, what happens in Vegas, stays in
Vegas.

* `git reset --hard REVISION`
Move `HEAD` and the current branch to the commit specified by REVISION.  If you
want to undo the most recent commit, run `git reset --hard HEAD~1` where
`HEAD~1` refers to the *parent* of `HEAD`.  You can advance both `HEAD` and the
current branch forward in time, too; you'll just need a way to refer to that
commit, perhaps by consulting `git reflog` or `git log`.


## Get rid of committed and pushed changes with 'git revert'

The problem of playing with the timeline is that you may create a paradox or
inadvertently write yourself out of history.  The latter is possible through
using commands such as `git reset --hard` which have the power to change
commits in the past.  The negative effects will be noticed when you next try to
push your changes to a remote repository, which will notice that you are
missing a few things and refuse to accept your changes until your version of
history agrees with its own.

You can avoid this situation by *adding* instead of removing commits.  Create
and record a new commit which is the *inverse* of the commit which you wish to
remove.  This is called "reverting" a commit.  Instead of your mistake never
appearing in the log, the timeline will record the mistake and another commit
which negates it.  I don't have a tourism slogan to describe this circumstance,
I guess you'll just have to swallow your pride and own up to the fact that you
goofed up.

* 'git revert -n REVISION'
Create and add to the staging area a commit which is the inverse of REVISION.
Does not automatically run `git commit`.

If you run `git revert` without the `-n` switch, git will attempt to commit it
at the same time.  This will put you into a text editor to make a remark about
this revert, and if Git thinks that you like to use Vim, you may have a bad
time.
